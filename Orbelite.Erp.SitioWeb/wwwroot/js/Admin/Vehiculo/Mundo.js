﻿google.charts.load('current', { 'packages': ['corechart', 'bar'] });
google.charts.setOnLoadCallback();

$(document).ready(function () {
    controles();

});
function controles() {
    $('#btnBuscar').click(function () {
        BuscarEstadistica();
        cargarDatosGraficaPorAnio();
        cargarDatosGraficaPorMarca();
        CargarDatosTablaTotalMarca();
        CargarDatosTablaTotalAnio();
    });

    $('#ddlUbigeoDistritos').change(function () {
        BuscarEstadistica();
        cargarDatosGraficaPorAnio();
        cargarDatosGraficaPorMarca();
        CargarDatosTablaTotalMarca();
        CargarDatosTablaTotalAnio();
    });
   
    $('#ddlUbigeoDepatamento').change(function () {
        $('#tablePersonas').DataTable().ajax.reload();
        $("#ddlUbigeoProvincias").html('<option value="" selected>Todos</option>');
        $("#ddlUbigeoDistritos").html('<option value="" selected>Todos</option>');
        if ($('#ddlUbigeoDepatamento').val().length > 0) {
            BuscarProvincias();
        } else {
            $("#ddlUbigeoProvincias").prop("disabled", true);
            $("#ddlUbigeoDistritos").prop("disabled", true);
        }
        BuscarEstadistica();
        cargarDatosGraficaPorAnio();
        cargarDatosGraficaPorMarca();
        CargarDatosTablaTotalMarca();
        CargarDatosTablaTotalAnio();
    });
    $('#ddlUbigeoProvincias').change(function () {
        $('#tablePersonas').DataTable().ajax.reload();
        $("#ddlUbigeoDistritos").html('<option value="" selected>Todos</option>');
        if ($('#ddlUbigeoProvincias').val().length > 0) {
            BuscarDistritos();
        } else {
            $("#ddlUbigeoDistritos").prop("disabled", true);
        }

        BuscarEstadistica();
        cargarDatosGraficaPorAnio();
        cargarDatosGraficaPorMarca();
        CargarDatosTablaTotalMarca();
        CargarDatosTablaTotalAnio();
    });

    BuscarEstadistica();
    cargarDatosGraficaPorAnio();
    cargarDatosGraficaPorMarca();
    CargarDatosTablaTotalMarca();
    CargarDatosTablaTotalAnio();
}

function BuscarProvincias() {
    var url = $('#__URL_BUSCAR_PROVINCIAS').val();
    var dato = {
        "nombre": $("#ddlUbigeoDepatamento").val()
    };
    realizarPost(url, dato, 'json', RespuestaBuscarProvincias, ErrorBuscarProvincias, 10000);
}

function RespuestaBuscarProvincias(data) {
    $("#ddlUbigeoProvincias").prop("disabled", false);
    $("#ddlUbigeoDistritos").prop("disabled", true);
    for (var i = 0; i < data.length; i++) {
        $("#ddlUbigeoProvincias").append('<option value="' + data[i].nombre + '">' + data[i].nombre + '</option>');
    }
}

function ErrorBuscarProvincias(data) {
    MensajeAlerta("Ocurrio un error en busqueda de ubigeo, por favor comunicarse con soporte", "error");
}

function BuscarDistritos() {
    var url = $('#__URL_BUSCAR_DISTRITOS').val();
    var dato = {
        "nombre": $("#ddlUbigeoProvincias").val()
    };
    realizarPost(url, dato, 'json', RespuestaBuscarDistritos, ErrorBuscarProvincias, 10000);
}

function RespuestaBuscarDistritos(data) {
    $("#ddlUbigeoDistritos").prop("disabled", false);
    for (var i = 0; i < data.length; i++) {
        $("#ddlUbigeoDistritos").append('<option value="' + data[i].nombre + '">' + data[i].nombre + '</option>');
    }

}


function BuscarEstadistica() {
    $("#loadingSucesso").show();
    $("#msjError").hide();

    var url = $("#__URL_CARGAR_GENERAL_TOTAL_VEHICULAR").val();
    var departamento = null;
    var provincia = null;
    var distrito = null;

    if ($("#ddlUbigeoDepatamento").val() != "") {
        departamento = $("#ddlUbigeoDepatamento :selected").text();
    }
    if ($("#ddlUbigeoProvincias").val() != "") {
        provincia = $("#ddlUbigeoProvincias :selected").text();
    }
    if ($("#ddlUbigeoDistritos").val() != "") {
        distrito = $("#ddlUbigeoDistritos :selected").text();
    }

    var dato = {
        "departamento": departamento,
        "provincia": provincia,
        "distrito": distrito
    }
    realizarPost(url, dato, 'json', RespuestaBuscarEstadistica, BuscarEstadisticaError, 10000);
}

function RespuestaBuscarEstadistica(data) {
    $("#loadingSucesso").hide();

    $("#totalVehiculo").html(numeroConComas(redondearExp(data.totalVehiculo, 2)));
    $("#totalMoto").html(numeroConComas(redondearExp(data.totalMoto, 2)));
    $("#totalTaxi").html(numeroConComas(redondearExp(data.totalTaxi, 2)));
    $("#totalParticular").html(numeroConComas(redondearExp(data.totalParticular, 2)));
    $("#totalTransportePublico").html(numeroConComas(redondearExp(data.totalTransportePublico, 2)));
}

function BuscarEstadisticaError(data) {
    $("#loadingSucesso").hide();
   $("#msjError").show();
}


function cargarDatosGraficaPorAnio() {
    $("#loadingSucesso").show();
    $("#msjError").hide();

    var url = $("#__URL_CARGAR_PRINCIPAL_ANIO_VEHICULAR").val();
    var departamento = null;
    var provincia = null;
    var distrito = null;

    if ($("#ddlUbigeoDepatamento").val() != "") {
        departamento = $("#ddlUbigeoDepatamento :selected").text();
    }
    if ($("#ddlUbigeoProvincias").val() != "") {
        provincia = $("#ddlUbigeoProvincias :selected").text();
    }
    if ($("#ddlUbigeoDistritos").val() != "") {
        distrito = $("#ddlUbigeoDistritos :selected").text();
    }

    var dato = {
        "llave": $("#ddlFiltroVehicularBuscar").val(),
        "departamento": departamento,
        "provincia": provincia,
        "distrito": distrito
    }
    realizarPost(url, dato, 'json', RespuestacargarDatosGraficaPorAnio, CargarDatosGraficaPorAnioError, 10000);
}

function RespuestacargarDatosGraficaPorAnio(data) {
    $("#loadingSucesso").hide();
    graficaBarraPorAnio(data);
}

function CargarDatosGraficaPorAnioError(data) {
    $("#loadingSucesso").hide();
    $("#msjError").show();
}

function graficaBarraPorAnio(data) {
    var nombres = ['Categorias'];
    for (var i = 0; i < data.length; i++) {
        nombres.push(data[i].llave);
        nombres.push({ role: 'tooltip', 'p': { 'html': true } });
    }

    var arregloData = [];

    var totalGobal = 0;
    var total = 0;

    arregloData.push(nombres);

    for (var i = 0; i < data.length; i++) {
        totalGobal = 0;
        var registro = [];
        var nombre = data[i].llave;
        for (var m = 0; m < data.length; m++) {
            var valorTotal = 0;
            if (data[m].llave === nombre) {
                valorTotal = data[m].valor;
            }
            totalGobal += valorTotal;
        }
        registro.push(nombre + "\nTotal:" + numeroConComas(totalGobal) + "\n");
        for (var j = 0; j < data.length; j++) {
            var valor = 0;
            //var descripcion = "";
            if (data[j].llave === nombre) {
                valor = data[j].valor;
                //descripcion = data[i].anio;
            }
            total += valor;
            registro.push(valor);

            registro.push('<strong>' + data[i].llave + '</strong>' + '<br/>' + "Cantidad" + ' : <strong>' + numeroConComas(valor) + '</strong><br />');
        }
        arregloData.push(registro);
    }

    $('.spanTotalCtc').html(numeroConComas(total));

    var dataChart = google.visualization.arrayToDataTable(arregloData);


    var options = {
        legend: { position: 'bottom', maxLines: 4, alignment: 'start' },
        isStacked: true,
        tooltip: { isHtml: true }
    };
    var chart = new google.visualization.ColumnChart(document.getElementById("chartBarraTotalPorAnio"));

    chart.draw(dataChart, options);
    ValidateErrorEmptyData(dataChart, chart, options);
}


function cargarDatosGraficaPorMarca() {
    $("#loadingSucesso").show();
    $("#msjError").hide();

    var url = $("#__URL_CARGAR_PRINCIPAL_MARCA_VEHICULAR").val();
    var departamento = null;
    var provincia = null;
    var distrito = null;

    if ($("#ddlUbigeoDepatamento").val() != "") {
        departamento = $("#ddlUbigeoDepatamento :selected").text();
    }
    if ($("#ddlUbigeoProvincias").val() != "") {
        provincia = $("#ddlUbigeoProvincias :selected").text();
    }
    if ($("#ddlUbigeoDistritos").val() != "") {
        distrito = $("#ddlUbigeoDistritos :selected").text();
    }

    var dato = {
        "llave": $("#ddlFiltroVehicularBuscar").val(),
        "departamento": departamento,
        "provincia": provincia,
        "distrito": distrito
    }
    realizarPost(url, dato, 'json', RespuestacargarDatosGraficaPorMarca, CargarDatosGraficaPorMarcaError, 10000);
}

function RespuestacargarDatosGraficaPorMarca(data) {
    $("#loadingSucesso").hide();
    graficaBarraPorMarca(data);
}

function CargarDatosGraficaPorMarcaError(data) {
    $("#loadingSucesso").hide();
    $("#msjError").show();
}

function graficaBarraPorMarca(data) {
    var nombres = ['Categorias'];
    for (var i = 0; i < data.length; i++) {
        nombres.push(data[i].llave);
        nombres.push({ role: 'tooltip', 'p': { 'html': true } });
    }

    var arregloData = [];

    var totalGobal = 0;
    var total = 0;

    arregloData.push(nombres);

    for (var i = 0; i < data.length; i++) {
        totalGobal = 0;
        var registro = [];
        var nombre = data[i].llave;
        for (var m = 0; m < data.length; m++) {
            var valorTotal = 0;
            if (data[m].llave === nombre) {
                valorTotal = data[m].valor;
            }
            totalGobal += valorTotal;
        }
        registro.push(nombre + "\nTotal:" + numeroConComas(totalGobal) + "\n");
        for (var j = 0; j < data.length; j++) {
            var valor = 0;
            //var descripcion = "";
            if (data[j].llave === nombre) {
                valor = data[j].valor;
                //descripcion = data[i].anio;
            }
            total += valor;
            registro.push(valor);

            registro.push('<strong>' + data[i].llave + '</strong>' + '<br/>' + "Cantidad" + ' : <strong>' + numeroConComas(valor) + '</strong><br />');
        }
        arregloData.push(registro);
    }

    $('.spanTotalCtc').html(numeroConComas(total));

    var dataChart = google.visualization.arrayToDataTable(arregloData);


    var options = {
        legend: { position: 'bottom', maxLines: 4, alignment: 'start' },
        isStacked: true,
        tooltip: { isHtml: true }
    };
    var chart = new google.visualization.ColumnChart(document.getElementById("chartBarraTotalPorMarca"));

    chart.draw(dataChart, options);
    ValidateErrorEmptyData(dataChart, chart, options);
}



function CargarDatosTablaTotalMarca() {
    $("#loadingSucesso").show();
    $("#msjError").hide();

    var url = $("#__URL_CARGAR_TOTAL_MARCA_VEHICULAR").val();
    var departamento = null;
    var provincia = null;
    var distrito = null;

    if ($("#ddlUbigeoDepatamento").val() != "") {
        departamento = $("#ddlUbigeoDepatamento :selected").text();
    }
    if ($("#ddlUbigeoProvincias").val() != "") {
        provincia = $("#ddlUbigeoProvincias :selected").text();
    }
    if ($("#ddlUbigeoDistritos").val() != "") {
        distrito = $("#ddlUbigeoDistritos :selected").text();
    }

    var dato = {
        "llave": $("#ddlFiltroVehicularBuscar").val(),
        "departamento": departamento,
        "provincia": provincia,
        "distrito": distrito
    }
    realizarPost(url, dato, 'json', sucesoCargarDatosTablaTotalMarca, 10000);
}

function sucesoCargarDatosTablaTotalMarca(data) {
    CargarTablaTodosMarca(data);
}

var tablaMarca = null;
function CargarTablaTodosMarca(data) {
    if (tablaMarca) {
        tablaMarca.destroy();
        tablaMarca = null;
    }
    var html = "";
    var total = 0;


    for (i = 0; i < data.length; i++) {
        total = total + data[i].valor;

        html += '<tr >' +
            "<td>" + data[i].llave + "</td>" +
            "<td data-order='" + numeroConComas(data[i].valor) + "'>" + numeroConComas(data[i].valor) + "</td>" +
            "</tr>";
    }
    $("#tableDataMarca tbody").html(html);
    $("#totalTableMarca").html("Total:  " + numeroConComas(total));

    tablaMarca = $('#tableDataMarca').DataTable({
        "order": [[1, "desc"]],
        "info": true,
        "searching": true,
        "bLengthChange": false,
        "language": {
            "url": "http://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
        }
    });
}


function CargarDatosTablaTotalAnio() {
    $("#loadingSucesso").show();
    $("#msjError").hide();

    var url = $("#__URL_CARGAR_TOTAL_ANIO_VEHICULAR").val();
    var departamento = null;
    var provincia = null;
    var distrito = null;

    if ($("#ddlUbigeoDepatamento").val() != "") {
        departamento = $("#ddlUbigeoDepatamento :selected").text();
    }
    if ($("#ddlUbigeoProvincias").val() != "") {
        provincia = $("#ddlUbigeoProvincias :selected").text();
    }
    if ($("#ddlUbigeoDistritos").val() != "") {
        distrito = $("#ddlUbigeoDistritos :selected").text();
    }

    var dato = {
        "llave": $("#ddlFiltroVehicularBuscar").val(),
        "departamento": departamento,
        "provincia": provincia,
        "distrito": distrito
    }
    realizarPost(url, dato, 'json', sucesoCargarDatosTablaTotalAnio, 10000);
}

function sucesoCargarDatosTablaTotalAnio(data) {
    CargarTablaTodosAnio(data);
}

var tablaAnio = null;
function CargarTablaTodosAnio(data) {
    if (tablaAnio) {
        tablaAnio.destroy();
        tablaAnio = null;
    }
    var html = "";
    var total = 0;


    for (i = 0; i < data.length; i++) {
        total = total + data[i].valor;

        html += '<tr >' +
            "<td>" + data[i].llave + "</td>" +
            "<td data-order='" + numeroConComas(data[i].valor) + "'>" + numeroConComas(data[i].valor) + "</td>" +
            "</tr>";
    }
    $("#tableDataAnio tbody").html(html);
    $("#totalTableAnio").html("Total:  " + numeroConComas(total));

    tablaAnio = $('#tableDataAnio').DataTable({
        "order": [[1, "desc"]],
        "info": true,
        "searching": true,
        "bLengthChange": false,
        "language": {
            "url": "http://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
        }
    });
}


function redondearExp(numero, digitos) {
    function toExp(numero, digitos) {
        let arr = numero.toString().split("e");
        let mantisa = arr[0], exponente = digitos;
        if (arr[1]) exponente = Number(arr[1]) + digitos;
        return Number(mantisa + "e" + exponente.toString());
    }
    let entero = Math.round(toExp(Math.abs(numero), digitos));
    return Math.sign(numero) * toExp(entero, -digitos);
}
function numeroConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}