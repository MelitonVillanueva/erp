﻿$(document).ready(function () {
    ConfiguracionSitio();
});

function ConfiguracionSitio() {

    $('#btnSubirArchivo').change(function () {
        InsertarArchivo();
    });


    $('#btnGuardarXml').click(function () {
        RegistroXml();
    });

}



function RegistroXml() {

    //if (validarCamposPersona() == 0) {
    $("#loadingSucessoModal").show();
    $("#infoSucesoModal").hide();
    var url = $('#__URL_CARGAR_XMLS').val();

    var dato = {
        "listaCadena": $("#__HD_URL_DOCUMENTO").val(),
        "habilitado": $("#chkHabilitado").is(':checked')
    };
    realizarPost(url, dato, 'json', RespuestaRegistroXml, RegistroXmlError, 10000);
    //}

}

function RespuestaRegistroXml(dato) {
    $("#loadingSucessoModal").hide();
    $("#infoSucesoModal").show();
    console.log(dato);
    $("#__HD_URL_DOCUMENTO").val("");
    $("#infoSucesoModal").html(dato.mensaje);
    document.getElementById("btnSubirArchivo").value = null;

}

function RegistroXmlError(dato) {

    console.log(dato.responseJSON);
    console.log(dato.responseJSON.mensajes);

    $("#loadingSucessoModal").hide();
    $("#infoErrorModal").show();
    document.getElementById("btnSubirArchivo").value = null;
    var html = "";
    if (dato.responseJSON.mensajes != null) {
            html += dato.responseJSON.mensajes;
    } else {
        html = "Ocurrio un problema al validar tu archivo";
    }

    $("#__HD_URL_DOCUMENTO").val("");

    $("#infoErrorModal").show();
    $("#infoErrorModal").html(html);
}






function InsertarArchivo() {
    $("#loadingSucessoModal").show();
    $("#infoErrorModal").hide();
    $("#infoSucesoModal").hide();
    var url = $('#__URL_SUBIR_DOCUMENTO').val();
    var dato = new FormData($("#FormArchivo")[0]);
    $.ajax({
        type: "POST",
        url: url,
        data: dato,
        processData: false,
        contentType: false,
        success: function (data) {
            RespuestaInsertarArchivo(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            ErrorInsertarArchivo(jqXHR.responseJSON);
        },
    });
}
function RespuestaInsertarArchivo(dato) {
    $("#loadingSucessoModal").hide();
    $("#infoSucesoModal").show();
    console.log(dato);
    $("#__HD_URL_DOCUMENTO").val(JSON.stringify(dato.listaXml));
    $("#infoSucesoModal").html("Se han cargado " + dato.listaXml.length + " archivos.");
    document.getElementById("btnSubirArchivo").value = null;
}

function ErrorInsertarArchivo(data) {
    $("#loadingSucessoModal").hide();
    $("#infoErrorModal").show();
    document.getElementById("btnSubirArchivo").value = null;
    var html = "";
    if (data.mensajes.length > 0) {
        for (var i = 0; i < data.mensajes.length; i++) {
            html += data.mensajes[i];
        }
    } else {
        html = "Ocurrio un problema al validar tu archivo";
    }

    $("#infoErrorModal").show();
    $("#infoErrorModal").html(html);
}