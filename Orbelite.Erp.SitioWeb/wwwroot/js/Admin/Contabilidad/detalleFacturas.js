﻿$(document).ready(function () {
    ConfiguracionSitio();
});

function ConfiguracionSitio() {

    $('#txtDocumento').keypress(function () {
        SoloNumeros1_9();
    });

    $('#txtIgv').bind('paste', function () {
        var self = this;
        setTimeout(function () {
            if (!/^\d*(,\d{1,2})+$/.test($(self).val())) $(self).val('');
        }, 0);
    });

    $('#txtImporteTotal').bind('paste', function () {
        var self = this;
        setTimeout(function () {
            if (!/^\d*(,\d{1,2})+$/.test($(self).val())) $(self).val('');
        }, 0);
    });

    $('.decimal').keypress(function (e) {
        var character = String.fromCharCode(e.keyCode)
        var newValue = this.value + character;
        if ((newValue.length == 1 && character == '.') || (isNaN(character) && character != '.') || hasDecimalPlace(newValue, 3)) {
            e.preventDefault();
            return false;
        }
    });


    if ($("#txtCorreo").val().length > 0) {
        $(".correox").show();
    }
    else {
        $(".correox").hide();
    }

    if ($("#txtTelefono").val().length > 0) {
        $(".telefonox").show();
    }
    else {
        $(".telefonox").hide();
    }

    if ($("#txtDocumento").val().length > 8) {
        $(".natural").hide();
        $(".juridica").show();
    }
    else {
        $(".juridica").hide();
        $(".natural").show();
    }

    var tipo = $('#___ID_MONEDA').val();

    $("#ddlMoneda").val(tipo).change();

    $('.input-group.date').datepicker({ format: "dd/mm/yyyy" });

    FiltrarDocumento();

    //FacturaGrillaLoad();
}

function hasDecimalPlace(value, x) {
    var pointIndex = value.indexOf('.');
    return pointIndex >= 0 && pointIndex < value.length - x;
}


function FiltrarDocumento() {
    var urlData = $("#__URL_OCURRENCIA").val();
    var docs = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('ocurrencia'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: urlData + '/%QUERY',
            wildcard: '%QUERY'
        }
    });

    $('#txtDocumento').typeahead(null, {
        name: 'docs',
        display: 'documento',
        source: docs,
        templates: {
            empty: [
                '<div class="empty-message text-danger" style="padding: 10px 15px; text-align: center;"> No existe sugerencia para su busqueda </div>'
            ].join(''),
            suggestion: Handlebars.compile('<div> <strong> {{razonSocial}} {{nombres}} {{paterno}} {{materno}} </strong> <br> {{documento}} </div>')
        }
    });

    $('#txtDocumento').bind('typeahead:select', function (ev, suggestion) {

        var url = $('#__GET_DETALLE').val() + $('#txtDocumento').val();
        var dato = {
        };
        realizarGet(url, dato, 'json', RespuestaBuscarInfoPerfil, BuscarInfoPerfilError, 10000);

    });
}


function RespuestaBuscarInfoPerfil(dato) {
    $("#loadingSucessoModal").hide();
    $("#txtRazonSocial").val(dato.razonSocial);
    $("#txtDepartamento").val(dato.departamento);
    $("#txtProvincia").val(dato.provincia);
    $("#txtDistrito").val(dato.distrito);
    $("#txtDireccion").val(dato.direccion);
    $("#txtCorreo").val(dato.correo);
    $("#txtTelefono").val(dato.telefono);

    if (dato.documento.length == 8) {
        $(".juridica").hide();
        $(".natural").show();
        $("#txtNombrePersona").show();
        $("#txtNombrePersona").val(dato.nombres + ' ' + dato.paterno + ' ' + dato.materno);
    }
    else {
        $(".natural").hide();
        $(".juridica").show();
    }

    if (dato.correo.length > 0) {
        $(".correox").show();
    }
    else {
        $(".correox").hide();
    }

    if (dato.telefono.length > 0) {
        $(".telefonox").show();
    }
    else {
        $(".telefonox").hide();
    }

}

function BuscarInfoPerfilError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucessoModal").hide();
    console.log(jqXHR);
    $("#msjError").hide();
}

//function FacturaGrillaLoad() {
//    var options = $.extend(true, {}, defaults, {
//        "aoColumnDefs": [
//            {
//                'bSortable': false,
//                'aTargets': [0]
//            },
//        ],
//        "order": [[0, "desc"]],
//        "iDisplayLength": 5,
//        "aLengthMenu": [
//            [5, 10, 25, 50, -1],
//            [5, 10, 25, 50, "All"]
//        ],
//        "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"ip>',
//        "tableTools": {
//            "aButtons": [
//            ]
//        },
//        "bFilter": false,
//        "bSort": true,
//        "processing": true,
//        "serverSide": true,
//        "lengthMenu": [
//            [10, 20, 50, 100, 150],
//            [10, 20, 50, 100, 150]
//        ],
//        "pageLength": 10,
//        "ajax": {
//            "url": $('#__URL_BUSQUEDA').val(), // ajax source
//            "type": "POST",
//            "error": validarErrorGrilla,
//            "data": function (d) {
//                return $.extend({}, d, {
//                    "serie": $("#txtNroFactura").val()
//                });
//            }
//        },
//        "columns": [
//            //{
//            //    "render": function (data, type, row) {
//            //        return '<button onclick="DetalleFactura(\'' + row.idFactura + '\')" class="btn btn-sm btn-clean btn-icon mr-1" title="Editar"><i class="la la-edit"></i></button> ';
//            //        //+ '<button onclick="PreguntarEliminarCategoria(\'' + row.id + '\')" class="btn btn-sm btn-clean btn-icon" title="Eliminar"><i class="la la-trash"></i></button>'
//            //    }
//            //},
//            { "data": "numero" },
//            {
//                "render": function (data, type, row) {
//                    let texto = "";
//                    if (row.persona != null) {
//                        texto = row.persona.documento;
//                    }
//                    return texto;
//                }
//            },
//            {
//                "render": function (data, type, row) {
//                    let texto = "";
//                    if (row.persona != null) {
//                        texto = row.persona.razonSocial;
//                    }
//                    return texto;
//                }
//            },
//            { "data": "fechaVencimientoCadena" },
//            { "data": "fechaEmisionCadena" },
//            //{ "data": "idMoneda" },
//            {
//                "render": function (data, type, row) {
//                    let texto = "DOLAR";
//                    if (row.idMoneda == 1) {
//                        texto = "NUEVO SOL";
//                    }
//                    return texto;
//                }
//            },
//            { "data": "igv" },
//            { "data": "subTotalVentas" }

//        ],
//        "language": {
//            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
//        },
//    });

//    $('#dataTableFactura').DataTable(options);
//}