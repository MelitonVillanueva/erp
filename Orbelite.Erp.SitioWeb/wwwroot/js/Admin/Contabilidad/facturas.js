﻿$(document).ready(function () {
    ConfiguracionSitio();
});

function ConfiguracionSitio() {

    $('#btnSubirArchivo').change(function () {
        InsertarArchivo();
    });

    $("#btnBuscar").click(function () {
        $('#dataTableFactura').DataTable().ajax.reload();
    });

    FacturaGrillaLoad();

    $('.input-group.date').datepicker({ format: "dd-mm-yyyy" });
}

function InsertarArchivo() {
    $("#loadingSucessoModal").show();
    $("#infoErrorModal").hide();
    $("#infoSucesoModal").hide();
    var url = $('#__URL_SUBIR_DOCUMENTO').val();
    var dato = new FormData($("#FormArchivo")[0]);
    $.ajax({
        type: "POST",
        url: url,
        data: dato,
        processData: false,
        contentType: false,
        success: function (data) {
            RespuestaInsertarArchivo(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            ErrorInsertarArchivo(jqXHR.responseJSON);
        },
    });
}
function RespuestaInsertarArchivo(dato) {
    $("#loadingSucessoModal").hide();
    $("#infoSucesoModal").show();
    console.log(dato);
    $("#__HD_URL_DOCUMENTO").val(dato.id);
    $("#infoSucesoModal").html(dato.mensaje);
    document.getElementById("btnSubirArchivo").value = null;
}

function ErrorInsertarArchivo(data) {
    $("#loadingSucessoModal").hide();
    $("#infoErrorModal").show();
    document.getElementById("btnSubirArchivo").value = null;
    var html = "";
    if (data.mensajes.length > 0) {
        for (var i = 0; i < data.mensajes.length; i++) {
            html += data.mensajes[i];
        }
    } else {
        html = "Ocurrio un problema al validar tu archivo";
    }

    $("#infoErrorModal").show();
    $("#infoErrorModal").html(html);

}


function FacturaGrillaLoad() {
    var options = $.extend(true, {}, defaults, {
        "aoColumnDefs": [
            {
                'bSortable': false,
                'aTargets': [0]
            },
        ],
        "order": [[0, "desc"]],
        "iDisplayLength": 5,
        "aLengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "All"]
        ],
        "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"ip>',
        "tableTools": {
            "aButtons": [
            ]
        },
        "bFilter": false,
        "bSort": true,
        "processing": true,
        "serverSide": true,
        "lengthMenu": [
            [10, 20, 50, 100, 150],
            [10, 20, 50, 100, 150]
        ],
        "pageLength": 10,
        "ajax": {
            "url": $('#__URL_BUSQUEDA').val(), // ajax source
            "type": "POST",
            "error": validarErrorGrilla,
            "data": function (d) {
                return $.extend({}, d, {
                    "serie": $("#txtNroFactura").val(),
                    "ruc": $("#txtRuc").val(),
                    "emision": $("#txtEmision").val(),
                    "vencimiento": $("#txtVencimiento").val()
                });
            }
        },
        "columns": [
            {
                "render": function (data, type, row) {
                    return '<button onclick="DetalleFactura(\'' + row.idFactura + '\')" class="btn btn-sm btn-clean btn-icon mr-1" title="Editar"><i class="la la-edit"></i></button> ';
                    //+ '<button onclick="PreguntarEliminarCategoria(\'' + row.id + '\')" class="btn btn-sm btn-clean btn-icon" title="Eliminar"><i class="la la-trash"></i></button>'
                }
            },
            { "data": "numero" },
            {
                "render": function (data, type, row) {
                    let texto = "";
                    if (row.persona != null) {
                        texto = row.persona.documento;
                    }
                    return texto;
                }
            },
            {
                "render": function (data, type, row) {
                    let texto = "";
                    if (row.persona != null) {
                        texto = row.persona.razonSocial;    
                    }
                    return texto;
                }
            },
            { "data": "fechaVencimientoCadena" },
            { "data": "fechaEmisionCadena" },
            //{ "data": "idMoneda" },
            {
                "render": function (data, type, row) {
                    let texto = "DOLAR";
                    if (row.idMoneda == 1) {
                        texto = "NUEVO SOL";
                    }
                    return texto;
                }
            },
            { "data": "importeTotal" },
            { "data": "igv" },
            { "data": "subTotalVentas" }

        ],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
    });

    $('#dataTableFactura').DataTable(options);
}

function DetalleFactura(id) {

    location.href = $('#__URL_DETALLE_FACTURA').val() + "/" + id;

}