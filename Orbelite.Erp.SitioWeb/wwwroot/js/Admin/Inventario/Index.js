﻿var idProducto = "";

$(document).ready(function () {
    ConfiguracionSitio();
});

function ConfiguracionSitio() {

    $("#tbmCantidad").inputSpinner({ buttonsOnly: true, autoInterval: undefined });

    ProductoGrillaLoad();

    $("#btnBuscar").click(function () {
        $('#dataTableProducto').DataTable().ajax.reload();
    });

    $("#btnNuevo").click(function () {
        idProducto = "";
        LimpiarAgregar();
        $('#verProductoModal').modal({ backdrop: 'static', keyboard: false })
    });

    //$("#btnGuardar").click(function () {
    //    //GuardarPersona();
    //    RegistroProducto();
    //});

    $("#btnLimpiar").click(function () {
        $("#txtProducto").val("");
        $("#ddlHabilitado").val("").change();
        $('#dataTableProducto').DataTable().ajax.reload();
    });


    $("#btnGuardar").click(function () {

        if (idProducto != "") {
            EditarProducto();
        }
        else {
            RegistroProducto();
        }

    });


    $('#btnBuscarDni').click(function () {

        if ($('#ddlTipo').find(":selected").val() == 100) {
            BuscarInfo();
        }
        else {
            BuscarInfoRuc();
        }

    });


    $("#chkIlimitado").change(function () {
        if (this.checked) {
            //$('#tbmCantidad').attr('readonly', true);
            $("#tbmCantidad").prop("disabled", $(this).prop("checked"))
            $("#tbmCantidad").val("0");
        }
        else {
            $("#tbmCantidad").prop("disabled", $(this).prop("checked"))
            $("#tbmCantidad").val("0");
        }
    });

    FiltrarProducto();
}

function FiltrarProducto() {
    var urlData = $("#__URL_OCURRENCIA").val();
    var docs = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('ocurrencia'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: urlData + '/%QUERY',
            wildcard: '%QUERY'
        }
    });

    $('#txtProducto').typeahead(null, {
        name: 'docs',
        display: 'nombre',
        source: docs
    });

    $('#txtProducto').bind('typeahead:select', function (ev, suggestion) {
        //$('#txtMarca').val(suggestion.nombre);
    });
}


function ProductoGrillaLoad() {
    var options = $.extend(true, {}, defaults, {
        "aoColumnDefs": [
            {
                'bSortable': false,
                'aTargets': [0]
            },
        ],
        "order": [[0, "desc"]],
        "iDisplayLength": 5,
        "aLengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "All"]
        ],
        "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"ip>',
        "tableTools": {
            "aButtons": [
            ]
        },
        "bFilter": false,
        "bSort": true,
        "processing": true,
        "serverSide": true,
        "lengthMenu": [
            [10, 20, 50, 100, 150],
            [10, 20, 50, 100, 150]
        ],
        "pageLength": 10,
        "ajax": {
            "url": $('#__URL_BUSQUEDA').val(), // ajax source
            "type": "POST",
            "error": validarErrorGrilla,
            "data": function (d) {
                return $.extend({}, d, {
                    "nombre": $("#txtProducto").val(),
                    "estaHabilitado": $('#ddlHabilitado').val()
                });
            }
        },
        "columns": [

            {
                "render": function (data, type, row) {
                    return '<button onclick="DetalleProducto(\'' + row.id + '\')" class="btn btn-sm btn-clean btn-icon mr-1" title="Editar"><i class="la la-edit"></i></button> ' + '<button onclick="PreguntarEliminarCategoria(\'' + row.id + '\')" class="btn btn-sm btn-clean btn-icon" title="Eliminar"><i class="la la-trash"></i></button>';
                }
            },
            { "data": "nombre" },
            { "data": "cantidad" },
            {
                "render": function (data, type, row) {
                    let texto = "No"
                    if (row.esIlimitado) {
                        texto = "Si"
                    }

                    if (row.esIlimitado == null) {
                        texto = ""
                    }

                    return texto;
                }
            },
            {
                "render": function (data, type, row) {
                    let texto = "No"
                    if (row.estaHabilitado) {
                        texto = "Si"
                    }

                    if (row.estaHabilitado == null) {
                        texto = ""
                    }

                    return texto;
                }
            }

        ],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
    });

    $('#dataTableProducto').DataTable(options);
}

function RegistroProducto() {

    if (validarCamposProducto() == 0) {
    $("#loadingSucessoModal").show();
    $("#infoSucesoModal").hide();
    var url = $('#__URL_REGISTRO_PRODUCTO').val();
    var cantidad = $("#tbmCantidad").val();

    if ($("#chkIlimitado").is(':checked')) {
        cantidad = 0;
        //$('#inputId').attr('readonly', true);
    }

    var dato = {
        //"idTipo": $('#ddlTipo').find(":selected").val(),
        "nombre": $("#tbmProducto").val(),
        "descripcion": $("#tbmDescripcion").val(),
        "estaHabilitado": $("#chkHabilitado").is(':checked'),
        "esIlimitado": $("#chkIlimitado").is(':checked'),
        "cantidad": cantidad
    };
    realizarPost(url, dato, 'json', RespuestaRegistroProducto, RegistroProductoError, 10000);
    }

}

function RespuestaRegistroProducto(dato) {
    $("#loadingSucessoModal").hide();

    MensajeAlerta("¡Se registró correctamente! ", "success");
    $('#verProductoModal').modal('toggle');
    $('#dataTableProducto').DataTable().ajax.reload();

}

function RegistroProductoError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucessoModal").hide();
    //alert("error");
    console.log(jqXHR);
    MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
}


function EditarProducto() {

    if (validarCamposProducto() == 0) {
    $("#loadingSucessoModal").show();
    $("#infoSucesoModal").hide();
    var url = $('#__URL_EDITAR_PRODUCTO').val();
    var cantidad = $("#tbmCantidad").val();

    if ($("#chkIlimitado").is(':checked')) {
        cantidad = 0;
        //$('#inputId').attr('readonly', true);
    }

    var dato = {
        "id": idProducto,
        "nombre": $("#tbmProducto").val(),
        "descripcion": $("#tbmDescripcion").val(),
        "estaHabilitado": $("#chkHabilitado").is(':checked'),
        "esIlimitado": $("#chkIlimitado").is(':checked'),
        "cantidad": cantidad
    };
    realizarPost(url, dato, 'json', RespuestaEditarProducto, EditarProductoError, 10000);
    }

}

function RespuestaEditarProducto(dato) {
    $("#loadingSucessoModal").hide();

    MensajeAlerta("¡Se guardó correctamente! ", "success");
    $('#verProductoModal').modal('toggle');
    $('#dataTableProducto').DataTable().ajax.reload();

}

function EditarProductoError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucessoModal").hide();
    //alert("error");
    console.log(jqXHR);
    MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
}



function validarCamposProducto() {

    var salida = 0;

    if ($("#tbmProducto").val().length !== 0) {
        $("#tbmProductoHtml").hide();
        //salida = true;
    }
    else {
        salida++;
        $("#tbmProductoHtml").html("Ingrese un valor válido");
        $("#tbmProductoHtml").show();
        $("#tbmProducto").focus();
    }

    if ($("#tbmDescripcion").val().length !== 0) {
        $("#tbmDescripcionHtml").hide();
        //salida = true;
    }
    else {
        salida++;
        $("#tbmDescripcionHtml").html("Ingrese un valor válido");
        $("#tbmDescripcionHtml").show();
        $("#tbmDescripcion").focus();
    }
    

    if ($("#chkIlimitado").is(':checked')) {
        $("#tbmCantidadHtml").hide();
    }
    else {

        if ($("#tbmCantidad").val() > 0) {
            $("#tbmCantidadHtml").hide();
        }
        else {
            salida++;
            $("#tbmCantidadHtml").html("Ingrese un valor válido");
            $("#tbmCantidadHtml").show();
            $("#tbmCantidad").focus();
        }

    }

    return salida;
}

function HideMensajes() {
    $("#tbmDocumentoHtml").hide();
    $("#tbmNombresHtml").hide();
    $("#tbmPaternoHtml").hide();
    $("#tbmMaternoHtml").hide();
    $("#tbmRazonHtml").hide();
    $("#tbmCorreoHtml").hide();
    $("#tbmTelefonoHtml").hide();
}

function LimpiarAgregar() {
    $("#tbmProducto").val("");
    $("#tbmDescripcion").val("");
    $("#chkIlimitado").prop('checked', false);
    $("#chkHabilitado").prop('checked', false);
    $("#tbmCantidad").prop('disabled', false);
    $("#tbmCantidad").val("0");
    //$('#tbmCantidad').attr('readonly', false);
}


function RespuestaGuardarPersona(dato) {
    $("#loadingSucessoModal").hide();

    location.href = $('#__URL_BUSQUEDA_PERSONA').val();

}

function GuardarPersonaError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucessoModal").hide();
    alert("error");
    console.log(jqXHR);
    MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
}


function DetalleProducto(id) {
    //Limpiar();
    //if ($("#tbmDocumento").val().length > 0) {

    //$("#tbmDocumentoHtml").hide();
    $("#loadingSucessoModal").show();
    var url = $('#__GET_DETALLE').val() + '/' + id;
    var dato = {
    };
    realizarGet(url, dato, 'json', RespuestaDetalleProducto, DetalleProductoError, 10000);
    //} else {
    //    $("#tbmDocumento").focus();
    //    $("#tbmDocumentoHtml").show();
    //    $("#tbmDocumentoHtml").html("El número de DNI es requerido");
    //}

}

function RespuestaDetalleProducto(dato) {
    $("#loadingSucessoModal").hide();
    //$("#tbmDocumentoHtml").hide();

    console.log(dato);

    idProducto = dato.id;

    $("#tbmProducto").val(dato.nombre);
    $("#tbmDescripcion").val(dato.descripcion);
    $("#chkIlimitado").prop('checked', dato.esIlimitado);
    $("#chkHabilitado").prop('checked', dato.estaHabilitado);

    if ($("#chkIlimitado").is(':checked')) {
        $("#tbmCantidad").val("");
        $('#tbmCantidad').attr('readonly', true);
    }
    else {
        $("#tbmCantidad").val(dato.cantidad);
    }

    $('#verProductoModal').modal({ backdrop: 'static', keyboard: false })

}

function DetalleProductoError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucessoModal").hide();
    console.log(jqXHR);
    //$("#tbmDocumentoHtml").hide();
    $("#msjError").hide();
}



function NuevoCargarArchivo() {
    $("#crearNuevoArchvioModal").modal("show");
    LimpiarDatosModal();
}

function InsertarArchivo() {
    $("#loadingSucessoModal").show();
    $("#infoErrorModal").hide();
    $("#infoSucesoModal").hide();
    var url = $('#__URL_SUBIR_DOCUMENTO').val();
    var dato = new FormData($("#FormArchivo")[0]);
    $.ajax({
        type: "POST",
        url: url,
        data: dato,
        processData: false,
        contentType: false,
        success: function (data) {
            RespuestaInsertarArchivo(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            ErrorInsertarArchivo(jqXHR.responseJSON);
        },
    });
}
function RespuestaInsertarArchivo(dato) {
    $("#loadingSucessoModal").hide();
    $("#infoSucesoModal").show();
    console.log(dato);
    $("#__HD_URL_DOCUMENTO").val(dato.id);
    $("#infoSucesoModal").html(dato.mensaje);
    document.getElementById("btnSubirArchivo").value = null;
}

function ErrorInsertarArchivo(data) {
    $("#loadingSucessoModal").hide();
    $("#infoErrorModal").show();
    document.getElementById("btnSubirArchivo").value = null;
    var html = "";
    if (data.mensajes.length > 0) {
        for (var i = 0; i < data.mensajes.length; i++) {
            html += data.mensajes[i];
        }
    } else {
        html = "Ocurrio un problema al validar tu archivo";
    }

    $("#infoErrorModal").show();
    $("#infoErrorModal").html(html);
}

function GuardarDocumento() {
    if ($("#tbmGrupo").val().length > 0) {
        if ($("#__HD_URL_DOCUMENTO").val().length > 0) {
            $("#loadingSucessoModal").show();
            $("#infoSucesoModal").hide();
            var url = $('#__URL_GUARDAR_DOCUMENTO').val();

            var datogrupo = {
                "id": $("#__tbIdGrupo").val(),
                "nombre": $("#tbmGrupo").val()
            };
            var dato = {
                "rutaArchivo": $("#__HD_URL_DOCUMENTO").val(),
                "grupo": datogrupo,
            };
            realizarPost(url, dato, 'json', RespuestaGuardarDocumento, GuardarDocumentoError, 10000);
        } else {
            MensajeAlerta("¡Seleccione archivo", "info");
        }
    } else {
        $("#tbmGrupoHtml").show();
        $("#tbmGrupoHtml").html("Debe ingresar un grupo");
        $("#tbmGrupo").focus();
    }

}

function RespuestaGuardarDocumento(dato) {
    $("#loadingSucessoModal").hide();
    MensajeAlerta("¡Se guardó correctamente! En unos minutos se veran reflejado los registros cargados", "success");
    LimpiarDatosModal();
}

function GuardarDocumentoError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucessoModal").hide();
    console.log(jqXHR);
    MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
}

function LimpiarDatosModal() {
    $("#tbmGrupo").val("");
    $("#__tbIdGrupo").val("");
    $("#__tbTextGrupo").val("");
    $("#__HD_URL_DOCUMENTO").val("");
    document.getElementById("btnSubirArchivo").value = null;
    $("#tbmGrupoHtml").hide();
    $("#infoSucesoModal").hide();
    $("#infoErrorModal").hide();
    $("#loadingSucessoModal").hide();

}

function Detalle(id) {

    $("#verPersonaModal").modal("show");
    //var url = $('#__URL_DETALLE_VEHICULO').val() + "/" + id;
    //var dato = {
    //};
    //realizarGet(url, dato, 'json', RespuestaDetalle, ErrorDetalle, 10000);
}

function RespuestaDetalle(data) {
    LimpiarDatosVehiculoModal();
    $("#verVehiculoModal").modal("show");
    $("#__HD_IdVehiculo").val(data.id);
    $("#tbmPlaca").val(data.placa);
    $("#tbmMarca").val(data.marca);
    $("#tbmModelo").val(data.modelo);
    $("#tbmColor").val(data.color);
    $("#tbmNota").val(data.anotacion);
}

function ErrorDetalle(data) {
    console.log(data);
}


function PreguntarEliminarCategoria(id) {
    MensajeCondicionalUno("¿Desea Eliminar?", "Se borrará del registro de personas", Eliminar, id);
}


function Eliminar(id) {

    var url = $('#__URL_ELIMINAR').val() + "/" + id;
    var dato = {
    };
    realizarGet(url, dato, 'json', RespuestaEliminar, ErrorEliminar, 10000);
}

function RespuestaEliminar(data) {
    $("#loadingSucessoModal").hide();
    MensajeAlerta("¡Se eliminó correctamente! ", "success");
    $('#dataTableProducto').DataTable().ajax.reload();
    LimpiarDatosModal();
}

function ErrorEliminar(data) {
    console.log(data);
}


function LimpiarDatosVehiculoModal() {
    $("#tbmPlaca").val("");
    $("#tbmMarca").val("");
    $("#tbmModelo").val("");
    $("#tbmColor").val("");


    //$("#tbmDni").val("");
    //$("#tbmNombres").val("");
    //$("#tbmPaterno").val("");
    //$("#tbmMaterno").val("");
    //$("#tbmNacimiento").val("");
    //$("#tbmSexo").val("");
    //$("#tbmTelefono").val("");
    //$("#tbmCorreo").val("");
    //$("#tbmDireccion").val("");
    //$("#tbmDistrito").val("");
    //$("#tbmProvincia").val("");
    //$("#tbmDeparmento").val("");
    //$("#tbmNivelSocioeconomico").val("");
    //$("#tbmBienCrediticio").val("");
    //$("#tbmTieneTarjeta").val("");
    //$("#tbmTieneVehiculo").val("");
    //$("#tbmEstaPlanilla").val("");
    //$('#DataCrediticio').html("");
    //$("#tbmDescripcion").val("");
    //$("#tbmDescripcion").val("");
    //$("#tbmNota").val("");
    $("#informacion-tab").trigger("click");
}



function BuscarGrupos() {
    var url = $('#__URL_LISTAR_GRUPO').val();
    var dato = {
    };
    realizarGet(url, dato, 'json', RespuestaBuscarGrupos, ErrorBuscarGrupos, 10000);
}

function RespuestaBuscarGrupos(data) {
    for (var i = 0; i < data.length; i++) {
        $("#ddlGrupo").append('<option value="' + data[i].id + '">' + data[i].nombre + '</option>');
    }
}
function ErrorBuscarGrupos(data) {
    console.log(data);
}

/************************ AUTOCOMPLETAR GRUPO******************************* */
function configurarBusquedaGrupo() {
    var urlData = $("#__URL_BUSCAR_GRUPO").val();
    var docs = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('ocurrencia'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: urlData + '/%QUERY',
            wildcard: '%QUERY'
        }
    });

    $('#tbmGrupo').typeahead(null, {
        name: 'docs',
        display: 'nombre',
        source: docs
    });

    $('#tbmGrupo').bind('typeahead:select', function (ev, suggestion) {
        $('#__tbIdGrupo').val(suggestion.id);
        $('#__tbTextGrupo').val(suggestion.nombre);
    });
}

function GenerarExcel() {
    var parametros = "?a=1";
    parametros += $("#tbPlacaBuscar").val().length > 0 ? "&dni=" + $("#tbPlacaBuscar").val() : "";
    parametros += $("#tbMarca").val().length > 0 ? "&nombreCompleto=" + $("#tbMarca").val() : "";
    parametros += $("#tbModelo").val().length > 0 ? "?sexo=" + $("#tbModelo").val() : "";
    parametros += $("#ddlGrupo").val().length > 0 ? "&idGrupo=" + $("#ddlGrupo").val() : "";
    window.open($('#__URL_GENERAR_EXCEL').val() + parametros, '_blank');

}

function countChars(obj) {
    var maxLength = 2000;
    var strLength = obj.value.length;
    var charRemain = (maxLength - strLength);
    $("#charNum").show();
    if (charRemain <= 0) {
        document.getElementById("charNum").innerHTML = '<span style="color: red;font-size: 11px;">Ha superado el limite de ' + maxLength + ' caracteres</span>';
    } else {
        document.getElementById("charNum").innerHTML = charRemain + ' caracteres restantes';
    }
}

function ActualizarNota() {
    $("#loadingNota").show();
    $("#succesNota").hide();
    $("#errorNota").hide();
    var url = $('#__URL_EDITAR_ANOTACION').val();
    var dato = {
        id: $("#__HD_IdVehiculo").val(),
        anotacion: $("#tbmNota").val(),
    };
    realizarPost(url, dato, 'json', RespuestaActualizarNota, ErrorActualizarNota, 10000);

}

function RespuestaActualizarNota(data) {
    $("#loadingNota").hide();
    $("#succesNota").show();
    setTimeout('LimpiarHtmlNota()', 6000);
}

function ErrorActualizarNota(data) {
    $("#loadingNota").hide();
    $("#errorNota").show();
    setTimeout('LimpiarHtmlNota()', 6000);
}

function LimpiarHtmlNota() {
    $("#loadingNota").hide();
    $("#succesNota").hide();
    $("#errorNota").hide();
}

function MensajeCondicionalUno(titulo, detalle, fun, uno) {
    Swal.fire({
        title: titulo,
        text: detalle,
        icon: "warning",
        showCancelButton: true,
        cancelButtonText: "Cancelar",
        confirmButtonText: "Sí"
    }).then(function (result) {
        if (result.value) {
            fun(uno);
        }
    });
}