﻿$(document).ready(function () {
    ConfiguracionSitio();
});

function ConfiguracionSitio() {

    $('#txtDocumento').keypress(function () {
        SoloNumeros1_9();
    });

    $('#txtTelefono').keypress(function () {
        SoloNumeros1_9();
    });


    var tipo = $('#___ID_TIPO').val();

    //$('#ddlTipo').find(":selected").val(tipo);
    //  //$("#ddlTipo select").val(tipo);
    $("#ddlTipo").val(tipo).change();

    if (tipo == 100) {
        $(".juridica").hide();
        $(".natural").show();
    }
    else {
        $(".natural").hide();
        $(".juridica").show();
    }

    $('#ddlTipo').on('change', function () {

        if (this.value == 100) {
            HideMensajes();
            LimpiarAgregar();
            $(".juridica").hide();
            $(".natural").show();
        }

        if (this.value == 101) {
            HideMensajes();
            LimpiarAgregar();
            $(".natural").hide();
            $(".juridica").show();
        }

    });

    $("#btnGrabar").click(function () {

        if ($('#___ID').val() != null) {
            EditarPersona();
        }
        else {
            RegistroPersona();
        }

    });

    $("#btnLimpiar").click(function () {
        $("#txtDocumento").val("");
        $("#txtNombres").val("");
        $("#txtApellidoPaterno").val("");
        $("#txtApellidoMaterno").val("");
        $("#txtRazonSocial").val("");
        $('#tablePersonas').DataTable().ajax.reload();
    });




    $('#ddlUbigeoDepatamento').change(function () {
        $("#ddlUbigeoProvincias").html('<option value="" selected>Todos</option>');
        $("#ddlUbigeoDistritos").html('<option value="" selected>Todos</option>');
        if ($('#ddlUbigeoDepatamento').val().length > 0) {
            BuscarProvincias();
        } else {
            $("#ddlUbigeoProvincias").prop("disabled", true);
            $("#ddlUbigeoDistritos").prop("disabled", true);
        }
    });
    $('#ddlUbigeoProvincias').change(function () {
        $("#ddlUbigeoDistritos").html('<option value="" selected>Todos</option>');
        if ($('#ddlUbigeoProvincias').val().length > 0) {
            BuscarDistritos();
        } else {
            $("#ddlUbigeoDistritos").prop("disabled", true);
        }
    });



}

function BuscarProvincias() {
    var url = $('#__URL_BUSCAR_PROVINCIAS').val();
    var dato = {
        "nombre": $("#ddlUbigeoDepatamento").val()
    };
    realizarPost(url, dato, 'json', RespuestaBuscarProvincias, ErrorBuscarProvincias, 10000);
}

function RespuestaBuscarProvincias(data) {
    $("#ddlUbigeoProvincias").prop("disabled", false);
    $("#ddlUbigeoDistritos").prop("disabled", true);
    for (var i = 0; i < data.length; i++) {
        $("#ddlUbigeoProvincias").append('<option value="' + data[i].nombre + '">' + data[i].nombre + '</option>');
    }
}

function ErrorBuscarProvincias(data) {
    MensajeAlerta("Ocurrio un error en busqueda de ubigeo, por favor comunicarse con soporte", "error");
}

function BuscarDistritos() {
    var url = $('#__URL_BUSCAR_DISTRITOS').val();
    var dato = {
        "nombre": $("#ddlUbigeoProvincias").val()
    };
    realizarPost(url, dato, 'json', RespuestaBuscarDistritos, ErrorBuscarProvincias, 10000);
}

function RespuestaBuscarDistritos(data) {
    $("#ddlUbigeoDistritos").prop("disabled", false);
    for (var i = 0; i < data.length; i++) {
        $("#ddlUbigeoDistritos").append('<option value="' + data[i].nombre + '">' + data[i].nombre + '</option>');
    }

}


function RegistroPersona() {

    if (validarCamposPersona() == 0) {
        $("#loadingSucessoModal").show();
        $("#infoSucesoModal").hide();
        var url = $('#__URL_REGISTRO_PERSONA').val();

        var dato = {
            "idTipo": $('#ddlTipo').find(":selected").val(),
            "documento": $("#txtDocumento").val(),
            "telefono": $("#txtTelefono").val(),
            "correo": $("#txtCorreo").val(),
            "nombres": $("#txtNombres").val(),
            "razonSocial": $("#txtRazonSocial").val(),
            "paterno": $("#txtApellidoPaterno").val(),
            "materno": $("#txtApellidoMaterno").val(),
            "direccion": $("#txtDireccion").val(),
            "departamento": $('#ddlUbigeoDepatamento').find(":selected").text(),
            "provincia": $('#ddlUbigeoProvincias').find(":selected").text(),
            "distrito": $('#ddlUbigeoDistritos').find(":selected").text()
        };
        realizarPost(url, dato, 'json', RespuestaRegistroPersona, RegistroPersonaError, 10000);
    }

}


function validarCamposPersona() {

    var salida = 0;

    if (($('#ddlTipo').find(":selected").val() == 100 && $("#txtDocumento").val().length == 8) || ($('#ddlTipo').find(":selected").val() == 101 && $("#txtDocumento").val().length == 11)) {
        $("#txtDocumentoHtml").hide();
        //salida = true;
    }
    else {
        salida++;
        $("#txtDocumentoHtml").html("Ingrese un nro° de documento válido");
        $("#txtDocumentoHtml").show();
        $("#txtDocumento").focus();
    }



    if ($('#ddlTipo').find(":selected").val() == 100) {

        if ($("#txtNombres").val().length !== 0) {
            $("#txtNombresHtml").hide();
            //salida = true;
        }
        else {
            salida++;
            $("#txtNombresHtml").html("Ingrese un valor válido");
            $("#txtNombresHtml").show();
            $("#txtNombres").focus();
        }

        if ($("#txtApellidoPaterno").val().length !== 0) {
            $("#txtApellidoPaternoHtml").hide();
            //salida = true;
        }
        else {
            salida++;
            $("#txtApellidoPaternoHtml").html("Ingrese un valor válido");
            $("#txtApellidoPaternoHtml").show();
            $("#txtApellidoPaterno").focus();
        }

        if ($("#txtApellidoMaterno").val().length !== 0) {
            $("#txtApellidoMaternoHtml").hide();
            //salida = true;
        }
        else {
            salida++;
            $("#txtApellidoMaternoHtml").html("Ingrese un valor válido");
            $("#txtApellidoMaternoHtml").show();
            $("#txtApellidoMaterno").focus();
        }

    }



    if ($('#ddlTipo').find(":selected").val() == 101) {
        if ($("#txtRazonSocial").val()) {
            $("#txtRazonSocialHtml").hide();
            //salida = true;
        }
        else {
            salida++;
            $("#txtRazonSocialHtml").html("Ingrese un valor válido");
            $("#txtRazonSocialHtml").show();
            $("#txtRazonSocial").focus();
        }
    }

    if ($("#txtCorreo").val().length !== 0) {

        //$('#tbmCorreo').keypress(function () {
        if (!ValidarEmail($('#txtCorreo').val())) {
            salida++;
            $("#txtCorreoHtml").html("Ingrese un correo válido");
            $("#txtCorreoHtml").show();
        }
        else {
            $("#txtCorreoHtml").html("");
            $("#txtCorreoHtml").hide();
        }
        //});

        //salida = true;
    }
    else {
        salida++;
        $("#txtCorreoHtml").html("Ingrese un correo válido");
        $("#txtCorreoHtml").show();
        $("#txtCorreo").focus();
    }


    if ($("#txtTelefono").val().length !== 0) {
        $("#txtTelefonoHtml").hide();
        //salida = true;
    }
    else {
        salida++;
        $("#txtTelefonoHtml").html("Ingrese un valor válido");
        $("#txtTelefonoHtml").show();
        $("#txtTelefono").focus();
    }

    if ($("#txtDireccion").val().length !== 0) {
        $("#txtDireccionHtml").hide();
        //salida = true;
    }
    else {
        salida++;
        $("#txtDireccionHtml").html("Ingrese un valor válido");
        $("#txtDireccionHtml").show();
        $("#txtDireccion").focus();
    }

    if ($('#ddlUbigeoDepatamento').find(":selected").val().length !== 0) {
        $("#txtDepartamentoHtml").hide();
        //salida = true;
    }
    else {
        salida++;
        $("#txtDepartamentoHtml").html("Ingrese un departamento válido");
        $("#txtDepartamentoHtml").show();
        //$("#txtDepartamento").focus();
    }

    if ($('#ddlUbigeoProvincias').find(":selected").val().length !== 0) {
        $("#txtProvinciaHtml").hide();
        //salida = true;
    }
    else {
        salida++;
        $("#txtProvinciaHtml").html("Ingrese una provincia válida");
        $("#txtProvinciaHtml").show();
        //$("#txtProvincia").focus();
    }

    if ($('#ddlUbigeoDistritos').find(":selected").val().length !== 0) {
        $("#txtDistritoHtml").hide();
        //salida = true;
    }
    else {
        salida++;
        $("#txtDistritoHtml").html("Ingrese un distrito válido");
        $("#txtDistritoHtml").show();
        //$("#txtDistrito").focus();
    }


    return salida;
}

function HideMensajes() {
    $("#txtDocumentoHtml").hide();
    $("#txtNombresHtml").hide();
    $("#txtApellidoPaternoHtml").hide();
    $("#txtApellidoMaternoHtml").hide();
    $("#txtRazonSocialHtml").hide();
    $("#txtCorreoHtml").hide();
    $("#txtTelefonoHtml").hide();
    $("#txtDireccionHtml").hide();
    $("#txtDepartamentoHtml").hide();
    $("#txtProvinciaHtml").hide();
    $("#txtDistritoHtml").hide();
}

function LimpiarAgregar() {
    $("#txtDocumento").val("");
    $("#txtNombres").val("");
    $("#txtApellidoPaterno").val("");
    $("#txtApellidoMaterno").val("");
    $("#txtRazonSocial").val("");
    $("#txtCorreo").val("");
    $("#txtTelefono").val("");
    $("#txtDireccion").val("");
    $('#ddlUbigeoDepatamento').val("").change();;
    $("#ddlUbigeoProvincias").html('<option value="" selected>Todos</option>');
    $("#ddlUbigeoDistritos").html('<option value="" selected>Todos</option>');
    $("#ddlUbigeoProvincias").prop("disabled", true);
    $("#ddlUbigeoDistritos").prop("disabled", true);
}






function RespuestaRegistroPersona(dato) {
    $("#loadingSucessoModal").hide();
    //$("#infoSucesoModal").show();

    MensajeAlerta("¡Se registró correctamente! ", "success");

    //setTimeout(function () {
    //    location.href = $('#__URL_PRINCIPAL').val();
    //}, 4000);


}

function RegistroPersonaError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucessoModal").hide();
    //alert("error");
    console.log(jqXHR);
    MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
}


function EditarPersona() {

    if (validarCamposPersona() == 0) {
        $("#loadingSucessoModal").show();
        $("#infoSucesoModal").hide();
        var url = $('#__URL_EDITAR_PERSONA').val();

        var dato = {
            "id": $('#___ID').val(),
            "idTipo": $('#ddlTipo').find(":selected").val(),
            "documento": $("#txtDocumento").val(),
            "telefono": $("#txtTelefono").val(),
            "correo": $("#txtCorreo").val(),
            "nombres": $("#txtNombres").val(),
            "razonSocial": $("#txtRazonSocial").val(),
            "paterno": $("#txtApellidoPaterno").val(),
            "materno": $("#txtApellidoMaterno").val(),
            "departamento": $('#ddlUbigeoDepatamento').find(":selected").val(),
            "provincia": $('#ddlUbigeoProvincias').find(":selected").val(),
            "distrito": $('#ddlUbigeoDistritos').find(":selected").val(),
            "direccion": $("#txtDireccion").val()
        };
        realizarPost(url, dato, 'json', RespuestaEditarPersona, EditarPersonaError, 10000);
    }

}

function RespuestaEditarPersona(dato) {
    $("#loadingSucessoModal").hide();
    //$("#infoSucesoModal").show();

    MensajeAlerta("¡Se guardó correctamente! ", "success");

    //setTimeout(function () {
    //    location.href = $('#__URL_PRINCIPAL').val();
    //}, 4000);


}

function EditarPersonaError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucessoModal").hide();
    //alert("error");
    console.log(jqXHR);
    MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
}

