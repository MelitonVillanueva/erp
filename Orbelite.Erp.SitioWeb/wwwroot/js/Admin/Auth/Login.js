﻿$(document).ready(function () {
    controles();

});
function controles() {
    $('#btnIngresarConApp, #btnGenerarNewQr').click(function () {
        GenerarQr();
    });

}


function GenerarQr() {
    $("#__ID_QR_GENERADO").val("");
    var url = $("#__HD_QR_GENERAR").val();
    var datos = {
    }
    realizarJsonGet(url, datos, GenerarQrSuceso, GenerarQrError)
}

function GenerarQrSuceso(data) {
    $("#exampleModalLoginQR").modal('show');
    $("#__ID_QR_GENERADO").val(data.mensaje);
    $('#img_qr').attr('src', 'data:image/jpeg;base64,' + data.id);
    verificarQrIniSesion();
}

function GenerarQrError(data) {
    console.log(data);
}

function EjecutarEscanerQr() {
    setTimeout(verificarQrIniSesion, 1500);
}

function verificarQrIniSesion() {
    var url = $("#__HD_LOGIN_QR").val() + $("#__ID_QR_GENERADO").val();
    var datos = {
    }
    realizarJsonGet(url, datos, verificarQrIniSesionSuceso, verificarQrIniSesionError)
}

function verificarQrIniSesionSuceso(data) {
    $("#__ID_QR_GENERADO").val("");
    location.href = $('#__HD_ADMIN').val();
}

function verificarQrIniSesionError(data) {
    EjecutarEscanerQr();
}