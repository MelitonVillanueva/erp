﻿$(document).ready(function () {
    ConfiguracionPlaca();
});

function ConfiguracionPlaca() {
    $("#btnBuscar").click(function () {
        BuscarTelefono();
    });
    $('#txtTelefono').keydown(function () {
        $("#txtTelefonoHtml").hide();
    });
}

function BuscarTelefono() {
    if ($("#txtTelefono").val().length !== 0) {
        Limpiar();
        $("#loadingSucesso").show();
        $("#msjError").hide();
        var url = $("#_HD_URL_CONSULTA_TELEFONO").val() + $("#txtTelefono").val();
        var dato = {}
        realizarGet(url, dato, 'json', RespuestaBuscarTelefono, BuscarTelefonoError, 10000);
    } else {
        $("#txtTelefonoHtml").html("Ingrese el número de teléfono");
        $("#txtTelefonoHtml").show();
        $("#txtTelefono").focus();
    }

}

function RespuestaBuscarTelefono(data) {
    $("#loadingSucesso").hide();
    $("#resultadoTelefonoHtml").show();  
    console.log(data);
    LlenarPropietarios(data);
}

function BuscarTelefonoError(data) {
    $("#loadingSucesso").hide();
    $("#msjError").show();
    console.log(data);
}

var tablaDataCategoria = null;
function LlenarPropietarios(data) {
    if (tablaDataCategoria) {
        tablaDataCategoria.destroy();
        tablaDataCategoria = null;
    }
    var table = $('#dataTablePropietario').DataTable();
    table.destroy();
    var html = "";
    for (i = 0; i < data.length; i++) {
        html += '<tr>';
        html += "<td>" + data[i].documento + "</td>";
        var urlVerMas = '<a href="' + $("#__URL_BUSQUEDA_PERSONA").val() + "/" + data[i].documento+ '" target="_blank">Ver más</a>';
        if (data[i].documento.length > 8) {
            urlVerMas = "";
        }
       
        html += '<td>' + urlVerMas + '</td>';
        html += "</tr>";
    }
    $("#dataTablePropietario tbody").html(html);
    tablaDataCategoria = $('#dataTablePropietario').DataTable({
        "searching": false,
        "bLengthChange": false,
        "info": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        "aoColumnDefs": [
            {
                'bSortable': false,
                'aTargets': [0]
            }
        ],
    });
}

function Limpiar() {
}