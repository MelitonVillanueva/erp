﻿$(document).ready(function () {
    ConfiguracionPlaca();
});

function ConfiguracionPlaca() {
    $("#btnBuscar").click(function () {
        BuscarNombres();
    });
    $('#txtNombres').keydown(function () {
        $("#txtNombresHtml").hide();
    });
    $('#txtApellidoPaterno').keydown(function () {
        $("#txtApellidoPaternoHtml").hide();
    });
    $('#txtApellidoMaterno').keydown(function () {
        $("#txtApellidoMaternoHtml").hide();
    });
}

function BuscarNombres() {
    if ($("#txtApellidoPaterno").val().length !== 0) {
        if ($("#txtApellidoMaterno").val().length !== 0) {
            Limpiar();
            $("#loadingSucesso").show();
            $("#msjError").hide();
            var url = $("#_HD_URL_CONSULTA_NOMBRES").val();
            var dato = {
                "nombres": $("#txtNombres").val(),
                "paterno": $("#txtApellidoPaterno").val(),
                "materno": $("#txtApellidoMaterno").val()
            }
            realizarPost(url, dato, 'json', RespuestaBuscarNombres, BuscarNombresError, 10000);
        } else {
            $("#txtApellidoMaternoHtml").html("Ingrese el A. Materno");
            $("#txtApellidoMaternoHtml").show();
            $("#txtApellidoMaterno").focus();
        }
    } else {
        $("#txtApellidoPaternoHtml").html("Ingrese el A. Paterno");
        $("#txtApellidoPaternoHtml").show();
        $("#txtApellidoPaterno").focus();
    }

}

function RespuestaBuscarNombres(data) {
    $("#loadingSucesso").hide();
    $("#resultadoTelefonoHtml").show();
    console.log(data);
    LlenarPropietarios(data);
}

function BuscarNombresError(data) {
    $("#loadingSucesso").hide();
    $("#msjError").show();
    console.log(data);
}

var tablaDataCategoria = null;
function LlenarPropietarios(data) {
    if (tablaDataCategoria) {
        tablaDataCategoria.destroy();
        tablaDataCategoria = null;
    }
    var table = $('#dataTablePropietario').DataTable();
    table.destroy();
    var html = "";
    for (i = 0; i < data.length; i++) {
        html += '<tr>';
        html += "<td>" + data[i].dni + "</td>";
        html += "<td>" + data[i].nombres + "</td>";
        html += "<td>" + data[i].paterno + "</td>";
        html += "<td>" + data[i].materno + "</td>";
        var urlVerMas = '<a href="' + $("#__URL_BUSQUEDA_PERSONA").val() + "/" + data[i].dni + '" target="_blank">Ver más</a>';
        html += '<td>' + urlVerMas + '</td>';
        html += "</tr>";
    }
    $("#dataTablePropietario tbody").html(html);
    tablaDataCategoria = $('#dataTablePropietario').DataTable({
        "searching": false,
        "bLengthChange": false,
        "info": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        "aoColumnDefs": [
            {
                'bSortable': false,
                'aTargets': [0]
            }
        ],
    });
}

function Limpiar() {
}