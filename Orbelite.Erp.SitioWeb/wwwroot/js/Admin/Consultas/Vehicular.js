﻿$(document).ready(function () {
    ConfiguracionPlaca();
});

function ConfiguracionPlaca() {
    $("#btnBuscar").click(function () {
        BuscarPlaca();
    });
    $('#txtPlaca').keydown(function () {
        $("#txtPlacaHtml").hide();
    });
}

function BuscarPlaca() {
    if ($("#txtPlaca").val().length !== 0) {
        Limpiar();
        $("#loadingSucesso").show();
        $("#msjError").hide();
        var url = $("#_HD_URL_CONSULTA_PLACA").val() + $("#txtPlaca").val();
        var dato = {}
        realizarGet(url, dato, 'json', RespuestaBuscarPlaca, BuscarPlacaError, 10000);
    } else {
        $("#txtPlacaHtml").show();
        $("#txtPlaca").focus();
    }
    
}

function RespuestaBuscarPlaca(data) {
    $("#loadingSucesso").hide();
    $("#resultadoPlacaHtml").show();    
    $("#tbgPlaca").val(data.placa);
    $("#tbgMarca").val(data.marca);
    $("#tbgModelo").val(data.modelo);
    $("#tbgColor").val(data.color);
    $("#tbgEstado").val(data.estado);
    $("#tbgAnio").val(data.anio);
    $("#tbgNroMotor").val(data.nroMotor);
    $("#tbgSerie").val(data.nroSerie);
    $("#tbgSede").val(data.sede);

    $("#tbsCompania").val(data.soat.compania);
    $("#tbsDesde").val(data.soat.inicio);
    $("#tbsClase").val(data.soat.claseVehiculo);
    $("#tbsTipo").val(data.soat.tipo);
    $("#tbsEstado").val(data.soat.estado);
    $("#tbsHasta").val(data.soat.fin);
    $("#tbsUso").val(data.soat.usoVehiculo);
    LlenarPropietarios(data.propietarios);
}

function BuscarPlacaError(data) {
    $("#loadingSucesso").hide();
    $("#msjError").show();
    console.log(data);
}

var tablaDataCategoria = null;
function LlenarPropietarios(data) {
    if (tablaDataCategoria) {
        tablaDataCategoria.destroy();
        tablaDataCategoria = null;
    }
    var table = $('#dataTablePropietario').DataTable();
    table.destroy();
    var html = "";
    for (i = 0; i < data.length; i++) {
        html += '<tr>';
        html += "<td>" + data[i].nroDocumento + "</td>";
        var nombres = data[i].tipoDocumento === "1" ? data[i].nombres + " " + data[i].apellidoPaterno + " " + data[i].apellidoMaterno : data[i].razonSocial;
        html += "<td>" + nombres + "</td>";
        var sexo = data[i].sexo === "1" ? "Masculino" : "Femenino";
        html += '<td>' + sexo + '</td>';
        html += '<td>' + data[i].direccion + '</td>';
        html += '<td>' + data[i].distrito + '</td>';
        html += '<td>' + data[i].departamento + '</td>';
        html += "</tr>";
    }
    $("#dataTablePropietario tbody").html(html);
    tablaDataCategoria = $('#dataTablePropietario').DataTable({
        "searching": true,
        "bLengthChange": false,
        "info": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        "aoColumnDefs": [
            {
                'bSortable': false,
                'aTargets': [0]
            }
        ],
    });
}

function Limpiar() {
    $("#tbgPlaca").val("");
    $("#tbgMarca").val("");
    $("#tbgModelo").val("");
    $("#tbgColor").val("");
    $("#tbgEstado").val("");
    $("#tbgAnio").val("");
    $("#tbgNroMotor").val("");
    $("#tbgSerie").val("");
    $("#tbgSede").val("");

    $("#tbsCompania").val("");
    $("#tbsDesde").val("");
    $("#tbsClase").val("");
    $("#tbsTipo").val("");
    $("#tbsEstado").val("");
    $("#tbsHasta").val("");
    $("#tbsUso").val("");
}