﻿var map;
var markers = [];

var sucesoCrediticio = false;
var sucesoSunedu = false;
var sucesoTwitter = false;
$(document).ready(function () {
    controles();

});


function controles() {
    $('#btnBuscar').click(function () {
        BuscarInfo();
    });

    $('#cbxFrecuencia').change(function () {
        $("#cbxFrecuenciaHtml").hide();
    });

    $('#cbxBanco').change(function () {
        $("#cbxBancoHtml").hide();
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var idTarget = $(e.target).attr("id");
        if (idTarget === "Credicticio-tab") {
            BuscarHistorialCrediticio();
        } else if (idTarget === "Sunedu-tab") {
            BuscarSunedu();
        } else if (idTarget === "Twitter-tab") {
            BuscarTwitter();
        }

    });

    if ($("#__DOCUMENTO_BUSQUEDA").val().length > 0) {
        $("#tbDni").val($("#__DOCUMENTO_BUSQUEDA").val());
        $("#btnBuscar").click();
    }
}

function Limpiar() {
    $("#resultadoBusquedaHtml").hide();
    $("#msjError").hide();
    sucesoCrediticio = false;
    sucesoSunedu = false;
    sucesoTwitter = false;

    $("#idListTwitter").html("");
    $("#idListTwitter").html("");
    document.getElementById("Generales-tab").click();
}


function BuscarInfo() {
    Limpiar();
    if ($("#tbDni").val().length > 0) {

        $("#resultadoBusquedaHtml").hide();
        $("#loadingSucesso").show();
        var url = $('#__URL_BUSQUEDA_DNI').val() + $("#tbDni").val();
        var dato = {
        };
        realizarGet(url, dato, 'json', RespuestaBuscarInfoPerfil, BuscarInfoPerfilError, 10000);
    } else {
        $("#tbDniHtml").focus();
        $("#tbDniHtml").show();
        $("#tbDniHtml").html("El número de DNI es requerido");
    }

}

function RespuestaBuscarInfoPerfil(dato) {
    $("#loadingSucesso").hide();
    $("#resultadoBusquedaHtml").show();
    $("#txtDni").val(dato.dni);
    $("#txtNombres").val(dato.nombres);
    $("#txtApellidoPaterno").val(dato.apellidoPaterno);
    $("#txtApellidoMaterno").val(dato.apellidoMaterno);
    $("#txtFechaNacimiento").val(dato.fechaNacimiento);
    $("#txtDireccion").val(dato.direccion);
    $("#txtDistrito").val(dato.distrito);
    $("#txtProvincia").val(dato.provincia);
    $("#txtDepartamento").val(dato.departamento);
    initMap();
    BuscarTelefonos(dato.dni);
}

function BuscarInfoPerfilError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucesso").hide();
    console.log(jqXHR);
    $("#resultadoBusquedaHtml").hide();
    $("#msjError").hide();
}


function initMap() {
    map = new google.maps.Map(document.getElementById('map_container_google_direccion'), {
        zoom: 12,
        center: { lat: -12.0431800, lng: -77.0282400 }
    });
    var geocoder = new google.maps.Geocoder();
    geocodeAddress(geocoder, map)
}

function geocodeAddress(geocoder, resultsMap) {
    var address = $('#txtDistrito').val() + " " + $('#txtProvincia').val() + " " + $('#txtDepartamento').val() + " " + $('#txtDireccion').val();
    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: resultsMap,
                position: results[0].geometry.location
            });
        }
    });
}

function BuscarHistorialCrediticio() {
    if (sucesoCrediticio) { return true; }
    $("#loadingSucesso").show();
    $("#msjCrediticio").hide();
    var url = $('#__URL_BUSCAR_HISTORIAL_CREDITICIO').val();
    var dato = {
        Documento: $("#tbDni").val(),
        Tipo: 100
    };
    realizarPost(url, dato, 'json', RespuestaBuscarHistorialCrediticio, BuscarHistorialCrediticioError, 10000 * 6);
}

function RespuestaBuscarHistorialCrediticio(data) {
    console.log(data);
    sucesoCrediticio = true;
    $("#loadingSucesso").hide();
    $('#lbMes1').html("");
    $('#lbMes2').html("");
    $('#lbMes3').html("");

    $('#btnColor1').html("");
    $('#btnColor2').html("");
    $('#btnColor3').html("");

    var UrlDetallePrimero = $("#__URL_BUSCAR_CREDITICIO_DETALLE_PRIMERO").val();
    var UrlDetalleSegundo = $("#__URL_BUSCAR_CREDITICIO_DETALLE_SEGUNDO").val();
    var UrlDetalleTercero = $("#__URL_BUSCAR_CREDITICIO_DETALLE_TERCERO").val();

    if (data === null) {
        $("#msjCrediticio").show();
        return;
    }

    if (data.length === 0) {
        $("#msjCrediticio").show();
    }

    var btn_red = '<button id="btnRed" class="retro-button red-button"></button>';
    var btn_yellow = '<button id="btnYellow" class="retro-button yellow-button"></button>';
    var btn_gray = '<button id="btnGray" class="retro-button"></button>';
    var btn_green = '<button id="btnGreen" class="retro-button green-button"></button>';


    for (var i = 0; i < data.length; i++) {
        //$("#Credicticio").show();
        var dato = data[i];
        var botonActual = i + 1;

        $('#lbMes' + botonActual).html('<label for="">' + dato.descripcion + '</label>');

        var pNormal = obtenerValorONumero(dato.normal);
        var pDeficiente = obtenerValorONumero(dato.deficiente);
        var pDudoso = obtenerValorONumero(dato.dudoso);
        var pPerdida = obtenerValorONumero(dato.perdida);
        var pProblemasPotenciales = obtenerValorONumero(dato.problemasPotenciales);



        if (pNormal === 0 && pDeficiente === 0 && pDudoso === 0 && pPerdida === 0 && pProblemasPotenciales === 0) {
            $('#btnColor' + botonActual).append(btn_gray);
            $('#btnGray').attr('id', 'btn' + botonActual);
            continue;
        }

        if (pNormal >= 70) {
            $('#btnColor' + botonActual).append(btn_green);
            $('#btnGreen').attr('id', 'btn' + botonActual);
            continue;
        }

        if (pNormal >= 40 && pNormal < 70) {
            $('#btnColor' + botonActual).append(btn_yellow);
            $('#btnYellow').attr('id', 'btn' + botonActual);
            continue;
        }

        if (pNormal < 40) {
            $('#btnColor' + botonActual).append(btn_red);
            $('#btnRed').attr('id', 'btn' + botonActual);
            continue;
        }
        $('#btnColor' + botonActual).append(btn_gray);
        $('#btnGray').attr('id', 'btn' + botonActual);
    }


    var EnClick = false;
    $('#Credicticio-tab').click(function () {
        if (EnClick) { return true; }
        EnClick = true;
        if (data.length > 0) {
            PintarHistorialCrediticio(data[0]);
            $('#btn3').removeClass('actived');
            $('#btn2').removeClass('actived');
            $('#btn1').addClass('actived');
            realizarPost(UrlDetallePrimero, { Id: data[0].id }, 'json', RespuestaCrediticioDetalle, null);
        }
    });



    $('#btn1').click(function () {
        if (data.length > 0) {
            PintarHistorialCrediticio(data[0]);
            $('#btn3').removeClass('actived');
            $('#btn2').removeClass('actived');
            $('#btn1').addClass('actived');
            realizarPost(UrlDetallePrimero, { Id: data[0].id }, 'json', RespuestaCrediticioDetalle, null);
        }
    });

    $('#btn2').click(function () {
        if (data.length > 1) {
            PintarHistorialCrediticio(data[1]);
            $('#btn1').removeClass('actived');
            $('#btn3').removeClass('actived');
            $('#btn2').addClass('actived');
            realizarPost(UrlDetalleSegundo, { Id: data[1].id }, 'json', RespuestaCrediticioDetalle);
        }
    });

    $('#btn3').click(function () {
        if (data.length > 2) {
            PintarHistorialCrediticio(data[2]);
            $('#btn1').removeClass('actived');
            $('#btn2').removeClass('actived');
            $('#btn3').addClass('actived');
            realizarPost(UrlDetalleTercero, { Id: data[2].id }, 'json', RespuestaCrediticioDetalle);
        }
    });

}

function PintarHistorialCrediticio(data) {

    $("#DataHistorialCrediticio tbody").html("");

    $('#DataHistorialCrediticio').append('<tr><td> % </td>'
        + '<td class="text-center"><span class="label label-inline label-light-dark font-weight-bold normal"> ' + data.normal + '</span></td>'
        + '<td class="text-center"><span class="label label-inline label-light-dark font-weight-bold problemas-potenciales"> ' + data.problemasPotenciales + '</span></td>>'
        + '<td class="text-center"><span class="label label-inline label-light-dark font-weight-bold dudoso"> ' + data.dudoso + '</span></td>'
        + '<td class="text-center"><span class="label label-inline label-light-dark font-weight-bold deficiente"> ' + data.deficiente + '</span></td>'
        + ' <td class="text-center"><span class="label label-inline label-light-dark font-weight-bold perdidas"> ' + data.perdida + '</span></td> </tr> ');
}

function RespuestaCrediticioDetalle(data) {
    var html = "";
    for (i = 0; i < data.length; i++) {
        html += '<tr>';
        html += "<td> " + data[i].nombre + " </td>";
        html += "<td> " + data[i].tipoMoneda + " </td>";
        html += '<td class="text-center"> ' + data[i].saldo.toFixed(2) + "  </td>";
        html += '<td class="text-center"> ' + data[i].condicion + " </td>";
        html += '<td> ' + data[i].clasificacionNombre + " </td>";
        html += '<td> ' + data[i].tipoCreditoNombre + " </td>";
        html += "</tr>";
    }
    $("#tableDatalleHistorial tbody").html(html);
}


function BuscarHistorialCrediticioError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucesso").hide();
    console.log("no existe crediticio", jqXHR);
}

function obtenerValorONumero(valor) {
    if (!valor) {
        return 0;
    }

    return valor;
}


function BuscarSunedu() {
    if (sucesoSunedu) { return true; }
    var url = $('#__URL_BUSCAR_SUNEDU').val() + $("#tbDni").val();
    var dato = {
    };
    realizarGet(url, dato, 'json', RespuestaBuscarSunedu, BuscarSuneduError, 10000);
}

var tablaPorSunedu = null;
function RespuestaBuscarSunedu(data) {
    sucesoSunedu = true;
    if (tablaPorSunedu) {
        tablaPorSunedu.destroy();
        tablaPorSunedu = null;
    }
    var html = "";
    for (i = 0; i < data.length; i++) {
        html += '<tr>' +
            "<td>" + data[i].titulo + "</td>" +
            "<td>" + data[i].institucion + "</td>" +
            "<td>" + data[i].fecha + "</td>" +
            '</tr>';
    }

    $("#tableSunedu tbody").html(html);

    tablaPorSunedu = $('#tableSunedu').DataTable({
        "info": false,
        "searching": false,
        //"pageLength": 10,
        //"lengthMenu": [[10, -1], [10, "Todos"]],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
    });
}

function BuscarSuneduError(data) {

}


function BuscarTwitter() {
    if (sucesoTwitter) { return true; }
    var url = $('#__URL_OBTENER_TWITTER').val();
    var dato = {
        Nombres: $("#txtNombres").val(),
        Paterno: $("#txtApellidoPaterno").val(),
        Materno: $("#txtApellidoMaterno").val()
    };
    realizarPost(url, dato, 'json', RespuestaTwitter, TwitterError, 10000);
}
function RespuestaTwitter(data) {
    $("#idListTwitter").html("");
    sucesoTwitter = true;
    console.log(data);
    var texto = "";
    for (var i = 0; i < data.length; i++) {
        var ubicacion = "";
        if (data[i].ubicacion != "") {
            ubicacion = " - <span style='color:#a3ada3;'> <b>Ubicación:</b> " + data[i].ubicacion + "</span>";
        }
        texto += "<div style='background: #f9f9f9;margin: 6px 10px;border-radius:26px;'><a href='" + data[i].url + "' target='_blank'><img src='" + data[i].urlFoto + "' style='width: 60px;border-radius:50%;' /> <b>@" + data[i].userName + "</b>, " + data[i].nombre + " " + ubicacion + "</a></div>";
    }
    $("#idListTwitter").html(texto);
}
function TwitterError(data) {

}

function BuscarTelefonos(dni) {
    var url = $('#__URL_OBTENER_TELEFONOS').val() + dni;
    var dato = {};
    realizarGet(url, dato, 'json', RespuestaBuscarTelefonos, BuscarTelefonosError, 10000);
}
function RespuestaBuscarTelefonos(data) {
    console.log(data);
    var html = "";
    if (data.length > 0) {
        for (i = 0; i < data.length; i++) {
            html += '<tr>';
            html += "<td> " + data[i].operador + " </td>";
            html += "<td> " + data[i].telefono + " </td>";
            html += "</tr>";
        }
    } else {
        html += '<tr><td colspan="2" class="text-center"> No hay teléfono encontrado </td></tr>';
    }

    $("#tableTelefono tbody").html(html);
}
function BuscarTelefonosError(data) {
    console.log(data);
    var html = '<tr><td colspan="2"> No hay teléfono encontrado </td></tr>';
    $("#tableTelefono tbody").html(html);
}
/***************************MENSAJES ALERTA*********************************/
function MensajeAlerta(mensaje, tipo) {
    Swal.fire({
        text: mensaje,
        icon: tipo,
        buttonsStyling: true,
        confirmButtonText: "Cerrar",
        customClass: {
            confirmButton: "btn font-weight-bold btn-light"
        }
    });
}
function MensajeCondicional(titulo, detalle, fun) {
    Swal.fire({
        title: titulo,
        text: detalle,
        icon: "warning",
        showCancelButton: true,
        cancelButtonText: "Cancelar",
        confirmButtonText: "Sí"
    }).then(function (result) {
        if (result.value) {
            fun();
        }
    });
}
/***************************VALIDACION*********************************/
function SoloNumerosYGuion() {
    var key = window.event ? event.which : event.keyCode;
    if ((key < 48 || key > 57)) {
        if (key != 45) {
            event.preventDefault();
        }
    }
}

