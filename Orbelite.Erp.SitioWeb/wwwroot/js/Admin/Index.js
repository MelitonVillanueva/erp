﻿$(document).ready(function () {
    controles();

});
function controles() {
    BuscarTotales();
    $('#chkConTelefono, #ddlGrupo').change(function () {
        BuscarTotales();
    });


    FacturaGrillaLoad();


}

function BuscarTotales() {
    $("#loadingSucesso").show();
    var url = $('#__URL_BUSCAR_TOTALES').val();
    var dato = {
        //SoloUbicables: $('#chkConTelefono').is(':checked'),
        IdGrupo: $('#ddlGrupo').val(),
    };
    realizarPost(url, dato, 'json', RespuestaBuscarTotales, BuscarTotalesError, 10000);
}

function RespuestaBuscarTotales(data) {

    console.log("SALIDA DE DATA");
    console.log(data);

    $("#loadingSucesso").hide();
    $("#totalVehiculo").html(numeroConComas(redondearExp(data.total, 2)));

    $("#totalConSoat").html(numeroConComas(redondearExp(data.conSoat, 2)));
    $("#totalSinSoat").html(numeroConComas(redondearExp(data.sinSoat, 2)));
    $("#venceEsteMes").html(numeroConComas(redondearExp(data.vencenSoatEsteMes, 2)));
    $("#venceSiguienteMes").html(numeroConComas(redondearExp(data.vencenSoatSiguienteMes, 2)));

    $("#totalConPapeletas").html(numeroConComas(redondearExp(data.conPapeletas, 2)));
    $("#totalSinPapeletas").html(numeroConComas(redondearExp(data.sinPapepeletas, 2)));
    $("#totalSat").html(numeroConComas(redondearExp(data.papeletasSat, 2)));
    $("#totalCallao").html(numeroConComas(redondearExp(data.papeletasCallao, 2)));
    $("#totalOtros").html(numeroConComas(redondearExp(data.otrasPapeletas, 2)));

    //PrecioVisionGeneral(data.total);
}

function BuscarTotalesError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucesso").hide();
}

//function PrecioVisionGeneral(total) {
//    var precio = parseFloat($("#__PERSONA_PRECIO_GENERAL").val());
//    var costoTotal = parseFloat(total) * parseFloat(precio);
//    $("#precioInformacionGeneral").html(precio.toFixed(2));
//    $("#totalDeDatosInformacionGeneral").html(numeroConComas(redondearExp(total, 2)));
//    $("#precioTotalInformacionGeneral").html(numeroConComas(redondearExp(costoTotal, 2)));
//}
function redondearExp(numero, digitos) {
    function toExp(numero, digitos) {
        let arr = numero.toString().split("e");
        let mantisa = arr[0], exponente = digitos;
        if (arr[1]) exponente = Number(arr[1]) + digitos;
        return Number(mantisa + "e" + exponente.toString());
    }
    let entero = Math.round(toExp(Math.abs(numero), digitos));
    return Math.sign(numero) * toExp(entero, -digitos);
}

function numeroConComas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}



function FacturaGrillaLoad() {
    var options = $.extend(true, {}, defaults, {
        "aoColumnDefs": [
            {
                'bSortable': false,
                'aTargets': [0]
            },
        ],
        "order": [[0, "desc"]],
        "iDisplayLength": 5,
        "aLengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "All"]
        ],
        "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"ip>',
        "tableTools": {
            "aButtons": [
            ]
        },
        "bFilter": false,
        "bSort": true,
        "processing": true,
        "serverSide": true,
        "lengthMenu": [
            [10, 20, 50, 100, 150],
            [10, 20, 50, 100, 150]
        ],
        "pageLength": 10,
        "ajax": {
            "url": $('#__URL_BUSQUEDA').val(), // ajax source
            "type": "POST",
            "error": validarErrorGrilla,
            "data": function (d) {
                return $.extend({}, d, {
                });
            }
        },
        "columns": [
            {
                "render": function (data, type, row) {
                    return '<button onclick="DetalleFactura(\'' + row.idFactura + '\')" class="btn btn-sm btn-clean btn-icon mr-1" title="Editar"><i class="la la-edit"></i></button> ';
                    //+ '<button onclick="PreguntarEliminarCategoria(\'' + row.id + '\')" class="btn btn-sm btn-clean btn-icon" title="Eliminar"><i class="la la-trash"></i></button>'
                }
            },
            { "data": "numero" },
            {
                "render": function (data, type, row) {
                    let texto = "";
                    if (row.persona != null) {
                        texto = row.persona.documento;
                    }
                    return texto;
                }
            },
            {
                "render": function (data, type, row) {
                    let texto = "";
                    if (row.persona != null) {
                        texto = row.persona.razonSocial;
                    }
                    return texto;
                }
            },
            { "data": "fechaVencimientoCadena" },
            { "data": "fechaEmisionCadena" },
            //{ "data": "idMoneda" },
            {
                "render": function (data, type, row) {
                    let texto = "DOLAR";
                    if (row.idMoneda == 1) {
                        texto = "NUEVO SOL";
                    }
                    return texto;
                }
            },
            { "data": "igv" },
            { "data": "subTotalVentas" }

        ],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
    });

    $('#dataTableFactura').DataTable(options);
}

function DetalleFactura(id) {

    location.href = $('#__URL_DETALLE_FACTURA').val() + "/" + id;

}