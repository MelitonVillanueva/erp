﻿var operacionActualizar = false;

$(document).ready(function () {
    ConfiguracionSitio();
});

function ConfiguracionSitio() {

    $("#tbmCantidad").inputSpinner({ buttonsOnly: true, autoInterval: undefined });

    $('#txtDescripcion').keypress(function () {
        $("#txtDescripcionHtml").html("");
        $("#txtDescripcionHtml").hide();
    });


    $("#tbmCantidad").on("input", function (event) {
        //$valueOnInput.html();

        //alert($(event.target).val());

        if ($('#ddlTipo').find(":selected").val() == 20) {
            $(".nroDias").hide();
            $("#tbmCantidad").val("0");
            $("#txtFeedHtml").html("La notificación llegará el mismo día de vencimiento de la factura.");
        }
        else {
            if (this.value == 30) {
                $(".nroDias").show();
                $("#txtFeedHtml").html("La notificación llegará " + $(event.target).val() + " días después de la fecha de vencimiento de la factura.");
            }
            else {
                $(".nroDias").show();
                $("#txtFeedHtml").html("La notificacion llegará " + $(event.target).val() + " días antes de la fecha de vencimiento de la factura.");
            }
        }
    })


    if ($('#ddlTipo').find(":selected").val() == 20) {
        $(".nroDias").hide();
        $("#tbmCantidad").val("0");
        $("#txtFeedHtml").html("La notificación llegará el mismo día de vencimiento de la factura.");
    }
    else {
        if (this.value == 30) {
            $(".nroDias").show();
            $("#txtFeedHtml").html("La notificación llegará días después de la fecha de vencimiento de la factura.");
        }
        else {
            $(".nroDias").show();
            $("#txtFeedHtml").html("La notificacion llegará días antes de la fecha de vencimiento de la factura.");
        }
    }



    $('#ddlTipo').on('change', function () {

        if (this.value == 20) {
            $(".nroDias").hide();
            $("#tbmCantidad").val("0");
            $("#txtFeedHtml").html("La notificación llegará el mismo día de vencimiento de la factura.");
        }
        else {
            if (this.value == 30) {
                $(".nroDias").show();
                $("#txtFeedHtml").html("La notificación llegará días después de la fecha de vencimiento de la factura.");
            }
            else {
                $(".nroDias").show();
                $("#txtFeedHtml").html("La notificacion llegará días antes de la fecha de vencimiento de la factura.");
            }
        }
    });



    $("#btnGuardar").click(function () {

        if ($("#txtDescripcion").val().length > 0) {

            $("#txtDescripcionHtml").html("");
            $("#txtDescripcionHtml").hide();

            if (operacionActualizar) {
                EditarNotificacion();
            }
            else {
                RegistroNotificacion();
            }

        } else {
            $("#txtDescripcionHtml").html("Debe ingresar una descripción para la notificación.");
            $("#txtDescripcionHtml").show();
            $("#txtDescripcion").focus();
        }

    });


    $("#btnBuscar").click(function () {
        $('#dataTableContacto').DataTable().ajax.reload();
    });

    $("#btnLimpiar").click(function () {
        $("#txtNombre").val("");
        $('#dataTableContacto').DataTable().ajax.reload();
    });

    $("#btnNuevo").click(function () {
        LimpiarAgregar();
        $('#verProductoModal').modal({ backdrop: 'static', keyboard: false });
    });


    NotificacionGrillaLoad();
}


function LimpiarAgregar() {

    operacionActualizar = false;

    $("#txtDescripcion").val("");
    $("#chkHabilitado").prop('checked', false);
    $("#tbmCantidad").prop('disabled', false);
    $("#tbmCantidad").val("0");
}


function EditarNotificacion() {

    if ($("#__ID_NOTIFICACION").val() != 0) {
        $("#loadingSucessoModal").show();
        $("#infoSucesoModal").hide();
        var url = $('#__URL_EDITAR_NOTIFICACION').val();

        var dato = {
            "id": $("#__ID_NOTIFICACION").val(),
            "nombre": $("#txtDescripcion").val(),
            "dias": $("#tbmCantidad").val(),
            "tipoAlerta": $("#ddlTipo").val(),
            "esDeSistema": false
        };
        realizarPost(url, dato, 'json', RespuestaEditarNotificacion, EditarNotificacionError, 10000);
    }
    else {
        MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
    }

}


function RespuestaEditarNotificacion(dato) {

    operacionActualizar = false;
    $("#loadingSucessoModal").hide();
    MensajeAlerta("¡Se guardó correctamente! ", "success");
    $('#verProductoModal').modal('toggle');
    $('#dataTableContacto').DataTable().ajax.reload();
}

function EditarNotificacionError(jqXHR, textStatus, errorThrown) {
    operacionActualizar = false;
    $("#loadingSucessoModal").hide();
    console.log(jqXHR);
    MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
}



function RegistroNotificacion() {

    //if ($("#__ID_NOTIFICACION").val() != 0) {
    $("#loadingSucessoModal").show();
    $("#infoSucesoModal").hide();
    var url = $('#__URL_REGISTRO_NOTIFICACION').val();

    var dato = {
        "nombre": $("#txtDescripcion").val(),
        "dias": $("#tbmCantidad").val(),
        "tipoAlerta": $("#ddlTipo").val(),
        "esDeSistema": false
    };
    realizarPost(url, dato, 'json', RespuestaRegistroNotificacion, RegistroNotificacionError, 10000);
    //}
    //else {
    //    MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
    //}

}


function RespuestaRegistroNotificacion(dato) {

    $("#loadingSucessoModal").hide();
    MensajeAlerta("¡Se registró correctamente! ", "success");
    $('#verProductoModal').modal('toggle');
    $('#dataTableContacto').DataTable().ajax.reload();

}

function RegistroNotificacionError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucessoModal").hide();
    console.log(jqXHR);
    MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
}



function hasDecimalPlace(value, x) {
    var pointIndex = value.indexOf('.');
    return pointIndex >= 0 && pointIndex < value.length - x;
}



function NotificacionGrillaLoad() {
    var options = $.extend(true, {}, defaults, {
        "aoColumnDefs": [
            {
                'bSortable': false,
                'aTargets': [0]
            },
        ],
        "order": [[0, "desc"]],
        "iDisplayLength": 5,
        "aLengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "All"]
        ],
        "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"ip>',
        "tableTools": {
            "aButtons": [
            ]
        },
        "bFilter": false,
        "bSort": true,
        "processing": true,
        "serverSide": true,
        "lengthMenu": [
            [10, 20, 50, 100, 150],
            [10, 20, 50, 100, 150]
        ],
        "pageLength": 10,
        "ajax": {
            "url": $('#__URL_BUSQUEDA').val(), // ajax source
            "type": "POST",
            "error": validarErrorGrilla,
            "data": function (d) {
                return $.extend({}, d, {
                    "nombre": $("#txtNombre").val()
                });
            }
        },
        "columns": [
            {
                "render": function (data, type, row) {

                    if (row.esDeSistema) {
                        return '<i class="la la-eye-slash"></i> ';
                    }
                    else {
                        return '<button onclick="DetalleNotificacion(\'' + row.id + '\')" class="btn btn-sm btn-clean btn-icon mr-1" title="Editar"><i class="la la-edit"></i></button> '
                            + '<button onclick="PreguntarEliminarCategoria(\'' + row.id + '\')" class="btn btn-sm btn-clean btn-icon" title="Eliminar"><i class="la la-trash"></i></button>';
                    }

                }
            },
            { "data": "nombre" },
            //{ "data": "dias" },
            {
                "render": function (data, type, row) {
                    let texto = "Mismo día";

                    if (row.dias > 0) {
                        texto = row.dias + " Días después";
                    }

                    if (row.dias < 0) {
                        row.dias = row.dias * -1
                        texto = row.dias + " Días después";
                    }

                    return texto;
                }
            },
            {
                "render": function (data, type, row) {
                    let texto = "No"
                    if (row.esDeSistema) {
                        texto = "Si"
                    }

                    if (row.esDeSistema == null) {
                        texto = ""
                    }

                    return texto;
                }
            }

        ],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
    });

    $('#dataTableContacto').DataTable(options);
}

function PreguntarEliminarCategoria(id) {
    MensajeCondicionalUno("¿Desea Eliminar?", "Se borrará del registro de notificaciones", Eliminar, id);
}


function Eliminar(id) {

    var url = $('#__URL_ELIMINAR_NOTIFICACION').val() + "/" + id;
    var dato = {
    };
    realizarGet(url, dato, 'json', RespuestaEliminar, ErrorEliminar, 10000);
}

function RespuestaEliminar(data) {
    $("#loadingSucessoModal").hide();
    MensajeAlerta("¡Se eliminó correctamente! ", "success");
    $('#dataTableContacto').DataTable().ajax.reload();
    LimpiarAgregar();
}

function ErrorEliminar(data) {
    console.log(data);
}






function DetalleNotificacion(id) {

    operacionActualizar = true;

    var url = $('#__GET_DETALLE').val() + id;
    var dato = {
    };
    realizarGet(url, dato, 'json', RespuestaBuscarInfoPerfil, BuscarInfoPerfilError, 10000);


}

function RespuestaBuscarInfoPerfil(dato) {

    $("#__ID_NOTIFICACION").val(dato.id);


    $("#txtDescripcion").val(dato.nombre);
    $("#chkHabilitado").prop('checked', dato.estaHabilitado);


    if (dato.dias == 0) {
        $(".nroDias").hide();
        $("#ddlTipo").val(20).change();
        //$("#tbmCantidad").val("0");
        //$('#tbmCantidad').attr('readonly', true);
    }
    else {
        if (dato.dias < 0) {
            $(".nroDias").show();
            $("#ddlTipo").val(30).change();
            $("#tbmCantidad").val(dato.dias * -1);
        }
        else {
            $(".nroDias").show();
            $("#ddlTipo").val(10).change();
            $("#tbmCantidad").val(dato.dias);
        }
    }

    $('#verProductoModal').modal({ backdrop: 'static', keyboard: false });

}

function BuscarInfoPerfilError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucessoModal").hide();
    console.log(jqXHR);
    $("#msjError").hide();
}