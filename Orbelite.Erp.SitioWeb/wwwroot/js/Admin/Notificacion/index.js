﻿var operacionActualizar = false;

$(document).ready(function () {
    ConfiguracionSitio();
});

function ConfiguracionSitio() {

    $("#btnGuardar").click(function () {

        if (operacionActualizar) {
            EditarContacto();
        }
        else {
            RegistroContacto();
        }

    });

    $(".juridica").hide();

    $('#txtDocumento').keypress(function () {
        SoloNumeros1_9();
    });

    $("#btnBuscar").click(function () {
        $('#dataTableContacto').DataTable().ajax.reload();
    });

    $("#btnLimpiar").click(function () {
        $("#txtDoc").val("");
        $('#dataTableContacto').DataTable().ajax.reload();
    });

    $("#btnNuevo").click(function () {
        LimpiarAgregar();
        $('#verProductoModal').modal({ backdrop: 'static', keyboard: false });
    });


    if ($("#txtCorreo").val().length > 0) {
        $(".correox").show();
    }
    else {
        $(".correox").hide();
    }

    if ($("#txtTelefono").val().length > 0) {
        $(".telefonox").show();
    }
    else {
        $(".telefonox").hide();
    }

    if ($("#txtDocumento").val().length > 8) {
        $(".natural").hide();
        $(".juridica").show();
    }
    else {
        $(".juridica").hide();
        $(".natural").show();
    }


    FiltrarDocumento();

    ContactoGrillaLoad();
}


function LimpiarAgregar() {

    operacionActualizar = false;

    $(".natural").hide();
    $(".juridica").show();
    $("#txtDocumento").val("");
    $("#txtRazonSocial").val("");
    $("#txtNombrePersona").val("");
    $("#txtCorreo").val("");
    $("#txtTelefono").val("");
    $("#txtDepartamento").val("");
    $("#txtProvincia").val("");
    $("#txtDistrito").val("");
    $("#txtDireccion").val("");
    $("#chkHabilitado").prop('checked', false);
}


function EditarContacto() {

    if ($("#__ID_CONTACTO").val() != 0) {
        $("#loadingSucessoModal").show();
        $("#infoSucesoModal").hide();
        var url = $('#__URL_EDITAR_CONTACTO').val();

        var dato = {
            "idContacto": $("#__ID_CONTACTO").val(),
            "vencimiento": $('#chkHabilitado').is(':checked')
        };
        realizarPost(url, dato, 'json', RespuestaEditarContacto, EditarContactoError, 10000);
    }
    else {
        MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
    }

}


function RespuestaEditarContacto(dato) {

    operacionActualizar = false;
    $("#loadingSucessoModal").hide();
    MensajeAlerta("¡Se guardó correctamente! ", "success");
    $('#verProductoModal').modal('toggle');
    $('#dataTableContacto').DataTable().ajax.reload();
}

function EditarContactoError(jqXHR, textStatus, errorThrown) {
    operacionActualizar = false;
    $("#loadingSucessoModal").hide();
    console.log(jqXHR);
    MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
}



function RegistroContacto() {

    if ($("#__ID_CONTACTO").val() != 0) {
        $("#loadingSucessoModal").show();
        $("#infoSucesoModal").hide();
        var url = $('#__URL_REGISTRO_CONTACTO').val();

        var dato = {
            "idContacto": $("#__ID_CONTACTO").val(),
            "vencimiento": $('#chkHabilitado').is(':checked')
        };
        realizarPost(url, dato, 'json', RespuestaRegistroContacto, RegistroContactoError, 10000);
    }
    else {
        MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
    }

}


function RespuestaRegistroContacto(dato) {

    $("#loadingSucessoModal").hide();
    MensajeAlerta("¡Se registró correctamente! ", "success");
    $('#verProductoModal').modal('toggle');
    $('#dataTableContacto').DataTable().ajax.reload();

}

function RegistroContactoError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucessoModal").hide();
    console.log(jqXHR);
    MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
}



function hasDecimalPlace(value, x) {
    var pointIndex = value.indexOf('.');
    return pointIndex >= 0 && pointIndex < value.length - x;
}



function ContactoGrillaLoad() {
    var options = $.extend(true, {}, defaults, {
        "aoColumnDefs": [
            {
                'bSortable': false,
                'aTargets': [0]
            },
        ],
        "order": [[0, "desc"]],
        "iDisplayLength": 5,
        "aLengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "All"]
        ],
        "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"ip>',
        "tableTools": {
            "aButtons": [
            ]
        },
        "bFilter": false,
        "bSort": true,
        "processing": true,
        "serverSide": true,
        "lengthMenu": [
            [10, 20, 50, 100, 150],
            [10, 20, 50, 100, 150]
        ],
        "pageLength": 10,
        "ajax": {
            "url": $('#__URL_BUSQUEDA').val(), // ajax source
            "type": "POST",
            "error": validarErrorGrilla,
            "data": function (d) {
                return $.extend({}, d, {
                    "documento": $("#txtDoc").val()
                });
            }
        },
        "columns": [
            {
                "render": function (data, type, row) {
                    return '<button onclick="DetalleContacto(\'' + row.persona.documento + '\' , \'' + row.vencimiento + '\')" class="btn btn-sm btn-clean btn-icon mr-1" title="Editar"><i class="la la-edit"></i></button> ';
                    //+ '<button onclick="PreguntarEliminarCategoria(\'' + row.id + '\')" class="btn btn-sm btn-clean btn-icon" title="Eliminar"><i class="la la-trash"></i></button>'
                }
            },
            //{ "data": "idContacto" },
            {
                "render": function (data, type, row) {
                    let texto = "";
                    if (row.persona != null) {
                        texto = row.persona.documento;
                    }
                    return texto;
                }
            },
            {
                "render": function (data, type, row) {
                    let texto = "";
                    if (row.persona != null) {
                        texto = row.persona.nombres + ' ' + row.persona.paterno + ' ' + row.persona.materno;
                    }
                    return texto;
                }
            },
            {
                "render": function (data, type, row) {
                    let texto = "No"
                    if (row.vencimiento) {
                        texto = "Si"
                    }

                    if (row.vencimiento == null) {
                        texto = ""
                    }

                    return texto;
                }
            }

        ],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
    });

    $('#dataTableContacto').DataTable(options);
}


function DetalleContacto(documento, vencimiento) {

    operacionActualizar = true;

    if (vencimiento === 'true') {
        $("#chkHabilitado").prop('checked', true);
    }
    else {
        $("#chkHabilitado").prop('checked', false);
    }

    var url = $('#__GET_DETALLE').val() + documento;
    var dato = {
    };
    realizarGet(url, dato, 'json', RespuestaBuscarInfoPerfil, BuscarInfoPerfilError, 10000);


}


function FiltrarDocumento() {
    var urlData = $("#__URL_OCURRENCIA").val();
    var docs = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('ocurrencia'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: urlData + '/%QUERY',
            wildcard: '%QUERY'
        }
    });

    $('#txtDocumento').typeahead(null, {
        name: 'docs',
        display: 'documento',
        source: docs,
        templates: {
            empty: [
                '<div class="empty-message text-danger" style="padding: 10px 15px; text-align: center;"> No existe sugerencia para su busqueda </div>'
            ].join(''),
            suggestion: Handlebars.compile('<div> <strong> {{razonSocial}} {{nombres}} {{paterno}} {{materno}} </strong> <br> {{documento}} </div>')
        }
    });

    $('#txtDocumento').bind('typeahead:select', function (ev, suggestion) {

        var url = $('#__GET_DETALLE').val() + $('#txtDocumento').val();
        var dato = {
        };
        realizarGet(url, dato, 'json', RespuestaBuscarInfoPerfil, BuscarInfoPerfilError, 10000);

    });
}


function RespuestaBuscarInfoPerfil(dato) {

    $("#__ID_CONTACTO").val(dato.id);


    $("#loadingSucessoModal").hide();
    $("#txtDocumento").val(dato.documento);
    $("#txtRazonSocial").val(dato.razonSocial);
    $("#txtDepartamento").val(dato.departamento);
    $("#txtProvincia").val(dato.provincia);
    $("#txtDistrito").val(dato.distrito);
    $("#txtDireccion").val(dato.direccion);
    $("#txtCorreo").val(dato.correo);
    $("#txtTelefono").val(dato.telefono);

    if (dato.documento.length == 8) {
        $(".juridica").hide();
        $(".natural").show();
        $("#txtNombrePersona").show();
        $("#txtNombrePersona").val(dato.nombres + ' ' + dato.paterno + ' ' + dato.materno);
    }
    else {
        $(".natural").hide();
        $(".juridica").show();
    }

    if (dato.correo.length > 0) {
        $(".correox").show();
    }
    else {
        $(".correox").hide();
    }

    if (dato.telefono.length > 0) {
        $(".telefonox").show();
    }
    else {
        $(".telefonox").hide();
    }


    $('#verProductoModal').modal({ backdrop: 'static', keyboard: false });

}

function BuscarInfoPerfilError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucessoModal").hide();
    console.log(jqXHR);
    $("#msjError").hide();
}