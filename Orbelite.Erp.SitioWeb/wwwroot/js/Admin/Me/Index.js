﻿var map;
var markers = [];
$(document).ready(function () {
    controles();

});


function controles() {

    $('#btnGuardarDataUsuario').click(function () {
        ActualizarDatoUsuario();
    });

    $('#btnGuardarRedesSociales').click(function () {
        GuardarAcatualizacionRedesUsuario();
    });

    $('#txtNumeroCuentaUser').keypress(function () {
        SoloNumerosYGuion();
    });

    $('#txtNumeroCuentaUser').keydown(function () {
        $("#txtNumeroCuentaUserHtml").hide();
    });

    $('#txtNumeroCuentaUser').keyup(delayTime(function (e) {
        ValidarNumeroDeCuentaRequerido();
    }, 1000));

    $('#cbxFrecuencia').change(function () {
        $("#cbxFrecuenciaHtml").hide();
    });

    $('#cbxBanco').change(function () {
        $("#cbxBancoHtml").hide();
    });

    BuscarInfoPerfil();
    BuscarHistorialCrediticio();
}


function BuscarInfoPerfil() {
    var url = $('#__URL_BUSCAR_INFORMACION_PERSONA').val();
    var dato = {
    };
    realizarPost(url, dato, 'json', RespuestaBuscarInfoPerfil, BuscarInfoPerfilError, 10000);
}

function RespuestaBuscarInfoPerfil(dato) {
    $("#txtDni").val(dato.dni);
    $("#txtNombres").val(dato.nombres);
    $("#txtApellidoPaterno").val(dato.apellidoPaterno);
    $("#txtApellidoMaterno").val(dato.apellidoMaterno);
    $("#txtFechaNacimiento").val(dato.fechaNacimiento);
    $("#txtDireccion").val(dato.direccion);
    $("#txtDistrito").val(dato.distrito);
    $("#txtProvincia").val(dato.provincia);
    $("#txtDepartamento").val(dato.departamento);
    initMap();
}

function BuscarInfoPerfilError(jqXHR, textStatus, errorThrown) {
}


function initMap() {
    map = new google.maps.Map(document.getElementById('map_container_google_direccion'), {
        zoom: 12,
        center: { lat: -12.0431800, lng: -77.0282400 }
    });
    var geocoder = new google.maps.Geocoder();
    geocodeAddress(geocoder, map)
}

function geocodeAddress(geocoder, resultsMap) {
    var address = $('#txtDistrito').val() + " " + $('#txtProvincia').val() + " " + $('#txtDepartamento').val() + " " + $('#txtDireccion').val();
    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: resultsMap,
                position: results[0].geometry.location
            });
        }
    });
}

function BuscarHistorialCrediticio() {
    var url = $('#__URL_BUSCAR_HISTORIAL_CREDITICIO').val();
    var dato = {
    };
    realizarPost(url, dato, 'json', RespuestaBuscarHistorialCrediticio, BuscarHistorialCrediticioError, 10000);
}

function RespuestaBuscarHistorialCrediticio(data) {
    $('#lbMes1').html("");
    $('#lbMes2').html("");
    $('#lbMes3').html("");

    $('#btnColor1').html("");
    $('#btnColor2').html("");
    $('#btnColor3').html("");

    var UrlDetallePrimero = $("#__URL_BUSCAR_CREDITICIO_DETALLE_PRIMERO").val();
    var UrlDetalleSegundo = $("#__URL_BUSCAR_CREDITICIO_DETALLE_SEGUNDO").val();
    var UrlDetalleTercero = $("#__URL_BUSCAR_CREDITICIO_DETALLE_TERCERO").val();

    if (data == null) {
        return;
    }

    var btn_red = '<button id="btnRed" class="retro-button red-button"></button>';
    var btn_yellow = '<button id="btnYellow" class="retro-button yellow-button"></button>';
    var btn_gray = '<button id="btnGray" class="retro-button"></button>';
    var btn_green = '<button id="btnGreen" class="retro-button green-button"></button>';


    for (var i = 0; i < data.length; i++) {
        //$("#Credicticio").show();
        var dato = data[i];
        var botonActual = i + 1;

        $('#lbMes' + botonActual).html('<label for="">' + dato.descripcion + '</label>');

        var pNormal = obtenerValorONumero(dato.normal);
        var pDeficiente = obtenerValorONumero(dato.deficiente);
        var pDudoso = obtenerValorONumero(dato.dudoso);
        var pPerdida = obtenerValorONumero(dato.perdida);
        var pProblemasPotenciales = obtenerValorONumero(dato.problemasPotenciales);



        if (pNormal === 0 && pDeficiente === 0 && pDudoso === 0 && pPerdida === 0 && pProblemasPotenciales === 0) {
            $('#btnColor' + botonActual).append(btn_gray);
            $('#btnGray').attr('id', 'btn' + botonActual);
            continue;
        }

        if (pNormal >= 70) {
            $('#btnColor' + botonActual).append(btn_green);
            $('#btnGreen').attr('id', 'btn' + botonActual);
            continue;
        }

        if (pNormal >= 40 && pNormal < 70) {
            $('#btnColor' + botonActual).append(btn_yellow);
            $('#btnYellow').attr('id', 'btn' + botonActual);
            continue;
        }

        if (pNormal < 40) {
            $('#btnColor' + botonActual).append(btn_red);
            $('#btnRed').attr('id', 'btn' + botonActual);
            continue;
        }
        $('#btnColor' + botonActual).append(btn_gray);
        $('#btnGray').attr('id', 'btn' + botonActual);
    }


    var EnClick = false;
    $('#Credicticio-tab').click(function () {
        if (EnClick) { return true; }
        EnClick = true;
        if (data.length > 0) {
            PintarHistorialCrediticio(data[0]);
            $('#btn3').removeClass('actived');
            $('#btn2').removeClass('actived');
            $('#btn1').addClass('actived');
            realizarPost(UrlDetallePrimero, { Id: data[0].id }, 'json', RespuestaCrediticioDetalle, null);
        }
    });



    $('#btn1').click(function () {
        if (data.length > 0) {
            PintarHistorialCrediticio(data[0]);
            $('#btn3').removeClass('actived');
            $('#btn2').removeClass('actived');
            $('#btn1').addClass('actived');
            realizarPost(UrlDetallePrimero, { Id: data[0].id }, 'json', RespuestaCrediticioDetalle, null);
        }
    });

    $('#btn2').click(function () {
        if (data.length > 1) {
            PintarHistorialCrediticio(data[1]);
            $('#btn1').removeClass('actived');
            $('#btn3').removeClass('actived');
            $('#btn2').addClass('actived');
            realizarPost(UrlDetalleSegundo, { Id: data[1].id }, 'json', RespuestaCrediticioDetalle);
        }
    });

    $('#btn3').click(function () {
        if (data.length > 2) {
            PintarHistorialCrediticio(data[2]);
            $('#btn1').removeClass('actived');
            $('#btn2').removeClass('actived');
            $('#btn3').addClass('actived');
            realizarPost(UrlDetalleTercero, { Id: data[2].id }, 'json', RespuestaCrediticioDetalle);
        }
    });
}

function PintarHistorialCrediticio(data) {

    $("#DataHistorialCrediticio tbody").html("");

    $('#DataHistorialCrediticio').append('<tr><td> % </td>'
        + '<td class="text-center"><span class="label label-inline label-light-dark font-weight-bold normal"> ' + data.normal + '</span></td>'
        + '<td class="text-center"><span class="label label-inline label-light-dark font-weight-bold problemas-potenciales"> ' + data.problemasPotenciales + '</span></td>>'
        + '<td class="text-center"><span class="label label-inline label-light-dark font-weight-bold dudoso"> ' + data.dudoso + '</span></td>'
        + '<td class="text-center"><span class="label label-inline label-light-dark font-weight-bold deficiente"> ' + data.deficiente + '</span></td>'
        + ' <td class="text-center"><span class="label label-inline label-light-dark font-weight-bold perdidas"> ' + data.perdida + '</span></td> </tr> ');
}

function RespuestaCrediticioDetalle(data) {
    var html = "";
    for (i = 0; i < data.length; i++) {
        html += '<tr>';
        html += "<td> " + data[i].nombre + " </td>";
        html += "<td> " + data[i].tipoMoneda + " </td>";
        html += '<td class="text-center"> ' + data[i].saldo.toFixed(2) + "  </td>";
        html += '<td class="text-center"> ' + data[i].condicion + " </td>";
        html += '<td> ' + data[i].clasificacionNombre + " </td>";
        html += '<td> ' + data[i].tipoCreditoNombre + " </td>";
        html += "</tr>";
    }
    $("#tableDatalleHistorial tbody").html(html);
}


function BuscarHistorialCrediticioError(jqXHR, textStatus, errorThrown) {
}

function obtenerValorONumero(valor) {
    if (!valor) {
        return 0;
    }

    return valor;
}

function ActualizarDatoUsuario() {
    if (ValidarCamposRequeridoDatoUsuario()) {
        MensajeCondicional("¿Dessea actualizar?", "Se actualizará tu información modificada", GuardarDatoUsuario);
    }
}

function ValidarCamposRequeridoDatoUsuario() {
    var flat = true;
    if (parseInt($("#cbxFrecuencia").val()) === 0) {
        $("#cbxFrecuencia").focus();
        $("#cbxFrecuenciaHtml").html("Frecuencia pago requerido, seleccione por favor.");
        $("#cbxFrecuenciaHtml").show();
        flat = false;
    } else if ($("#cbxBanco").val().length === 0) {
        $("#cbxBanco").focus();
        $("#cbxBancoHtml").html("Banco requerido, seleccione por favor.");
        $("#cbxBancoHtml").show();
        flat = false;
    } else if ($("#txtNumeroCuentaUser").val().length === 0) {
        $("#txtNumeroCuentaUser").focus();
        $("#txtNumeroCuentaUserHtml").html("N° cuenta requerido, ingrese por favor.");
        $("#txtNumeroCuentaUserHtml").show();
        flat = false;
    }
    return flat;
}

function ValidarNumeroDeCuentaRequerido() {
    var flat = true;
    if ($("#txtNumeroCuentaUser").val().length === 0) {
        $("#txtNumeroCuentaUser").focus();
        $("#txtNumeroCuentaUserHtml").html("N° cuenta requerido, ingrese por favor.");
        $("#txtNumeroCuentaUserHtml").show();
        $("#txtNumeroCuentaUser").addClass("is-invalid");
        $("#txtNumeroCuentaUser").removeClass("is-valid");
        flat = false;
    } else if ($("#txtNumeroCuentaUser").val().length < 10) {
        $("#txtNumeroCuentaUser").focus();
        $("#txtNumeroCuentaUserHtml").html("Verifique su número de cuenta por favor.");
        $("#txtNumeroCuentaUserHtml").show();
        $("#txtNumeroCuentaUser").addClass("is-invalid");
        $("#txtNumeroCuentaUser").removeClass("is-valid");
        flat = false;
    } else if ($("#txtNumeroCuentaUser").val().length > 9) {
        $("#txtNumeroCuentaUserHtml").hide();
        $("#txtNumeroCuentaUser").removeClass("is-invalid");
        $("#txtNumeroCuentaUser").addClass("is-valid");
        flat = false;
    }
    return flat;
}

function GuardarDatoUsuario() {

    var url = $('#__URL_ACTUALIZAR_DATO_USUARIO').val();
    var banco = {
        "Id": $("#cbxBanco").val(),
    };

    var dato = {
        "Id": $("#__Id_Usuario").val(),
        "IdFrecuenciaPago": $("#cbxFrecuencia").val(),
        "NumeroCuentaBancaria": $("#txtNumeroCuentaUser").val(),
        "Banco": banco
    };
    realizarPost(url, dato, 'json', RespuestaGuardarDatoUsuario, RespuestaGuardarDatoUsuarioError, 10000);
}

function RespuestaGuardarDatoUsuario(data) {
    MensajeAlerta(data.mensaje, "success");
}

function RespuestaGuardarDatoUsuarioError(jqXHR) {
    MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
}

function GuardarAcatualizacionRedesUsuario() {

    var url = $('#__URL_ACTUALIZAR_REDES_SOCIALES_USUARIO').val();
    var banco = {
        "Id": $("#cbxBanco").val(),
    };
    var banco = {
        "Id": $("#cbxBanco").val(),
    };

    var dato = {
        "Id": $("#__Id_Usuario").val(),
        "IdFrecuenciaPago": $("#cbxFrecuencia").val(),
        "NumeroCuentaBancaria": $("#txtNumeroCuentaUser").val(),
        "Banco": banco,
        "FacebookUrl": $("#txtFacebook").val(),
        "InstagramUrl": $("#txtInstagram").val(),
        "YoutubeUrl": $("#txtYoutube").val(),
        "TwitterUrl": $("#txtTwiter").val(),
        "TiktokUrl": $("#txtTikTok").val(),
        "WebSiteUrl": $("#txtWebSitio").val(),
    };
    realizarPost(url, dato, 'json', RespuestaGuardarAcatualizacionRedesUsuario, GuardarAcatualizacionRedesUsuarioError, 10000);
}

function RespuestaGuardarAcatualizacionRedesUsuario(data) {
    MensajeAlerta(data.mensaje, "success");
}

function GuardarAcatualizacionRedesUsuarioError(jqXHR) {
    MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
}
/***************************MENSAJES ALERTA*********************************/
function MensajeAlerta(mensaje, tipo) {
    Swal.fire({
        text: mensaje,
        icon: tipo,
        buttonsStyling: true,
        confirmButtonText: "Cerrar",
        customClass: {
            confirmButton: "btn font-weight-bold btn-light"
        }
    });
}

function MensajeCondicional(titulo, detalle, fun) {
    Swal.fire({
        title: titulo,
        text: detalle,
        icon: "warning",
        showCancelButton: true,
        cancelButtonText: "Cancelar",
        confirmButtonText: "Sí"
    }).then(function (result) {
        if (result.value) {
            fun();
        }
    });
}
/***************************VALIDACION*********************************/
function SoloNumerosYGuion() {
    var key = window.event ? event.which : event.keyCode;
    if ((key < 48 || key > 57)) {
        if (key != 45) {
            event.preventDefault();
        }
    }
}