﻿$(document).ready(function () {
    ConfiguracionSitio();
});

function ConfiguracionSitio() {

    $('#txtDocumento').keypress(function () {
        SoloNumeros1_9();
    });

    $('#tbmDocumento').keypress(function () {
        SoloNumeros1_9();
    });

    $('#tbmTelefono').keypress(function () {
        SoloNumeros1_9();
    });


    if ($('#ddlTipo').find(":selected").val() == 100) {
        $(".juridica").hide();
        $(".natural").show();
    }

    $('#ddlTipo').on('change', function () {

        if (this.value == 100) {
            HideMensajes();
            LimpiarAgregar();
            $(".juridica").hide();
            $(".natural").show();
        }

        if (this.value == 101) {
            HideMensajes();
            LimpiarAgregar();
            $(".natural").hide();
            $(".juridica").show();
        }

    });

    VehiculoGrillaLoad();

    $("#btnBuscar").click(function () {
        $('#dataTablePersona').DataTable().ajax.reload();
    });

    $("#btnNuevo").click(function () {
        LimpiarAgregar();
        //$("#verPersonaModal").modal("show");
        $('#verPersonaModal').modal({ backdrop: 'static', keyboard: false })
    });

    $("#btnGuardar").click(function () {
        //GuardarPersona();
        RegistroPersona();
    });

    $("#btnLimpiar").click(function () {
        $("#txtDocumento").val("");
        $("#txtNombres").val("");
        $("#txtApellidoPaterno").val("");
        $("#txtApellidoMaterno").val("");
        $("#txtRazonSocial").val("");
        $('#dataTablePersona').DataTable().ajax.reload();
    });


    $('#btnBuscarDni').click(function () {

        if ($('#ddlTipo').find(":selected").val() == 100) {
            BuscarInfo();
        }
        else {
            BuscarInfoRuc();
        }

    });


}


function BuscarInfo() {
    //Limpiar();
    if ($("#tbmDocumento").val().length > 0) {

        $("#tbmDocumentoHtml").hide();
        $("#loadingSucessoModal").show();
        var url = $('#__URL_BUSQUEDA_DNI').val() + $("#tbmDocumento").val();
        var dato = {
        };
        realizarGet(url, dato, 'json', RespuestaBuscarInfoPerfil, BuscarInfoPerfilError, 10000);
    } else {
        $("#tbmDocumento").focus();
        $("#tbmDocumentoHtml").show();
        $("#tbmDocumentoHtml").html("El número de DNI es requerido");
    }

}

function RespuestaBuscarInfoPerfil(dato) {
    $("#loadingSucessoModal").hide();
    $("#tbmDocumentoHtml").hide();
    $("#tbmDocumento").val(dato.dni);
    $("#tbmNombres").val(dato.nombres);
    $("#tbmPaterno").val(dato.apellidoPaterno);
    $("#tbmMaterno").val(dato.apellidoMaterno);
}

function BuscarInfoPerfilError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucessoModal").hide();
    console.log(jqXHR);
    $("#tbmDocumentoHtml").hide();
    $("#msjError").hide();
}


function BuscarInfoRuc() {
    //Limpiar();
    if ($("#tbmDocumento").val().length > 0) {

        $("#tbmDocumentoHtml").hide();
        $("#loadingSucessoModal").show();
        var url = $('#__URL_BUSQUEDA_RUC').val() + $("#tbmDocumento").val();
        var dato = {
        };
        realizarGet(url, dato, 'json', RespuestaBuscarInfoRuc, BuscarInfoRucError, 10000);
    } else {
        $("#tbmDocumento").focus();
        $("#tbmDocumentoHtml").show();
        $("#tbmDocumentoHtml").html("El número de RUC es requerido");
    }

}

function RespuestaBuscarInfoRuc(dato) {
    $("#loadingSucessoModal").hide();
    $("#tbmDocumentoHtml").hide();
    $("#tbmDocumento").val(dato.resultado.ruc);
    $("#tbmRazon").val(dato.resultado.razonSocial);
}

function BuscarInfoRucError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucessoModal").hide();
    console.log(jqXHR);
    $("#tbmDocumentoHtml").hide();
    $("#msjError").hide();
}




function VehiculoGrillaLoad() {
    var options = $.extend(true, {}, defaults, {
        "aoColumnDefs": [
            {
                'bSortable': false,
                'aTargets': [0]
            },
        ],
        "order": [[0, "desc"]],
        "iDisplayLength": 5,
        "aLengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "All"]
        ],
        "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"ip>',
        "tableTools": {
            "aButtons": [
            ]
        },
        "bFilter": false,
        "bSort": true,
        "processing": true,
        "serverSide": true,
        "lengthMenu": [
            [10, 20, 50, 100, 150],
            [10, 20, 50, 100, 150]
        ],
        "pageLength": 10,
        "ajax": {
            "url": $('#__URL_BUSQUEDA').val(), // ajax source
            "type": "POST",
            "error": validarErrorGrilla,
            "data": function (d) {
                return $.extend({}, d, {
                    "documento": $("#txtDocumento").val(),
                    "nombres": $("#txtNombres").val(),
                    "paterno": $("#txtApellidoPaterno").val(),
                    "materno": $("#txtApellidoMaterno").val(),
                    "razon": $("#txtRazonSocial").val()
                });
            }
        },
        "columns": [

            {
                "render": function (data, type, row) {
                    //return '<a href="javascript:void(0)" onclick="DetallePersona(\'' + row.id + '\')">Editar</a>' + ' ' + '<a href="javascript:void(0)" onclick="PreguntarEliminarCategoria(\'' + row.id + '\')">Eliminar</a>';
                    return '<button onclick="DetallePersona(\'' + row.id + '\')" class="btn btn-sm btn-clean btn-icon mr-1" title="Editar"><i class="la la-edit"></i></button> ' + '<button onclick="PreguntarEliminarCategoria(\'' + row.id + '\')" class="btn btn-sm btn-clean btn-icon" title="Eliminar"><i class="la la-trash"></i></button>';

                }
            },
            { "data": "documento" },
            {
                "render": function (data, type, row) {
                    let texto = "";
                    if (row.tipo != null) {
                        texto = row.tipo.nombre;
                    }
                    return texto;
                }
            },
            {
                "render": function (data, type, row) {
                    let texto = "";
                    if (row.idTipo == 100) {
                        texto = row.nombres + " " + row.paterno + " " + row.materno;
                    }
                    else {
                        texto = row.razonSocial;
                    }
                    return texto;
                }
            },
            //{ "data": "nombres" }

        ],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
    });

    $('#dataTablePersona').DataTable(options);
}

function RegistroPersona() {

    if (validarCamposPersona() == 0) {
        $("#loadingSucessoModal").show();
        $("#infoSucesoModal").hide();
        var url = $('#__URL_REGISTRO_PERSONA').val();

        var dato = {
            "idTipo": $('#ddlTipo').find(":selected").val(),
            "documento": $("#tbmDocumento").val(),
            "nombres": $("#tbmNombres").val(),
            "razonSocial": $("#tbmRazon").val(),
            "paterno": $("#tbmPaterno").val(),
            "materno": $("#tbmMaterno").val(),
            "correo": $("#tbmCorreo").val(),
            "telefono": $("#tbmTelefono").val()
        };
        realizarPost(url, dato, 'json', RespuestaRegistroPersona, RegistroPersonaError, 10000);
    }

}

function RespuestaRegistroPersona(dato) {
    $("#loadingSucessoModal").hide();
    //$("#infoSucesoModal").show();

    location.href = $('#__URL_BUSQUEDA_PERSONA').val() + "/" + dato.id;

}

function RegistroPersonaError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucessoModal").hide();
    //alert("error");
    console.log(jqXHR);
    MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
}




function GuardarPersona() {

    //if (($('#ddlTipo').find(":selected").val() == 100 && $("#tbmDocumento").val().length == 8) || ($('#ddlTipo').find(":selected").val() == 101 && $("#tbmDocumento").val().length == 11)) {

    if (validarCamposPersona() == 0) {
        $("#loadingSucessoModal").show();
        $("#infoSucesoModal").hide();
        var url = $('#__GET_PERSONA').val();

        var dato = {
            "idTipo": $('#ddlTipo').find(":selected").val(),
            "documento": $("#tbmDocumento").val(),
            "nombres": $("#tbmNombres").val(),
            "razonSocial": $("#tbmRazon").val(),
            "paterno": $("#tbmPaterno").val(),
            "materno": $("#tbmMaterno").val(),
            "correo": $("#tbmCorreo").val(),
            "telefono": $("#tbmTelefono").val()
        };
        realizarPost(url, dato, 'json', RespuestaGuardarPersona, GuardarPersonaError, 10000);
    }



    //}
    //else {
    //    $("#tbmDocumentoHtml").html("Ingrese un nro° de documento válido");
    //    $("#tbmDocumentoHtml").show();
    //    $("#tbmDocumento").focus();
    //}

}

function validarCamposPersona() {

    var salida = 0;

    if (($('#ddlTipo').find(":selected").val() == 100 && $("#tbmDocumento").val().length == 8) || ($('#ddlTipo').find(":selected").val() == 101 && $("#tbmDocumento").val().length == 11)) {
        $("#tbmDocumentoHtml").hide();
        //salida = true;
    }
    else {
        salida++;
        $("#tbmDocumentoHtml").html("Ingrese un nro° de documento válido");
        $("#tbmDocumentoHtml").show();
        $("#tbmDocumento").focus();
    }



    if ($('#ddlTipo').find(":selected").val() == 100) {

        if ($("#tbmNombres").val().length !== 0) {
            $("#tbmNombresHtml").hide();
            //salida = true;
        }
        else {
            salida++;
            $("#tbmNombresHtml").html("Ingrese un valor válido");
            $("#tbmNombresHtml").show();
            $("#tbmNombres").focus();
        }

        if ($("#tbmPaterno").val().length !== 0) {
            $("#tbmPaternoHtml").hide();
            //salida = true;
        }
        else {
            salida++;
            $("#tbmPaternoHtml").html("Ingrese un valor válido");
            $("#tbmPaternoHtml").show();
            $("#tbmPaterno").focus();
        }

        if ($("#tbmMaterno").val().length !== 0) {
            $("#tbmMaternoHtml").hide();
            //salida = true;
        }
        else {
            salida++;
            $("#tbmMaternoHtml").html("Ingrese un valor válido");
            $("#tbmMaternoHtml").show();
            $("#tbmMaterno").focus();
        }

    }



    if ($('#ddlTipo').find(":selected").val() == 101) {
        if ($("#tbmRazon").val()) {
            $("#tbmRazonHtml").hide();
            //salida = true;
        }
        else {
            salida++;
            $("#tbmRazonHtml").html("Ingrese un valor válido");
            $("#tbmRazonHtml").show();
            $("#tbmRazon").focus();
        }
    }

    if ($("#tbmCorreo").val().length !== 0) {

        //$('#tbmCorreo').keypress(function () {
        if (!ValidarEmail($('#tbmCorreo').val())) {
            salida++;
            $("#tbmCorreoHtml").html("Ingrese un correo válido");
            $("#tbmCorreoHtml").show();
        }
        else {
            $("#tbmCorreoHtml").html("");
            $("#tbmCorreoHtml").hide();
        }
        //});

        //salida = true;
    }
    else {
        salida++;
        $("#tbmCorreoHtml").html("Ingrese un correo válido");
        $("#tbmCorreoHtml").show();
        $("#tbmCorreo").focus();
    }


    if ($("#tbmTelefono").val().length !== 0) {
        $("#tbmTelefonoHtml").hide();
        //salida = true;
    }
    else {
        salida++;
        $("#tbmTelefonoHtml").html("Ingrese un valor válido");
        $("#tbmTelefonoHtml").show();
        $("#tbmTelefono").focus();
    }

    return salida;
}

function HideMensajes() {
    $("#tbmDocumentoHtml").hide();
    $("#tbmNombresHtml").hide();
    $("#tbmPaternoHtml").hide();
    $("#tbmMaternoHtml").hide();
    $("#tbmRazonHtml").hide();
    $("#tbmCorreoHtml").hide();
    $("#tbmTelefonoHtml").hide();
}

function LimpiarAgregar() {
    $("#tbmDocumento").val("");
    $("#tbmNombres").val("");
    $("#tbmPaterno").val("");
    $("#tbmMaterno").val("");
    $("#tbmRazon").val("");
    $("#tbmCorreo").val("");
    $("#tbmTelefono").val("");
}


function RespuestaGuardarPersona(dato) {
    $("#loadingSucessoModal").hide();

    location.href = $('#__URL_BUSQUEDA_PERSONA').val();

}

function GuardarPersonaError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucessoModal").hide();
    alert("error");
    console.log(jqXHR);
    MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
}


function DetallePersona(id) {

    //$("#loadingSucessoModal").show();
    //$("#infoSucesoModal").hide();
    //var url = $('#__GET_DETALLE').val() + "/" + id;
    //var dato = {
    //};
    //realizarGet(url, dato, 'json', RespuestaDetallePersona, DetallePersonaError, 10000);

    location.href = $('#__URL_BUSQUEDA_PERSONA').val() + "/" + id;

}

//function RespuestaDetallePersona(dato) {
//    $("#loadingSucessoModal").hide();

//    location.href = $('#__URL_BUSQUEDA_PERSONA').val();

//}

//function DetallePersonaError(jqXHR, textStatus, errorThrown) {
//    $("#loadingSucessoModal").hide();
//    alert("error");
//    console.log(jqXHR);
//    MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
//}






function NuevoCargarArchivo() {
    $("#crearNuevoArchvioModal").modal("show");
    LimpiarDatosModal();
}

function InsertarArchivo() {
    $("#loadingSucessoModal").show();
    $("#infoErrorModal").hide();
    $("#infoSucesoModal").hide();
    var url = $('#__URL_SUBIR_DOCUMENTO').val();
    var dato = new FormData($("#FormArchivo")[0]);
    $.ajax({
        type: "POST",
        url: url,
        data: dato,
        processData: false,
        contentType: false,
        success: function (data) {
            RespuestaInsertarArchivo(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            ErrorInsertarArchivo(jqXHR.responseJSON);
        },
    });
}
function RespuestaInsertarArchivo(dato) {
    $("#loadingSucessoModal").hide();
    $("#infoSucesoModal").show();
    console.log(dato);
    $("#__HD_URL_DOCUMENTO").val(dato.id);
    $("#infoSucesoModal").html(dato.mensaje);
    document.getElementById("btnSubirArchivo").value = null;
}

function ErrorInsertarArchivo(data) {
    $("#loadingSucessoModal").hide();
    $("#infoErrorModal").show();
    document.getElementById("btnSubirArchivo").value = null;
    var html = "";
    if (data.mensajes.length > 0) {
        for (var i = 0; i < data.mensajes.length; i++) {
            html += data.mensajes[i];
        }
    } else {
        html = "Ocurrio un problema al validar tu archivo";
    }

    $("#infoErrorModal").show();
    $("#infoErrorModal").html(html);
}

function GuardarDocumento() {
    if ($("#tbmGrupo").val().length > 0) {
        if ($("#__HD_URL_DOCUMENTO").val().length > 0) {
            $("#loadingSucessoModal").show();
            $("#infoSucesoModal").hide();
            var url = $('#__URL_GUARDAR_DOCUMENTO').val();

            var datogrupo = {
                "id": $("#__tbIdGrupo").val(),
                "nombre": $("#tbmGrupo").val()
            };
            var dato = {
                "rutaArchivo": $("#__HD_URL_DOCUMENTO").val(),
                "grupo": datogrupo,
            };
            realizarPost(url, dato, 'json', RespuestaGuardarDocumento, GuardarDocumentoError, 10000);
        } else {
            MensajeAlerta("¡Seleccione archivo", "info");
        }
    } else {
        $("#tbmGrupoHtml").show();
        $("#tbmGrupoHtml").html("Debe ingresar un grupo");
        $("#tbmGrupo").focus();
    }

}

function RespuestaGuardarDocumento(dato) {
    $("#loadingSucessoModal").hide();
    MensajeAlerta("¡Se guardó correctamente! En unos minutos se veran reflejado los registros cargados", "success");
    LimpiarDatosModal();
}

function GuardarDocumentoError(jqXHR, textStatus, errorThrown) {
    $("#loadingSucessoModal").hide();
    console.log(jqXHR);
    MensajeAlerta(jqXHR.responseJSON.mensajes, "error");
}

function LimpiarDatosModal() {
    $("#tbmGrupo").val("");
    $("#__tbIdGrupo").val("");
    $("#__tbTextGrupo").val("");
    $("#__HD_URL_DOCUMENTO").val("");
    document.getElementById("btnSubirArchivo").value = null;
    $("#tbmGrupoHtml").hide();
    $("#infoSucesoModal").hide();
    $("#infoErrorModal").hide();
    $("#loadingSucessoModal").hide();

}

function Detalle(id) {

    $("#verPersonaModal").modal("show");
    //var url = $('#__URL_DETALLE_VEHICULO').val() + "/" + id;
    //var dato = {
    //};
    //realizarGet(url, dato, 'json', RespuestaDetalle, ErrorDetalle, 10000);
}

function RespuestaDetalle(data) {
    LimpiarDatosVehiculoModal();
    $("#verVehiculoModal").modal("show");
    $("#__HD_IdVehiculo").val(data.id);
    $("#tbmPlaca").val(data.placa);
    $("#tbmMarca").val(data.marca);
    $("#tbmModelo").val(data.modelo);
    $("#tbmColor").val(data.color);
    $("#tbmNota").val(data.anotacion);
}

function ErrorDetalle(data) {
    console.log(data);
}


function PreguntarEliminarCategoria(id) {
    MensajeCondicionalUno("¿Desea Eliminar?", "Se borrará del registro de personas", Eliminar, id);
}


function Eliminar(id) {

    var url = $('#__URL_ELIMINAR').val() + "/" + id;
    var dato = {
    };
    realizarGet(url, dato, 'json', RespuestaEliminar, ErrorEliminar, 10000);
}

function RespuestaEliminar(data) {
    $("#loadingSucessoModal").hide();
    MensajeAlerta("¡Se eliminó correctamente! ", "success");
    $('#dataTablePersona').DataTable().ajax.reload();
    LimpiarDatosModal();
}

function ErrorEliminar(data) {
    console.log(data);
}


function LimpiarDatosVehiculoModal() {
    $("#tbmPlaca").val("");
    $("#tbmMarca").val("");
    $("#tbmModelo").val("");
    $("#tbmColor").val("");


    //$("#tbmDni").val("");
    //$("#tbmNombres").val("");
    //$("#tbmPaterno").val("");
    //$("#tbmMaterno").val("");
    //$("#tbmNacimiento").val("");
    //$("#tbmSexo").val("");
    //$("#tbmTelefono").val("");
    //$("#tbmCorreo").val("");
    //$("#tbmDireccion").val("");
    //$("#tbmDistrito").val("");
    //$("#tbmProvincia").val("");
    //$("#tbmDeparmento").val("");
    //$("#tbmNivelSocioeconomico").val("");
    //$("#tbmBienCrediticio").val("");
    //$("#tbmTieneTarjeta").val("");
    //$("#tbmTieneVehiculo").val("");
    //$("#tbmEstaPlanilla").val("");
    //$('#DataCrediticio').html("");
    //$("#tbmDescripcion").val("");
    //$("#tbmDescripcion").val("");
    //$("#tbmNota").val("");
    $("#informacion-tab").trigger("click");
}



function BuscarGrupos() {
    var url = $('#__URL_LISTAR_GRUPO').val();
    var dato = {
    };
    realizarGet(url, dato, 'json', RespuestaBuscarGrupos, ErrorBuscarGrupos, 10000);
}

function RespuestaBuscarGrupos(data) {
    for (var i = 0; i < data.length; i++) {
        $("#ddlGrupo").append('<option value="' + data[i].id + '">' + data[i].nombre + '</option>');
    }
}
function ErrorBuscarGrupos(data) {
    console.log(data);
}

/************************ AUTOCOMPLETAR GRUPO******************************* */
function configurarBusquedaGrupo() {
    var urlData = $("#__URL_BUSCAR_GRUPO").val();
    var docs = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('ocurrencia'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: urlData + '/%QUERY',
            wildcard: '%QUERY'
        }
    });

    $('#tbmGrupo').typeahead(null, {
        name: 'docs',
        display: 'nombre',
        source: docs
    });

    $('#tbmGrupo').bind('typeahead:select', function (ev, suggestion) {
        $('#__tbIdGrupo').val(suggestion.id);
        $('#__tbTextGrupo').val(suggestion.nombre);
    });
}

function GenerarExcel() {
    var parametros = "?a=1";
    parametros += $("#tbPlacaBuscar").val().length > 0 ? "&dni=" + $("#tbPlacaBuscar").val() : "";
    parametros += $("#tbMarca").val().length > 0 ? "&nombreCompleto=" + $("#tbMarca").val() : "";
    parametros += $("#tbModelo").val().length > 0 ? "?sexo=" + $("#tbModelo").val() : "";
    parametros += $("#ddlGrupo").val().length > 0 ? "&idGrupo=" + $("#ddlGrupo").val() : "";
    window.open($('#__URL_GENERAR_EXCEL').val() + parametros, '_blank');

}

function countChars(obj) {
    var maxLength = 2000;
    var strLength = obj.value.length;
    var charRemain = (maxLength - strLength);
    $("#charNum").show();
    if (charRemain <= 0) {
        document.getElementById("charNum").innerHTML = '<span style="color: red;font-size: 11px;">Ha superado el limite de ' + maxLength + ' caracteres</span>';
    } else {
        document.getElementById("charNum").innerHTML = charRemain + ' caracteres restantes';
    }
}

function ActualizarNota() {
    $("#loadingNota").show();
    $("#succesNota").hide();
    $("#errorNota").hide();
    var url = $('#__URL_EDITAR_ANOTACION').val();
    var dato = {
        id: $("#__HD_IdVehiculo").val(),
        anotacion: $("#tbmNota").val(),
    };
    realizarPost(url, dato, 'json', RespuestaActualizarNota, ErrorActualizarNota, 10000);

}

function RespuestaActualizarNota(data) {
    $("#loadingNota").hide();
    $("#succesNota").show();
    setTimeout('LimpiarHtmlNota()', 6000);
}

function ErrorActualizarNota(data) {
    $("#loadingNota").hide();
    $("#errorNota").show();
    setTimeout('LimpiarHtmlNota()', 6000);
}

function LimpiarHtmlNota() {
    $("#loadingNota").hide();
    $("#succesNota").hide();
    $("#errorNota").hide();
}

function MensajeCondicionalUno(titulo, detalle, fun, uno) {
    Swal.fire({
        title: titulo,
        text: detalle,
        icon: "warning",
        showCancelButton: true,
        cancelButtonText: "Cancelar",
        confirmButtonText: "Sí"
    }).then(function (result) {
        if (result.value) {
            fun(uno);
        }
    });
}