﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your Javascript code.

function realizarJsonGet(url, datos, fSuceso, fError, timeoutAjax, noRedireccionar) {
    realizarGet(url, datos, 'json', fSuceso, fError, timeoutAjax, noRedireccionar);
}

function realizarGet(url, datos, tipoRespuesta, fSuceso, fError, timeoutAjax, noRedireccionar) {

    var timeoutDefecto = 1000 * 60;
    if (timeoutAjax) {
        timeoutDefecto = 1000 * timeoutAjax;
    }
    //console.log(timeoutDefecto);

    var xhr = $.ajax({
        type: "GET",
        url: url,
        dataType: tipoRespuesta,
        data: datos,
        success: function (data) {
            if (fSuceso) {
                fSuceso(data);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus + ': ' + errorThrown);
            desbloquearPantalla();

            if (jqXHR.status == 403) {
                alert("Su sesión acaba de expirar. Por favor ingrese de nuevo su login");
                var respuestaRedireccion = JSON.parse(jqXHR.responseText);
                location.href = respuestaRedireccion.urlLogin;
            }

            if (fError) {
                fError(jqXHR, textStatus, errorThrown);
            }

        },
        timeout: timeoutDefecto
    });
    return xhr;
}

function realizarJsonDelete(url, datos, fSuceso, fError, timeoutAjax, noRedireccionar) {
    realizarDelete(url, datos, 'json', fSuceso, fError, timeoutAjax, noRedireccionar);
}

function realizarDelete(url, datos, tipoRespuesta, fSuceso, fError, timeoutAjax, noRedireccionar) {

    var timeoutDefecto = 1000 * 60;
    if (timeoutAjax) {
        timeoutDefecto = 1000 * timeoutAjax;
    }
    //console.log(timeoutDefecto);

    var xhr = $.ajax({
        type: "DELETE",
        url: url,
        dataType: tipoRespuesta,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(datos),
        success: function (data) {
            if (fSuceso) {
                fSuceso(data);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus + ': ' + errorThrown);
            desbloquearPantalla();

            if (jqXHR.status == 403) {
                alert("Su sesión acaba de expirar. Por favor ingrese de nuevo su login");
                var respuestaRedireccion = JSON.parse(jqXHR.responseText);
                location.href = respuestaRedireccion.urlLogin;
            }

            if (fError) {
                fError(jqXHR, textStatus, errorThrown);
            }

        },
        timeout: timeoutDefecto
    });
    return xhr;
}



function realizarJsonPost(url, datos, fSuceso, fError, timeoutAjax, noRedireccionar) {
    realizarPost(url, datos, 'json', fSuceso, fError, timeoutAjax, noRedireccionar);
}

function realizarPost(url, datos, tipoRespuesta, fSuceso, fError, timeoutAjax, noRedireccionar) {

    var timeoutDefecto = 1000 * 60;
    if (timeoutAjax) {
        timeoutDefecto = 1000 * timeoutAjax;
    }
    //console.log(timeoutDefecto);

    var xhr = $.ajax({
        type: "POST",
        url: url,
        dataType: tipoRespuesta,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(datos),
        success: function (data) {
            if (fSuceso) {
                fSuceso(data);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus + ': ' + errorThrown);
            desbloquearPantalla();

            if (jqXHR.status == 403) {
                alert("Su sesión acaba de expirar. Por favor ingrese de nuevo su login");
                var respuestaRedireccion = JSON.parse(jqXHR.responseText);
                location.href = respuestaRedireccion.urlLogin;
            }

            if (fError) {
                fError(jqXHR, textStatus, errorThrown);
            }

        },
        timeout: timeoutDefecto
    });
    return xhr;
}

function manejarJsonBadRequest(jqXHR, idHtml) {
    if (jqXHR.status == 400) {
        var respuesta = JSON.parse(jqXHR.responseText);
        var html = "";

        for (i = 0; i < respuesta.mensajes.length; i++) {
            html += respuesta.mensajes[i] + '<br/>';
        }

        $('#' + idHtml + ' .mensajes').html(html);
        $('#' + idHtml).show();
    }
}

function bloquearPantalla(mensaje) {
}

function desbloquearPantalla() {
}

function obtenerStatus(jqXHR) {
    return jqXHR.status;
}

//******************************************** VALIDACION DE NUMEROS **********************************
function SoloNumeros1_9() {
    var key = window.event ? event.which : event.keyCode;
    if (key < 48 || key > 57) {
        event.preventDefault();
    }
}

function SoloLetrasNumeros() {
    var key = window.event ? event.which : event.keyCode;
    if ((key < 65 || key > 90) && (key < 97 || key > 122)) {
        if (key < 48 || key > 57) {
            event.preventDefault();
        }
    }
}

function BorrarDatosSegunCodigo(s) {
    if (event.which == 8 || event.which == 46) {
        document.getElementById(s).value = "";
    }
}

function ValidarEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}

function delayTime(callback, ms) {
    var timer = 0;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}

//******************************************** CONSTANTES **********************************

const HTTP_CODIGOS = {
    OK: 200,
    BAD_REQUEST: 400,
    NOT_FOUND: 404,
    INTERNAL_SERVER_ERROR: 500
}

Object.freeze(HTTP_CODIGOS);

//***************************************** VALIDAR GRILLA *************************************//
function validarError(xhr) {
    if (xhr.status === 403) {
        var datoError = $.parseJSON(xhr.responseText);
        location.href = datoError.urlLogin;
    } else {
        alert('Ocurrio un error, contactar con soporte');
    }
}

function validarErrorGrilla(xhr, textStatus, error) {
    validarError(xhr);
}

/***************************MENSAJES ALERTA*********************************/
function MensajeAlerta(mensaje, tipo) {
    Swal.fire({
        text: mensaje,
        icon: tipo,
        buttonsStyling: true,
        confirmButtonText: "Cerrar",
        customClass: {
            confirmButton: "btn font-weight-bold btn-light"
        }
    });
}

function MensajeCondicionalUno(titulo, detalle, fun, uno) {
    Swal.fire({
        title: titulo,
        text: detalle,
        icon: "warning",
        showCancelButton: true,
        cancelButtonText: "Cancelar",
        confirmButtonText: "Sí"
    }).then(function (result) {
        if (result.value) {
            fun(uno);
        }
    });
}
