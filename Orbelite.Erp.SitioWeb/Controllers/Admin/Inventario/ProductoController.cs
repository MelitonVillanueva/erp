﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Orbelite.Erp.Servicios.General.Dtos;
using Orbelite.Erp.Servicios.Inventario.Dtos;
using Orbelite.Erp.Servicios.Inventario.Servicios.Abstracciones;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using Orbelite.Principal.Seguridad.Servicios.General.Dtos;
using Orbelite.Principal.Seguridad.Servicios.Peticiones.Servicios.Abstracciones;
using Orbelite.Principal.WebComunes.ApiWeb.Auth.Atributos;
using Orbelite.Principal.WebComunes.WebSite.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Orbelite.Erp.SitioWeb.Controllers.Admin.Inventario
{
    [Route("api/admin/inventario/[controller]")]
    //[ApiController]
    public class ProductoController : ControladorBase
    {
        private readonly UrlConfiguracionDto _urlConfiguracionDto;
        private readonly IProductoServicio _productoServicio;
        private readonly SeguridadConfiguracionDto _seguridadConfiguracionDto;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IPeticionServicio _peticionServicio;

        public ProductoController(
            UrlConfiguracionDto urlConfiguracionDto,
            IProductoServicio productoServicio,
            SeguridadConfiguracionDto seguridadConfiguracionDto,
            IWebHostEnvironment webHostEnvironment,
            IPeticionServicio peticionServicio
            )
        {
            _urlConfiguracionDto = urlConfiguracionDto;
            _productoServicio = productoServicio;
            _seguridadConfiguracionDto = seguridadConfiguracionDto;
            _webHostEnvironment = webHostEnvironment;
            _peticionServicio = peticionServicio;
        }

        [HttpPost("Busqueda")]
        [RequiereAcceso()]
        public async Task<JQueryDatatableDto<ProductoDto>> Busqueda(
            FormDataTable Data,
            string nombre, bool? estaHabilitado)
        {
            var columna = Data.Columns[Data.Order[0].column].data;
            if (Data.Order[0].dir == "desc")
            {
                columna = $"{columna} {"DESC"}";
            }

            var idUsuario = ObtenerIdUsuarioActual();

            var operacion = await _productoServicio.ListarPaginadosAsync(
                 columna, Data.Start, Data.Length, Data.Draw, idUsuario,
                 nombre, estaHabilitado
                );
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }


        [HttpPost("Registrar")]
        [RequiereAcceso()]
        public async Task<ActionResult<RespuestaSimpleDto<string>>> RegistrarProductoAsync([FromBody] ProductoDto parametros)
        {
            parametros.IngresoPorSunat = false;

            var operacion = await _productoServicio.RegistrarProductoAsync(parametros);
                
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }


        [HttpGet("Detalle/{id}")]
        [RequiereAcceso()]
        public async Task<ActionResult<ProductoDto>> DetalleAsync(string id)
        {
            var entidad = new ProductoDto();

            var operacion = await _productoServicio.DetalleAsync(id);

            entidad = operacion.Resultado;

            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }


        [HttpGet("Eliminar/{id}")]
        [RequiereAcceso()]
        public async Task<ActionResult<RespuestaSimpleDto<string>>> EliminarAsync(string id)
        {
            var operacion = await _productoServicio.EliminarAsync(id);
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }


        [HttpPost("Editar")]
        [RequiereAcceso()]
        public async Task<ActionResult<RespuestaSimpleDto<string>>> EditarPersonaAsync([FromBody] ProductoDto parametros)
        {
            var operacion = await _productoServicio.EditarProductoAsync(parametros);

            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }


        [HttpGet("BuscarOcurrencia/{ocurrencia}")]
        [RequiereAcceso()]
        public async Task<List<ProductoDto>> BuscarOcurrenciaAsync(string ocurrencia)
        {
            var operacion = await _productoServicio.ListarPorCoincidenciaAsync(ocurrencia);
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }

    }
}
