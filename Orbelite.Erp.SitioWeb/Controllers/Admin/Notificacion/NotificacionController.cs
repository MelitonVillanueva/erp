﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Orbelite.Erp.Servicios.General.Dtos;
using Orbelite.Erp.Servicios.Notificaciones.Dtos;
using Orbelite.Erp.Servicios.Notificaciones.Servicios.Abstracciones;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using Orbelite.Principal.WebComunes.ApiWeb.Auth.Atributos;
using Orbelite.Principal.WebComunes.WebSite.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Orbelite.Erp.SitioWeb.Controllers.Admin.Notificacion
{
    [Route("api/admin/notificacion/[controller]")]
    //[ApiController]
    public class NotificacionController : ControladorBase
    {

        private readonly IContactoServicio _contactoServicio;
        private readonly INotificacionServicio _notificacionServicio;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public NotificacionController(
            IContactoServicio contactoServicio,
            INotificacionServicio notificacionServicio,
            IWebHostEnvironment webHostEnvironment
            )
        {
            _contactoServicio = contactoServicio;
            _notificacionServicio = notificacionServicio;
            _webHostEnvironment = webHostEnvironment;
        }


        [HttpPost("Busqueda")]
        [RequiereAcceso()]
        public async Task<JQueryDatatableDto<ContactoDto>> Busqueda(
            FormDataTable Data, string documento)
        {

            var columna = Data.Columns[Data.Order[0].column].data;
            if (Data.Order[0].dir == "desc")
            {
                columna = $"{columna} {"DESC"}";
            }

            var idUsuario = ObtenerIdUsuarioActual();

            var operacion = await _contactoServicio.ListarPaginadosAsync(
                 columna, Data.Start, Data.Length, Data.Draw, idUsuario, documento
                );
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);

        }


        [HttpPost("Registrar")]
        [RequiereAcceso()]
        public async Task<ActionResult<RespuestaSimpleDto<string>>> RegistrarPersonaAsync([FromBody] ContactoDto parametros)
        {

            var operacion = await _contactoServicio.RegistrarContactoAsync(parametros);
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);

        }


        [HttpPost("Editar")]
        [RequiereAcceso()]
        public async Task<ActionResult<RespuestaSimpleDto<string>>> EditarPersonaAsync([FromBody] ContactoDto parametros)
        {

            var operacion = await _contactoServicio.EditarContactoAsync(parametros);
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);

        }


        [HttpPost("BusquedaNotificacion")]
        [RequiereAcceso()]
        public async Task<JQueryDatatableDto<NotificacionDto>> BusquedaNotificacion(
            FormDataTable Data, string nombre)
        {

            var columna = Data.Columns[Data.Order[0].column].data;
            if (Data.Order[0].dir == "desc")
            {
                columna = $"{columna} {"DESC"}";
            }

            var idUsuario = ObtenerIdUsuarioActual();

            var operacion = await _notificacionServicio.ListarPaginadosAsync(
                 columna, Data.Start, Data.Length, Data.Draw, idUsuario, nombre
                );
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);

        }


        [HttpPost("RegistrarNotificacion")]
        [RequiereAcceso()]
        public async Task<ActionResult<RespuestaSimpleDto<string>>> RegistrarNotificacionAsync([FromBody] NotificacionDto parametros)
        {

            var operacion = await _notificacionServicio.RegistrarNotificacionAsync(parametros);
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);

        }


        [HttpPost("EditarNotificacion")]
        [RequiereAcceso()]
        public async Task<ActionResult<RespuestaSimpleDto<string>>> EditarNotificacionAsync([FromBody] NotificacionDto parametros)
        {

            var operacion = await _notificacionServicio.EditarNotificacionAsync(parametros);
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);

        }


        [HttpGet("Detalle/{id}")]
        [RequiereAcceso()]
        public async Task<ActionResult<NotificacionDto>> DetalleAsync(string id)
        {
            var entidad = new NotificacionDto();

            var operacion = await _notificacionServicio.DetalleAsync(id);

            entidad = operacion.Resultado;

            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }


        [HttpGet("EliminarNotificacion/{id}")]
        [RequiereAcceso()]
        public async Task<ActionResult<RespuestaSimpleDto<string>>> EliminarAsync(string id)
        {
            var operacion = await _notificacionServicio.EliminarAsync(id);
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }

    }
}
