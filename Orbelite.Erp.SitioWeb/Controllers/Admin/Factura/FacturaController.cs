﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Orbelite.Erp.Servicios.Contabilidad.Dtos;
using Orbelite.Erp.Servicios.Contabilidad.Servicios.Abstracciones;
using Orbelite.Erp.Servicios.Facturas.Dtos;
using Orbelite.Erp.Servicios.Facturas.Servicios.Abstracciones;
using Orbelite.Erp.Servicios.General.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using Orbelite.Principal.WebComunes.ApiWeb.Auth.Atributos;
using Orbelite.Principal.WebComunes.WebSite.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Orbelite.Erp.SitioWeb.Controllers.Admin.Factura
{
    [Route("api/admin/factura/[controller]")]
    //[ApiController]
    public class FacturaController : ControladorBase
    {

        private readonly IFacturaServicio _facturaServicio;
        private readonly IFacturacionServicio _facturacionServicio;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public FacturaController(
            IFacturaServicio facturaServicio,
            IFacturacionServicio facturacionServicio,
            IWebHostEnvironment webHostEnvironment
            )
        {
            _facturaServicio = facturaServicio;
            _facturacionServicio = facturacionServicio;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpPost("ValidarArchivo")]
        [RequiereAcceso()]
        public async Task<ActionResult<CargarFileDto>> ValidarArchivoAsync(IFormFile file)
        {
            var RutaComprimidos = $"{_webHostEnvironment.WebRootPath}\\archivo\\comprimidos\\";
            var RutaDocumentos = $"{_webHostEnvironment.WebRootPath}\\archivo\\documentos\\";
            var idUsuario = ObtenerIdUsuarioActual() ?? 0;
            var operacion = await _facturaServicio.ValidarArchivoAsync(file, RutaComprimidos, RutaDocumentos, idUsuario);
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }

        [HttpPost("CargaListaXml")]
        [RequiereAcceso()]
        public async Task<ActionResult<RespuestaSimpleDto<string>>> CargaListaXmlAsync([FromBody] CargarFileDto peticion)
        {
            //var RutaComprimidos = $"{_webHostEnvironment.WebRootPath}\\archivo\\comprimidos\\";
            //var RutaDocumentos = $"{_webHostEnvironment.WebRootPath}\\archivo\\documentos\\";
            var idUsuario = ObtenerIdUsuarioActual() ?? 0;
            var operacion = await _facturaServicio.ProcesarListaXmlAsync(peticion.ListaCadena, idUsuario, peticion.Habilitado);
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }


        [HttpPost("UltimosCincoBusqueda")]
        [RequiereAcceso()]
        public async Task<JQueryDatatableDto<FacturaDto>> UltimosCincoBusqueda(
            FormDataTable Data)
        {

            var columna = Data.Columns[Data.Order[0].column].data;
            if (Data.Order[0].dir == "desc")
            {
                columna = $"{columna} {"DESC"}";
            }

            var idUsuario = ObtenerIdUsuarioActual();

            var operacion = await _facturacionServicio.ListarUltimosCincoAsync(
                 columna, Data.Draw
                );
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);

        }


        [HttpPost("Busqueda")]
        [RequiereAcceso()]
        public async Task<JQueryDatatableDto<FacturaDto>> Busqueda(
            FormDataTable Data,
            string serie, string ruc, DateTime? emision, DateTime? vencimiento)
        {

            var columna = Data.Columns[Data.Order[0].column].data;
            if (Data.Order[0].dir == "desc")
            {
                columna = $"{columna} {"DESC"}";
            }

            var idUsuario = ObtenerIdUsuarioActual();

            var operacion = await _facturacionServicio.ListarPaginadosAsync(
                 columna, Data.Start, Data.Length, Data.Draw, idUsuario,
                 serie, ruc, emision, vencimiento
                );
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);

        }

        [HttpPost("BusquedaNotaCredito")]
        [RequiereAcceso()]
        public async Task<JQueryDatatableDto<NotaCreditoDto>> BusquedaNotaCredito(
            FormDataTable Data,
            string serie, string ruc, DateTime? emision)
        {

            var columna = Data.Columns[Data.Order[0].column].data;
            if (Data.Order[0].dir == "desc")
            {
                columna = $"{columna} {"DESC"}";
            }

            var idUsuario = ObtenerIdUsuarioActual();

            var operacion = await _facturacionServicio.ListarPaginadosNotaCreditoAsync(
                 columna, Data.Start, Data.Length, Data.Draw, idUsuario,
                 serie, ruc, emision
                );
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);

        }

        [HttpPost("BusquedaNotaDebito")]
        [RequiereAcceso()]
        public async Task<JQueryDatatableDto<NotaDebitoDto>> BusquedaNotaDebito(
            FormDataTable Data,
            string serie, string ruc, DateTime? emision)
        {

            var columna = Data.Columns[Data.Order[0].column].data;
            if (Data.Order[0].dir == "desc")
            {
                columna = $"{columna} {"DESC"}";
            }

            var idUsuario = ObtenerIdUsuarioActual();

            var operacion = await _facturacionServicio.ListarPaginadosNotaDebitoAsync(
                 columna, Data.Start, Data.Length, Data.Draw, idUsuario,
                 serie, ruc, emision
                );
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);

        }


        [HttpPost("BusquedaDetalleFactura")]
        [RequiereAcceso()]
        public async Task<JQueryDatatableDto<DetalleFacturaDto>> BusquedaDetalleFactura(
            FormDataTable Data, long idFactura)
        {

            var columna = Data.Columns[Data.Order[0].column].data;
            if (Data.Order[0].dir == "desc")
            {
                columna = $"{columna} {"DESC"}";
            }

            var idUsuario = ObtenerIdUsuarioActual();

            var operacion = await _facturacionServicio.ListarPaginadosDetalleFacturaAsync(
                 columna, Data.Start, Data.Length, Data.Draw, idUsuario, idFactura
                );
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);

        }

        [HttpPost("BusquedaDetalleNotaCredito")]
        [RequiereAcceso()]
        public async Task<JQueryDatatableDto<DetalleNotaCreditoDto>> BusquedaDetalleNotaCredito(
            FormDataTable Data, long idNotaCredito)
        {

            var columna = Data.Columns[Data.Order[0].column].data;
            if (Data.Order[0].dir == "desc")
            {
                columna = $"{columna} {"DESC"}";
            }

            var idUsuario = ObtenerIdUsuarioActual();

            var operacion = await _facturacionServicio.ListarPaginadosDetalleNotaCreditoAsync(
                 columna, Data.Start, Data.Length, Data.Draw, idUsuario, idNotaCredito
                );
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);

        }

        [HttpPost("BusquedaDetalleNotaDebito")]
        [RequiereAcceso()]
        public async Task<JQueryDatatableDto<DetalleNotaDebitoDto>> BusquedaDetalleNotaDebito(
            FormDataTable Data, long idNotaDebito)
        {

            var columna = Data.Columns[Data.Order[0].column].data;
            if (Data.Order[0].dir == "desc")
            {
                columna = $"{columna} {"DESC"}";
            }

            var idUsuario = ObtenerIdUsuarioActual();

            var operacion = await _facturacionServicio.ListarPaginadosDetalleNotaDebitoAsync(
                 columna, Data.Start, Data.Length, Data.Draw, idUsuario, idNotaDebito
                );
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);

        }


        [HttpGet("BuscarOcurrenciaFactura/{ocurrencia}")]
        [RequiereAcceso()]
        public async Task<List<FacturaDto>> BuscarOcurrenciaFacturaAsync(string ocurrencia)
        {
            var operacion = await _facturacionServicio.ListarNotaFacturaPorCoincidenciaAsync(ocurrencia);
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }


        [HttpGet("BuscarOcurrenciaNotaCredito/{ocurrencia}")]
        [RequiereAcceso()]
        public async Task<List<NotaCreditoDto>> BuscarOcurrenciaNotaCreditoAsync(string ocurrencia)
        {
            var operacion = await _facturacionServicio.ListarNotaCreditoPorCoincidenciaAsync(ocurrencia);
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }


        [HttpGet("BuscarOcurrenciaNotaDebito/{ocurrencia}")]
        [RequiereAcceso()]
        public async Task<List<NotaDebitoDto>> BuscarOcurrenciaNotaDebitoAsync(string ocurrencia)
        {
            var operacion = await _facturacionServicio.ListarNotaDebitoPorCoincidenciaAsync(ocurrencia);
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }

    }
}
