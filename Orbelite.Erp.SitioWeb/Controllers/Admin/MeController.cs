﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Orbelite.Erp.Servicios.Admin.Me.Dtos;
using Orbelite.Principal.Seguridad.Datos.Repositorios.Abstracciones;
using Orbelite.Principal.Seguridad.Servicios.Auth.Servicios.Abstracciones;
using Orbelite.Principal.Seguridad.Servicios.General.Dtos;
using Orbelite.Principal.Seguridad.Servicios.Peticiones.Servicios.Abstracciones;
using Orbelite.Principal.WebComunes.ApiWeb.Auth.Atributos;
using Orbelite.Principal.WebComunes.WebSite.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Orbelite.Erp.SitioWeb.Controllers.Admin
{
    [Route("api/admin/[controller]")]
    [ApiController]
    public class MeController : ControladorBase
    {
        private readonly UrlConfiguracionDto _urlConfiguracionDto;
        private readonly SeguridadConfiguracionDto _seguridadConfiguracionDto;
        private readonly IPeticionServicio _peticionServicio;
        private readonly IPersonaRepositorio _personaRepositorio;
        private readonly IUsuarioServicio _usuarioServicio;
        public MeController(
                             UrlConfiguracionDto urlConfiguracionDto,
                             SeguridadConfiguracionDto seguridadConfiguracionDto,
                             IPeticionServicio peticionServicio,
                             IPersonaRepositorio personaRepositorio,
                             IUsuarioServicio usuarioServicio)
        {
            _urlConfiguracionDto = urlConfiguracionDto;
            _seguridadConfiguracionDto = seguridadConfiguracionDto;
            _peticionServicio = peticionServicio;
            _personaRepositorio = personaRepositorio;
            _usuarioServicio = usuarioServicio;
        }

        [HttpPost("DatosUsuario")]
        [RequiereAcceso]
        public async Task<IActionResult> DatosUsuario()
        {
            var persona = await _personaRepositorio.BuscarPorIdUsuarioAsync((ObtenerIdUsuarioActual() ?? 0));
            var cabecera = new Dictionary<string, string>();
            cabecera.Add("app-name", _seguridadConfiguracionDto.AppName);
            var peticion = _peticionServicio.RealizarJsonPost($"{_urlConfiguracionDto.UrlApiConsultas}api/consultas/Persona/Consulta", new { Dni = persona.Dni }, ObtenerIdUsuarioActual() ?? 0, cabecera);
            return ObtenerResultadoOGenerarErrorDeSimpleRespuestaPeticionWebDto(peticion);
        }

        [HttpPost("HistotrialCrediticio")]
        [RequiereAcceso]
        public async Task<IActionResult> HistotrialCrediticio()
        {
            var persona = await _personaRepositorio.BuscarPorIdUsuarioAsync((ObtenerIdUsuarioActual() ?? 0));
            var cabecera = new Dictionary<string, string>();
            cabecera.Add("app-name", _seguridadConfiguracionDto.AppName);
            var peticion = _peticionServicio.RealizarJsonPost($"{_urlConfiguracionDto.UrlApiFuentes}api/persona/consultas/Sbs/Historial", new { documento = persona.Dni, tipo = (string.IsNullOrWhiteSpace(persona.IdTipo.ToString()) ? 100 : persona.IdTipo) }, ObtenerIdUsuarioActual() ?? 0, cabecera);
            return ObtenerResultadoOGenerarErrorDeSimpleRespuestaPeticionWebDto(peticion);
        }

        [HttpPost("CrediticioDetallePrimero")]
        [RequiereAcceso]
        public IActionResult CrediticioDetallePrimero([FromBody] ConsultarSbsCalificacionDetallePeticionDto parametro)
        {
            var cabecera = new Dictionary<string, string>();
            cabecera.Add("app-name", _seguridadConfiguracionDto.AppName);
            var peticion = _peticionServicio.RealizarJsonPost($"{_urlConfiguracionDto.UrlApiFuentes}api/persona/consultas/Sbs/DetallePrimero", new { id = parametro.Id }, ObtenerIdUsuarioActual() ?? 0, cabecera);
            return ObtenerResultadoOGenerarErrorDeSimpleRespuestaPeticionWebDto(peticion);
        }

        [HttpPost("CrediticioDetalleSegundo")]
        [RequiereAcceso]
        public IActionResult CrediticioDetalleSegundo([FromBody] ConsultarSbsCalificacionDetallePeticionDto parametro)
        {
            var cabecera = new Dictionary<string, string>();
            cabecera.Add("app-name", _seguridadConfiguracionDto.AppName);
            var peticion = _peticionServicio.RealizarJsonPost($"{_urlConfiguracionDto.UrlApiFuentes}api/persona/consultas/Sbs/DetalleSegundo", new { id = parametro.Id }, ObtenerIdUsuarioActual() ?? 0, cabecera);
            return ObtenerResultadoOGenerarErrorDeSimpleRespuestaPeticionWebDto(peticion);
        }

        [HttpPost("CrediticioDetalleTercero")]
        [RequiereAcceso]
        public IActionResult CrediticioDetalleTercero([FromBody] ConsultarSbsCalificacionDetallePeticionDto parametro)
        {
            var cabecera = new Dictionary<string, string>();
            cabecera.Add("app-name", _seguridadConfiguracionDto.AppName);
            var peticion = _peticionServicio.RealizarJsonPost($"{_urlConfiguracionDto.UrlApiFuentes}api/persona/consultas/Sbs/DetalleTercero", new { id = parametro.Id }, ObtenerIdUsuarioActual() ?? 0, cabecera);
            return ObtenerResultadoOGenerarErrorDeSimpleRespuestaPeticionWebDto(peticion);
        }
    }
}
