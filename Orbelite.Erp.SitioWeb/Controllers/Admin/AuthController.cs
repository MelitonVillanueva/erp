﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Orbelite.Erp.Servicios.Admin.Auth.Usuarios.Dtos;
using Orbelite.Erp.Servicios.Auth.Usuarios.Servicios.Abstracciones;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using Orbelite.Principal.Seguridad.Datos.Repositorios.Abstracciones;
using Orbelite.Principal.Seguridad.Servicios.Auth.Servicios.Abstracciones;
using Orbelite.Principal.Seguridad.Servicios.General.Dtos;
using Orbelite.Principal.Seguridad.Servicios.Peticiones.Servicios.Abstracciones;
using Orbelite.Principal.WebComunes.ApiWeb.Auth.Atributos;
using Orbelite.Principal.WebComunes.WebSite.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Orbelite.Erp.SitioWeb.Controllers.Admin
{
    [Route("api/admin/[controller]")]
    [ApiController]
    public class AuthController : ControladorAuth
    {
        private readonly UrlConfiguracionDto _urlConfiguracionDto;
        private readonly SeguridadConfiguracionDto _seguridadConfiguracionDto;
        private readonly IPeticionServicio _peticionServicio;
        private readonly IPersonaRepositorio _personaRepositorio;
        private readonly ILoginServicio _loginServicio;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IRecuperarContraseniaServicio _recuperarContraseniaServicio;

        public AuthController(IOptionsMonitor<CookieAuthenticationOptions> optionsMonitor,
            UrlConfiguracionDto urlConfiguracionDto,
            SeguridadConfiguracionDto seguridadConfiguracionDto,
            IPeticionServicio peticionServicio,
            IPersonaRepositorio personaRepositorio,
            ILoginServicio loginServicio,
            IWebHostEnvironment hostingEnvironment,
            IRecuperarContraseniaServicio recuperarContraseniaServicio
            ) : base(optionsMonitor)
        {
            _urlConfiguracionDto = urlConfiguracionDto;
            _seguridadConfiguracionDto = seguridadConfiguracionDto;
            _peticionServicio = peticionServicio;
            _personaRepositorio = personaRepositorio;
            _loginServicio = loginServicio;
            _hostingEnvironment = hostingEnvironment;
            _recuperarContraseniaServicio = recuperarContraseniaServicio;
        }


        [HttpPost("OlvidoContrasenia")]
        public async Task<RespuestaSimpleDto<bool>> OlvidoContrasenia([FromBody] RecuperarContraseniaDto peticion)
        {
            peticion.Plantilla = $"{_hostingEnvironment.WebRootPath}\\archivos\\htmlRecuperarContrasenia.html";
            var operacion = await _recuperarContraseniaServicio.EnviarCorreoRecuperarContrasenia(peticion);
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }


        [HttpGet("Crediticio/{dni}")]
        [RequiereAcceso]
        public IActionResult Crediticio(string dni)
        {
            var cabecera = new Dictionary<string, string>();
            cabecera.Add("app-name", _seguridadConfiguracionDto.AppName);
            var peticion = _peticionServicio.RealizarJsonPost($"{_urlConfiguracionDto.UrlApiFuentes}api/persona/consultas/Sbs/Consultar", new { Documento = dni, Tipo = 100 }, ObtenerIdUsuarioActual() ?? 0, cabecera);
            return ObtenerResultadoOGenerarErrorDeSimpleRespuestaPeticionWebDto(peticion);
        }

        [HttpPost("Tansacciones")]
        [RequiereAcceso]
        public IActionResult Tansacciones()
        {
            var peticion = _peticionServicio.RealizarJsonPost($"{_urlConfiguracionDto.UrlSmkt}api/Transaccion/TransaccionesPorAgente", new { Pagina = 1, TamanioPagina = 10 }, ObtenerIdUsuarioActual() ?? 0);
            return ObtenerResultadoOGenerarErrorDeSimpleRespuestaPeticionWebDto(peticion);
        }

        [HttpGet("LoginAplicacion/{token}")]
        public async Task<ActionResult> LoginAplicacion(string token)
        {
            try
            {
                var operacion = await LoginApp(token);
                if (operacion.Suceso)
                {
                    return RedirectToPage("/Admin/Index");
                }
            }
            catch
            { }
            return RedirectToPage("/Admin/Login");
        }

        [HttpGet("AppIbroker")]
        [RequiereAcceso]
        public async Task<ActionResult> AppIbroker()
        {
            var operacion = await _loginServicio.ObtenerTokenSesionApp(ObtenerIdUsuarioActual() ?? 0);
            return Redirect($"{_urlConfiguracionDto.UrlIbroker}api/admin/Auth/AppIbroker/{operacion.Resultado.Id}");
        }

        //[HttpGet("AppInfraxion")]
        //[RequiereAcceso]
        //public async Task<ActionResult> AppInfraxion()
        //{
        //    var operacion = await _loginServicio.ObtenerTokenSesionApp(ObtenerIdUsuarioActual() ?? 0);
        //    return Redirect($"{_urlConfiguracionDto.UrlInfraxion}Auth/LoginAplicativo?id={operacion.Resultado.Id}");
        //}
        [HttpGet("AppXkaner")]
        [RequiereAcceso]
        public async Task<ActionResult> AppXkaner()
        {
            var operacion = await _loginServicio.ObtenerTokenSesionApp(ObtenerIdUsuarioActual() ?? 0);
            return Redirect($"{_urlConfiguracionDto.UrlEscaner}api/admin/Auth/AppXkaner/{operacion.Resultado.Id}");
        }

        [HttpGet("AppAliados")]
        [RequiereAcceso]
        public async Task<ActionResult> AppAliados()
        {
            var operacion = await _loginServicio.ObtenerTokenSesionApp(ObtenerIdUsuarioActual() ?? 0);
            return Redirect($"{_urlConfiguracionDto.UrlAliadosAdmin}api/administrador/Auth/LoginAplicacion/{operacion.Resultado.Id}");
        }

        [HttpGet("AppTaxiCoolmena")]
        [RequiereAcceso]
        public async Task<ActionResult> AppTaxiCoolmena()
        {
            var operacion = await _loginServicio.ObtenerTokenSesionApp(ObtenerIdUsuarioActual() ?? 0);
            return Redirect($"{_urlConfiguracionDto.UrlTaxiCoolmena}api/admin/Auth/LoginAplicacion/{operacion.Resultado.Id}");
        }

        [HttpGet("AppKme")]
        [RequiereAcceso]
        public async Task<ActionResult> AppKme()
        {
            var operacion = await _loginServicio.ObtenerTokenSesionApp(ObtenerIdUsuarioActual() ?? 0);
            return Redirect($"{_urlConfiguracionDto.UrlKme}api/admin/Auth/LoginAplicacion/{operacion.Resultado.Id}");
        }
    }
}
