﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Orbelite.Erp.Servicios.Entidad.Dtos;
using Orbelite.Erp.Servicios.Entidad.Servicios.Abstracciones;
using Orbelite.Erp.Servicios.General.Dtos;
using Orbelite.Erp.SitioWeb.Infraestructura.Helpers;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using Orbelite.Principal.Seguridad.Servicios.General.Dtos;
using Orbelite.Principal.Seguridad.Servicios.Peticiones.Servicios.Abstracciones;
using Orbelite.Principal.WebComunes.ApiWeb.Auth.Atributos;
using Orbelite.Principal.WebComunes.WebSite.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Orbelite.Erp.SitioWeb.Controllers.Admin.Entidad
{
    [Route("api/admin/entidad/[controller]")]
    //[ApiController]
    public class PersonaController : ControladorBase
    {
        private readonly UrlConfiguracionDto _urlConfiguracionDto;
        private readonly IPersonaServicio _personaServicio;
        private readonly SeguridadConfiguracionDto _seguridadConfiguracionDto;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IPeticionServicio _peticionServicio;

        public PersonaController(
            UrlConfiguracionDto urlConfiguracionDto,
            IPersonaServicio personaServicio,
            SeguridadConfiguracionDto seguridadConfiguracionDto,
            IWebHostEnvironment webHostEnvironment,
            IPeticionServicio peticionServicio
            )
        {
            _urlConfiguracionDto = urlConfiguracionDto;
            _personaServicio = personaServicio;
            _seguridadConfiguracionDto = seguridadConfiguracionDto;
            _webHostEnvironment = webHostEnvironment;
            _peticionServicio = peticionServicio;
        }

        [HttpPost("Busqueda")]
        [RequiereAcceso()]
        public async Task<JQueryDatatableDto<PersonaDto>> Busqueda(
            FormDataTable Data,
            string documento, string nombres, string paterno, string materno, string razon)
        {
            var columna = Data.Columns[Data.Order[0].column].data;
            if (Data.Order[0].dir == "desc")
            {
                columna = $"{columna} {"DESC"}";
            }

            var idUsuario = ObtenerIdUsuarioActual();

            var operacion = await _personaServicio.ListarPaginadosAsync(
                 columna, Data.Start, Data.Length, Data.Draw, idUsuario,
                 documento, nombres, paterno, materno, razon
                );
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }


        [HttpPost]
        [Route("ObtenerPersona")]
        public async Task<ActionResult<PersonaDto>> ObtenerPersona([FromBody] PersonaDto parametros)
        {
            await Task.Delay(0);

            var entidad = new PersonaDto()
            {
                IdTipo = parametros.IdTipo,
                Documento = parametros.Documento,
                Nombres = parametros.Nombres,
                RazonSocial = parametros.RazonSocial,
                Paterno = parametros.Paterno,
                Materno = parametros.Materno,
                Correo = parametros.Correo,
                Telefono = parametros.Telefono
            };

            HttpContext.Session.SetObjectAsJson("Entidad", entidad);

            var operacion = new OperacionDto<PersonaDto>(entidad);

            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }


        [HttpPost("Registrar")]
        [RequiereAcceso()]
        public async Task<ActionResult<RespuestaSimpleDto<string>>> RegistrarPersonaAsync([FromBody] PersonaDto parametros)
        {
            parametros.IngresoPorSunat = false;

            var operacion = await _personaServicio.RegistrarPersonaAsync(parametros);

            HttpContext.Session.Remove("Entidad");

            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }


        [HttpGet("Consulta/{dni}")]
        [RequiereAcceso]
        public IActionResult Consulta(string dni)
        {
            var cabecera = new Dictionary<string, string>();
            cabecera.Add("app-name", _seguridadConfiguracionDto.AppName);
            var peticion = _peticionServicio.RealizarJsonPost($"{_urlConfiguracionDto.UrlApiConsultas}api/consultas/Persona/Consulta", new { Dni = dni }, ObtenerIdUsuarioActual() ?? 0, cabecera);
            return ObtenerResultadoOGenerarErrorDeSimpleRespuestaPeticionWebDto(peticion);
        }

        [HttpGet("ConsultaRuc/{ruc}")]
        [RequiereAcceso]
        public IActionResult ConsultaRuc(string ruc)
        {
            var cabecera = new Dictionary<string, string>();
            cabecera.Add("app-name", _seguridadConfiguracionDto.AppName);
            //var peticion = _peticionServicio.RealizarJsonPost($"{_urlConfiguracionDto.UrlApiConsultas}api/consultas/Empresa/ConsultaSimple", new { Ruc = ruc }, ObtenerIdUsuarioActual() ?? 0, cabecera);
            var peticion = _peticionServicio.RealizarJsonPost($"http://161.132.211.211/Consultas/api/Empresa/buscar", new { Ruc = ruc }, ObtenerIdUsuarioActual() ?? 0, cabecera);
            return ObtenerResultadoOGenerarErrorDeSimpleRespuestaPeticionWebDto(peticion);
        }

        [HttpGet("Detalle/{id}")]
        [RequiereAcceso()]
        public async Task<ActionResult<PersonaDto>> DetalleAsync(string id)
        {
            var entidad = new PersonaDto();

            var operacion = await _personaServicio.DetalleAsync(id);

            entidad = operacion.Resultado;

            HttpContext.Session.SetObjectAsJson("Entidad", entidad);

            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }

        [HttpGet("Eliminar/{id}")]
        [RequiereAcceso()]
        public async Task<ActionResult<RespuestaSimpleDto<string>>> EliminarAsync(string id)
        {
            var operacion = await _personaServicio.EliminarAsync(id);
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }

        [HttpPost("Editar")]
        [RequiereAcceso()]
        public async Task<ActionResult<RespuestaSimpleDto<string>>> EditarPersonaAsync([FromBody] PersonaDto parametros)
        {
            var operacion = await _personaServicio.EditarPersonaAsync(parametros);

            //HttpContext.Session.Remove("Entidad");

            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }

        [HttpGet("BuscarOcurrencia/{ocurrencia}")]
        [RequiereAcceso()]
        public async Task<List<PersonaDto>> BuscarOcurrenciaAsync(string ocurrencia)
        {
            var operacion = await _personaServicio.ListarPorCoincidenciaAsync(ocurrencia);
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }

        [HttpGet("DetalleDoc/{documento}")]
        [RequiereAcceso()]
        public async Task<ActionResult<PersonaDto>> DetalleDocAsync(string documento)
        {
            var entidad = new PersonaDto();

            var operacion = await _personaServicio.BuscarPorDocumento(documento);

            entidad = operacion.Resultado;

            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }

    }
}
