﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Orbelite.Erp.Servicios.Configuraciones.Ubigeo.Dtos;
using Orbelite.Erp.Servicios.Configuraciones.Ubigeo.Servicios.Abstracciones;
using Orbelite.Principal.WebComunes.ApiWeb.Auth.Atributos;
using Orbelite.Principal.WebComunes.ApiWeb.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Orbelite.Erp.SitioWeb.Controllers.General
{
    [Route("api/general/[controller]")]
    [ApiController]
    public class UbigeoController : ApiControladorBase
    {
        private readonly IUbigeoServicio _ubigeoServicio;

        public UbigeoController(IUbigeoServicio ubigeoServicio)
        {
            _ubigeoServicio = ubigeoServicio;
        }


        [HttpPost("Provincias")]
        [RequiereAcceso()]
        public async Task<List<UbigeoDto>> Provincias(UbigeoDto peticion)
        {
            var operacion = await _ubigeoServicio.BuscarProvincias(peticion.Nombre);
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }

        [HttpPost("Distritos")]
        [RequiereAcceso()]
        public async Task<List<UbigeoDto>> Distritos(UbigeoDto peticion)
        {
            var operacion = await _ubigeoServicio.BuscarDistritos(peticion.Nombre);
            return ObtenerResultadoOGenerarErrorDeOperacion(operacion);
        }
    }
}
