﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Module = Autofac.Module;
using Microsoft.Extensions.Configuration;
using Autofac;
using Orbelite.Erp.Servicios.General.Dtos;

namespace Orbelite.Erp.SitioWeb.Infraestructura.Autofac
{
    public class ConfiguracionModule : Module
    {
        private readonly IConfiguration _configuration;

        public ConfiguracionModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {            
            builder.Register(e => new ConfiguracionDto()
            {
                General = new GeneralDto()
                {
                    WhatsApp = _configuration["general:whatsApp"],
                    CorreoLandingIbroker = _configuration["general:correoLandingIbroker"],
                    ApiKeyGoogle = _configuration["general:ApiKeyGoogle"],
                    UrlSitioPersonalizadoBase = _configuration["general:urlSitioPersonalizadoBase"],
                    UrlBase = _configuration["general:urlBase"],
                    UrlApiConsultas = _configuration["general:urlApiConsultas"],
                    RutaArhivos = _configuration["general:rutaArhivos"],
                    UrlApiEscaner = _configuration["general:urlApiEscaner"],
                    UrlEscaner = _configuration["general:urlEscaner"],                    
                    UrlInfraxion = _configuration["general:urlInfraxion"],
                    UrlInfraxionPro = _configuration["general:urlInfraxionPro"],
                    UrlBasePersonalizado = _configuration["general:urlBasePersonalizado"],
                    UrlBaseUrbelite = _configuration["general:urlBaseUrbelite"],
                    UrlAutoencePersonalizado = _configuration["general:urlAutoencePersonalizado"],
                    
                },

                Email = new EmailDto()
                {
                    Email_Host = _configuration["email:host"],
                    Email_Port = _configuration["email:port"],
                    Email_Usuario = _configuration["email:usuario"],
                    Email_From = _configuration["email:from"],
                    Email_Psw = _configuration["email:psw"]
                },

                Recaptcha = new RecaptchaDto()
                {
                    ClaveSitioWeb = _configuration["recaptcha:claveSitioWeb"],
                    ClaveSecrera = _configuration["recaptcha:claveSecreta"]
                },

                 Visa = new VisaDto()
                 {
                     Usuario = _configuration["visa:usuario"],
                     Password = _configuration["visa:password"],
                     IdMercader = _configuration["visa:idMercader"],
                     TimeOut = _configuration["visa:timeout"],
                     Javascript = _configuration["visa:javascript"],
                     UrlToken = _configuration["visa:urlToken"],
                     UrlSesion = _configuration["visa:urlSesion"],
                     UrlAutorizacion = _configuration["visa:urlAutorizacion"]                     
                 }

            }).InstancePerLifetimeScope();
        }
    }
}
