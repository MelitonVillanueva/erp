using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Orbelite.Erp.SitioWeb.Pages.Admin
{
    public class LoginModel : PageModel
    {
        public string UrlDireccionar { get; set; }
        public void OnGet(string ReturnUrl)
        {
            UrlDireccionar = ReturnUrl;
        }
    }
}
