using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Orbelite.Erp.Servicios.Configuraciones.Ubigeo.Dtos;
using Orbelite.Erp.Servicios.Configuraciones.Ubigeo.Servicios.Abstracciones;
using Orbelite.Erp.Servicios.Entidad.Dtos;
using Orbelite.Erp.Servicios.Entidad.Servicios.Abstracciones;
using Orbelite.Erp.SitioWeb.Infraestructura.Helpers;

namespace Orbelite.Erp.SitioWeb.Pages.Admin.Productos
{
    public class DetalleProductoModel : PageModel
    {

        private readonly IPersonaServicio _personaServicio;
        private readonly IUbigeoServicio _ubigeoServicio;
        public PersonaDto Persona { get; set; }
        public List<UbigeoDto> Departamentos { get; set; }
        public List<UbigeoDto> Provincias { get; set; }
        public List<UbigeoDto> Distritos { get; set; }

        public DetalleProductoModel(
                          IPersonaServicio personaServicio,
                          IUbigeoServicio ubigeoServicio
                         )
        {
            _personaServicio = personaServicio;
            _ubigeoServicio = ubigeoServicio;


            Persona = new PersonaDto();
            Provincias = new List<UbigeoDto>();
            Distritos = new List<UbigeoDto>();

        }


        public async Task OnGet(string Id)
        {

            ViewData["Id"] = Id;

            //Persona = HttpContext.Session.GetObjectFromJson<PersonaDto>("Entidad");

            var operacionDep = await _ubigeoServicio.BuscarDepartamentos();
            if (operacionDep.Completado)
            {
                Departamentos = operacionDep.Resultado;
            }

            var operacionPersona = await _personaServicio.DetalleAsync(Id);
            if (operacionPersona.Completado)
            {
                Persona = operacionPersona.Resultado;

                if (!string.IsNullOrEmpty(operacionPersona.Resultado.Departamento))
                {
                    var operacionPro = await _ubigeoServicio.BuscarProvincias(operacionPersona.Resultado.Departamento);
                    if (operacionPro.Completado)
                    {
                        Provincias = operacionPro.Resultado;
                    }
                }

                if (!string.IsNullOrEmpty(operacionPersona.Resultado.Provincia))
                {
                    var operacionDis = await _ubigeoServicio.BuscarDistritos(operacionPersona.Resultado.Provincia);
                    if (operacionDis.Completado)
                    {
                        Distritos = operacionDis.Resultado;
                    }
                }
            }               

        }
    }
}
