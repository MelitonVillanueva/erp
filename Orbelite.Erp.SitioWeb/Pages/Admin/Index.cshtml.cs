using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Orbelite.Principal.Seguridad.Datos.Modelo;
using Orbelite.Principal.Seguridad.Datos.Repositorios.Abstracciones;

namespace Orbelite.Erp.SitioWeb.Pages.Admin
{
    public class IndexModel : PageModel
    {
        private readonly IPersonaRepositorio _personaRepositorio;
        public Persona Persona { get; set; }

        public IndexModel(
                           IPersonaRepositorio personaRepositorio
           )
        {

            _personaRepositorio = personaRepositorio;
        }

        public async Task OnGet()
        {
            var claim = HttpContext.User.Claims.SingleOrDefault(m => m.Type == ClaimTypes.NameIdentifier);
            long idUsuario = 0;
            if (claim != null)
            {
                idUsuario = Convert.ToInt64(claim.Value);
            }

            var personaOperacion = await _personaRepositorio.BuscarPorIdUsuarioAsync(idUsuario);
            if (personaOperacion != null)
            {
                Persona = personaOperacion;
            }
                      

        }
    }
}
