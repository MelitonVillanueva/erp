using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Orbelite.Erp.Servicios.Contabilidad.Dtos;
using Orbelite.Erp.Servicios.Contabilidad.Servicios.Abstracciones;

namespace Orbelite.Erp.SitioWeb.Pages.Admin.Contabilidad
{
    public class DetalleNotasCreditoModel : PageModel
    {
        private readonly IFacturacionServicio _facturacionServicio;
        public NotaCreditoDto NotaCredito { get; set; }
        public List<DetalleNotaCreditoDto> DetalleNotaCredito { get; set; }

        public DetalleNotasCreditoModel(
            IFacturacionServicio facturacionServicio
            )
        {
            _facturacionServicio = facturacionServicio;
        }

        public async Task OnGet(string Id)
        {
            ViewData["IdNotaCredito"] = Id;

            var operacionFactura = await _facturacionServicio.ObtenerNotaCreditoAsync(Id);
            if (operacionFactura.Completado)
            {
                NotaCredito = operacionFactura.Resultado;
            }

            var operacionDetalleNotaCredito = await _facturacionServicio.ListarDetalleNotaCreditoAsync(Id);
            if (operacionDetalleNotaCredito.Completado)
            {
                DetalleNotaCredito = operacionDetalleNotaCredito.Resultado;
            }

        }
    }
}
