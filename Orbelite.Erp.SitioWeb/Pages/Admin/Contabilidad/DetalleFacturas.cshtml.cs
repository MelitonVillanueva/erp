using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Orbelite.Erp.Servicios.Contabilidad.Dtos;
using Orbelite.Erp.Servicios.Contabilidad.Servicios.Abstracciones;

namespace Orbelite.Erp.SitioWeb.Pages.Admin.Contabilidad
{
    public class DetalleFacturasModel : PageModel
    {

        private readonly IFacturacionServicio _facturacionServicio;
        public FacturaDto Factura { get; set; }
        public List<DetalleFacturaDto> DetalleFactura { get; set; }

        public DetalleFacturasModel(
            IFacturacionServicio facturacionServicio
            )
        {
            _facturacionServicio = facturacionServicio;
        }

        public async Task OnGet(string Id)
        {
            ViewData["IdFactura"] = Id;

            var operacionFactura = await _facturacionServicio.ObtenerFacturaAsync(Id);
            if (operacionFactura.Completado)
            {
                Factura = operacionFactura.Resultado;
            }

            var operacionDetalleFactura = await _facturacionServicio.ListarDetalleFacturaAsync(Id);
            if (operacionDetalleFactura.Completado)
            {
                DetalleFactura = operacionDetalleFactura.Resultado;
            }

        }
    }
}
