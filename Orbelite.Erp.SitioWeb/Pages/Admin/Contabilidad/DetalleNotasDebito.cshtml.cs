using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Orbelite.Erp.Servicios.Contabilidad.Dtos;
using Orbelite.Erp.Servicios.Contabilidad.Servicios.Abstracciones;

namespace Orbelite.Erp.SitioWeb.Pages.Admin.Contabilidad
{
    public class DetalleNotasDebitoModel : PageModel
    {

        private readonly IFacturacionServicio _facturacionServicio;
        public NotaDebitoDto NotaDebito { get; set; }
        public List<DetalleNotaDebitoDto> DetalleNotaDebito { get; set; }

        public DetalleNotasDebitoModel(
            IFacturacionServicio facturacionServicio
            )
        {
            _facturacionServicio = facturacionServicio;
        }

        public async Task OnGet(string Id)
        {
            ViewData["IdNotaDebito"] = Id;

            var operacionFactura = await _facturacionServicio.ObtenerNotaDebitoAsync(Id);
            if (operacionFactura.Completado)
            {
                NotaDebito = operacionFactura.Resultado;
            }

            var operacionDetalleNotaDebito = await _facturacionServicio.ListarDetalleNotaDebitoAsync(Id);
            if (operacionDetalleNotaDebito.Completado)
            {
                DetalleNotaDebito = operacionDetalleNotaDebito.Resultado;
            }

        }
    }
}
