using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor.RuntimeCompilation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Orbelite.Erp.Datos.Infraestructura.Configuraciones.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Configuraciones.Implementaciones;
using Orbelite.Erp.Datos.Infraestructura.Contextos.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Contextos.Implementaciones;
using Orbelite.Erp.SitioWeb.Infraestructura.Autofac;
using Orbelite.Principal.Cargadores.Bd;
using Orbelite.Principal.Cargadores.Generales;
using Orbelite.Principal.ComponentesWeb.ViewComponents.Auth;
using Orbelite.Principal.Seguridad.Infraestructura.Modulos;
using Orbelite.Principal.WebComunes.Infraestructura.Errores;
using Orbelite.Principal.WebSeguridad.Infraestructura.Modulos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Orbelite.Erp.SitioWeb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();
            services.AddHttpContextAccessor();
            services.AddRazorPages(
                options =>
                {
                    options.Conventions.AuthorizeFolder("/Admin");
                    options.Conventions.AllowAnonymousToPage("/Admin/Login");
                    options.Conventions.AllowAnonymousToPage("/Admin/Auth/OlvidoContrasenia");
                    options.Conventions.AllowAnonymousToPage("/Admin/Auth/NuevaContrasenia");
                })
            .AddRazorRuntimeCompilation()
            .AddRazorPagesOptions(options =>
            {
                options.Conventions.AddPageRoute("/Admin/Login", "");
            });

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            var assemblyComponentes = typeof(AuthUsuarioMenuDerechoViewComponent).Assembly;
            services.Configure<MvcRazorRuntimeCompilationOptions>(options =>
            {
                options.FileProviders.Add(new EmbeddedFileProvider(assemblyComponentes));
            });

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromHours(8);
            });

            services.AddAutofac();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
               .AddCookie(config =>
               {
                   config.LoginPath = "/Admin/Login";
               });
            services.AddAutoMapper(Assembly.Load("Orbelite.Erp.Servicios"));
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new SeguridadModule(Configuration));
            builder.RegisterModule(new WebSeguridadModule(Configuration, "erp"));
            builder.RegisterModule(new
                CargarSqlServerEF<ErpContexto,
                IErpConfiguracion,
                ErpConfiguracion, IErpUnidadDeTrabajo>(Configuration, "erp", "Orbelite.Erp.Datos"));

            builder.RegisterModule(new ServicioCargador("Orbelite.Erp.Servicios"));
            builder.RegisterModule(new ConfiguracionModule(Configuration));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseExceptionHandler(errorApp => errorApp.UseCustomErrors(env, true));

            app.UseAuthentication();
            app.UseAuthorization();
            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
            });
        }
    }
}
