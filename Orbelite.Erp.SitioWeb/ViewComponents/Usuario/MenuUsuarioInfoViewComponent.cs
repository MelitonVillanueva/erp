﻿using Microsoft.AspNetCore.Mvc;
using Orbelite.Principal.Seguridad.Datos.Repositorios.Abstracciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Orbelite.Erp.SitioWeb.ViewComponents.Usuario
{
    public class MenuUsuarioInfoViewComponent: ViewComponent
    {
        private readonly IPersonaRepositorio _personaRepositorio;

        public MenuUsuarioInfoViewComponent(
            IPersonaRepositorio personaRepositorio
            )
        {

            _personaRepositorio = personaRepositorio;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var claim = HttpContext.User.Claims.SingleOrDefault(m => m.Type == ClaimTypes.NameIdentifier);
            long idUsuario = 0;
            if (claim != null)
            {
                idUsuario = Convert.ToInt64(claim.Value);
            }

            var persona = await _personaRepositorio.BuscarPorIdUsuarioAsync(idUsuario);
            var nombre = default(string);
            if (persona != null)
            {
                nombre = persona.Nombres;
            }

            return View(new UsuarioInfo()
            {
                Nombre = nombre
            });
        }

        public class UsuarioInfo
        {
            public string Nombre { get; set; }
        }
    }
}
