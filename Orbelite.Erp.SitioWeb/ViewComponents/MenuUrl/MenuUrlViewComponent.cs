﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Orbelite.Principal.WebSeguridad.Servicios.MenuUrl.Dtos;
using Orbelite.Principal.WebSeguridad.Servicios.MenuUrl.Servicios.Abstracciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Orbelite.Erp.SitioWeb.ViewComponents.MenuUrl
{
    public class MenuUrlViewComponent: ViewComponent
    {
        private readonly IMenuUrlServicio _menuUrlServicio;
        public MenuUrlViewComponent(
            IMenuUrlServicio menuUrlServicio
            )
        {
            _menuUrlServicio = menuUrlServicio;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var claim = HttpContext.User.Claims.SingleOrDefault(m => m.Type == ClaimTypes.NameIdentifier);
            long idUsuario = 0;
            if (claim != null)
            {
                idUsuario = Convert.ToInt64(claim.Value);
            }
            var operacion = await _menuUrlServicio.ObtenerListaMenuUrl(idUsuario);
            var lista = new List<MenuUrlAdminDto>();
            if (operacion.Completado)
            {
                lista = operacion.Resultado;
            }
            return View(lista);
        }
    }
}
