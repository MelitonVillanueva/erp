﻿using Microsoft.AspNetCore.Mvc;
using Orbelite.Principal.Seguridad.Servicios.General.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Orbelite.Erp.SitioWeb.ViewComponents.GoogleMapScript
{
    public class GoogleMapJavascriptViewComponent: ViewComponent
    {
        
        private readonly UrlConfiguracionDto _urlConfiguracionDto;
        public GoogleMapJavascriptViewComponent(
                                                UrlConfiguracionDto urlConfiguracionDto)
        {
            _urlConfiguracionDto = urlConfiguracionDto;
        }
        public IViewComponentResult Invoke(int numberOfItems)
        {
            return View("Default", _urlConfiguracionDto.ApiKeyGoogle);
        }
    }
}
