using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Orbelite.Erp.Datos.Infraestructura.Configuraciones.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Configuraciones.Implementaciones;
using Orbelite.Erp.Datos.Infraestructura.Contextos.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Contextos.Implementaciones;
using Orbelite.Principal.Cargadores.Bd;
using Orbelite.Principal.Cargadores.Generales;
using Orbelite.Principal.Seguridad.Infraestructura.Modulos;
using Orbelite.Principal.WebComunes.ApiWeb.Auth.Base;
using Orbelite.Principal.WebComunes.Infraestructura.Errores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Orbelite.Erp.ServiciosWeb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();
            services.AddAutofac();
            services.AddAutoMapper(Assembly.Load("Orbelite.Erp.Servicios"));
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddAuthentication(ComunesAuthenticationOptions.DefaultScemeName).AddScheme<ComunesAuthenticationOptions, ComunesAuthenticationHandler>(
                ComunesAuthenticationOptions.DefaultScemeName, opts => { }
                );
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new SeguridadModule(Configuration));
            builder.RegisterModule(new
                CargarSqlServerEF<ErpContexto,
                IErpConfiguracion,
                ErpConfiguracion, IErpUnidadDeTrabajo>(Configuration, "erp", "Orbelite.Erp.Datos"));

            builder.RegisterModule(new ServicioCargador("Orbelite.Erp.Servicios"));
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseExceptionHandler(errorApp => errorApp.UseCustomErrors(env, false));

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
