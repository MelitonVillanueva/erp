﻿using Orbelite.Erp.Datos.Contabilidad.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Entidad.Entidades
{
    public class Tipo
    {

        public int IdTipo { get; set; }
        public string Nombre { get; set; }
        public string Grupo { get; set; }
        public int Orden { get; set; }


        public virtual List<Persona> Personas { get; set; }
        public virtual List<FilesXML> FilesXMLs { get; set; }

    }
}
