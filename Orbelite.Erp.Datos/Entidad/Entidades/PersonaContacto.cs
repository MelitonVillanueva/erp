﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Entidad.Entidades
{
    public class PersonaContacto
    {

        public long IdPersonaContacto { get; set; }
        public string NombreCompleto { get; set; }
        public string Apodo { get; set; }
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public long IdPersona { get; set; }

        public virtual Persona Persona { get; set; }

    }
}
