﻿using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Datos.Notificaciones.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Entidad.Entidades
{
    public class Persona
    {

        public long Id { get; set; }
        public int IdTipo { get; set; }
        public string Documento { get; set; }
        public string Nombres { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public string RazonSocial { get; set; }
        public string Direccion { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public DateTime Creado { get; set; }
        public DateTime Actualizado { get; set; }
        public bool? EstaBorrado { get; set; }
        public DateTime? Borrado { get; set; }


        public virtual Tipo Tipo { get; set; }
        public virtual List<PersonaContacto> PersonaContactos { get; set; }
        public virtual List<Factura> Facturas { get; set; }
        public virtual List<NotaCredito> NotasCredito { get; set; }
        public virtual List<NotaDebito> NotasDebito { get; set; }
        public virtual List<Contacto> Contactos { get; set; }

        public Persona()
        {
            Creado = DateTime.UtcNow;
            Actualizado = DateTime.UtcNow;
            EstaBorrado = false;
        }

    }
}
