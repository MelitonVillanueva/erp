﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Entidad.Entidades.Mapeo
{
    public class TipoMapeo : IEntityTypeConfiguration<Tipo>
    {
        public void Configure(EntityTypeBuilder<Tipo> builder)
        {
            builder.ToTable("Tipo", "Configuracion");
            builder.HasKey(e => e.IdTipo);
            builder.Property(e => e.IdTipo).HasColumnName("IdTipo").IsRequired().ValueGeneratedOnAdd();

            builder.Property(e => e.Nombre).HasColumnName("Nombre").IsUnicode(false).HasMaxLength(200);
            builder.Property(e => e.Grupo).HasColumnName("Grupo").IsUnicode(false).HasMaxLength(200);
            builder.Property(e => e.Orden).HasColumnName("Orden");
        }
    }
}
