﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Entidad.Entidades.Mapeo
{
    public class PersonaContactoMapeo : IEntityTypeConfiguration<PersonaContacto>
    {
        public void Configure(EntityTypeBuilder<PersonaContacto> builder)
        {
            builder.ToTable("PersonaContacto", "Crm");
            builder.HasKey(e => e.IdPersonaContacto);
            builder.Property(e => e.IdPersonaContacto).HasColumnName("IdPersonaContacto").IsRequired().ValueGeneratedOnAdd();

            builder.Property(e => e.NombreCompleto).HasColumnName("NombreCompleto").IsUnicode(false).HasMaxLength(400);
            builder.Property(e => e.Apodo).HasColumnName("Apodo").IsUnicode(false).HasMaxLength(400);
            builder.Property(e => e.Correo).HasColumnName("Correo").IsUnicode(false).HasMaxLength(200);
            builder.Property(e => e.Telefono).HasColumnName("Telefono").HasMaxLength(50);

            builder.HasOne(e => e.Persona).WithMany(b => b.PersonaContactos).HasForeignKey(c => c.IdPersona);
        }
    }
}
