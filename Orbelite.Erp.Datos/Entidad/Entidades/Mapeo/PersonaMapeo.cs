﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Entidad.Entidades.Mapeo
{
    public class PersonaMapeo : IEntityTypeConfiguration<Persona>
    {
        public void Configure(EntityTypeBuilder<Persona> builder)
        {
            builder.ToTable("Persona", "Crm");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasColumnName("IdPersona").IsRequired().ValueGeneratedOnAdd();

            builder.Property(e => e.Documento).HasColumnName("Documento").HasMaxLength(20);
            builder.Property(e => e.Nombres).HasColumnName("Nombres").IsUnicode(false).HasMaxLength(400);
            builder.Property(e => e.Paterno).HasColumnName("Paterno").IsUnicode(false).HasMaxLength(400);
            builder.Property(e => e.Materno).HasColumnName("Materno").IsUnicode(false).HasMaxLength(400);
            builder.Property(e => e.RazonSocial).HasColumnName("RazonSocial").IsUnicode(false).HasMaxLength(2000);
            builder.Property(e => e.Direccion).HasColumnName("Direccion").IsUnicode(false).HasMaxLength(2000);
            builder.Property(e => e.Departamento).HasColumnName("Departamento").IsUnicode(false).HasMaxLength(200);
            builder.Property(e => e.Provincia).HasColumnName("Provincia").IsUnicode(false).HasMaxLength(200);
            builder.Property(e => e.Distrito).HasColumnName("Distrito").IsUnicode(false).HasMaxLength(200);
            builder.Property(e => e.Correo).HasColumnName("Correo").IsUnicode(false).HasMaxLength(200);
            builder.Property(e => e.Telefono).HasColumnName("Telefono").HasMaxLength(50);
            builder.Property(e => e.Creado).HasColumnName("Creado");
            builder.Property(e => e.Actualizado).HasColumnName("Actualizado");
            builder.Property(e => e.EstaBorrado).HasColumnName("EstaBorrado");
            builder.Property(e => e.Borrado).HasColumnName("Borrado");
            builder.HasOne(e => e.Tipo).WithMany(b => b.Personas).HasForeignKey(c => c.IdTipo);
        }
    }
}
