﻿using Orbelite.Erp.Datos.Entidad.Entidades;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Abstracciones;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Entidad.Repositorios.Abstracciones
{
    public interface IPersonaRepositorio : IErpRepositorio<Persona, long>
    {

        public Task<Persona> BuscarPorDocumentoAsync(string doc);

        Task<Tuple<List<Persona>, int>> ListarPaginadosAsync(
            string orden, int start, int length, long? idUsuario,
            string documento, string nombres, string paterno, string materno, string razon);

        Task<List<Persona>> BuscarPorCoincidenciaAsync(string nombre, int limite);

    }
}
