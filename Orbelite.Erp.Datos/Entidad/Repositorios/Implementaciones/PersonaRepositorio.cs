﻿using Microsoft.EntityFrameworkCore;
using Orbelite.Erp.Datos.Entidad.Entidades;
using Orbelite.Erp.Datos.Entidad.Repositorios.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Configuraciones.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Contextos.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Implementaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Entidad.Repositorios.Implementaciones
{
    public class PersonaRepositorio : ErpRepositorio<Persona, long>, IPersonaRepositorio
    {

        public PersonaRepositorio(IErpUnidadDeTrabajo unidadDeTrabajo, IErpConfiguracion configuracion) : base(unidadDeTrabajo, configuracion)
        {
        }

        public override Task<Persona> BuscarPorIdAsync(long idPersona)
            => UnidadDeTrabajo.Personas.Where(e => e.Id == idPersona && e.EstaBorrado == false).FirstOrDefaultAsync();


        public Task<Persona> BuscarPorDocumentoAsync(string doc)
           => UnidadDeTrabajo.Personas.Where(e => e.Documento == doc).FirstOrDefaultAsync();

        public async Task<List<Persona>> BuscarPorCoincidenciaAsync(string nombre, int limite)
        => await UnidadDeTrabajo.Personas.Where(e => e.Documento.ToLower().StartsWith(nombre.ToLower())).Take(limite).ToListAsync();

        public async Task<Tuple<List<Persona>, int>> ListarPaginadosAsync(string orden, int start, int length, long? idUsuario, string documento, string nombres, string paterno, string materno, string razon)
        {
            var query = UnidadDeTrabajo.Personas.Include(e => e.Tipo)
                .Where(e =>
                 //(!idUsuario.HasValue || e.IdUsuario == idUsuario)
                 (string.IsNullOrWhiteSpace(documento) || e.Documento.StartsWith(documento))
                 && (string.IsNullOrWhiteSpace(nombres) || e.Nombres.StartsWith(nombres))
                 && (string.IsNullOrWhiteSpace(paterno) || e.Paterno.StartsWith(paterno))
                 && (string.IsNullOrWhiteSpace(materno) || e.Materno.StartsWith(materno))
                 && (string.IsNullOrWhiteSpace(razon) || e.RazonSocial.StartsWith(razon))
                 && e.EstaBorrado == false
                );

            var total = query.Count();
            switch (orden)
            {
                case "id":
                    query = query.OrderByDescending(e => e.Id);
                    break;
                case "id DESC":
                    query = query.OrderByDescending(e => e.Id);
                    break;
                case "documento":
                    query = query.OrderBy(e => e.Documento);
                    break;
                case "documento DESC":
                    query = query.OrderByDescending(e => e.Documento);
                    break;
                case "2":
                    query = query.OrderBy(e => e.Nombres);
                    break;
                case "2 DESC":
                    query = query.OrderByDescending(e => e.Nombres);
                    break;
                case "3":
                    query = query.OrderBy(e => e.Paterno);
                    break;
                case "3 DESC":
                    query = query.OrderByDescending(e => e.Paterno);
                    break;
                    //case "materno":
                    //    query = query.OrderBy(e => e.Materno);
                    //    break;
                    //case "materno DESC":
                    //    query = query.OrderByDescending(e => e.Materno);
                    //    break;
                    //case "razonsocial":
                    //    query = query.OrderBy(e => e.RazonSocial);
                    //    break;
                    //case "razonsocial DESC":
                    //    query = query.OrderByDescending(e => e.RazonSocial);
                    //    break;
            }

            query = query.Skip(start).Take(length);
            var resultados = await query.ToListAsync();
            return new Tuple<List<Persona>, int>(resultados, total);

        }
    }
}
