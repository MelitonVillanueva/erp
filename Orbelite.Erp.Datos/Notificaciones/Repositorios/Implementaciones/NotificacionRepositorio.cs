﻿using Microsoft.EntityFrameworkCore;
using Orbelite.Erp.Datos.Infraestructura.Configuraciones.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Contextos.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Implementaciones;
using Orbelite.Erp.Datos.Notificaciones.Entidades;
using Orbelite.Erp.Datos.Notificaciones.Repositorios.Abstracciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Notificaciones.Repositorios.Implementaciones
{
    public class NotificacionRepositorio : ErpRepositorio<Notificacion, int>, INotificacionRepositorio
    {

        public NotificacionRepositorio(IErpUnidadDeTrabajo unidadDeTrabajo, IErpConfiguracion configuracion) : base(unidadDeTrabajo, configuracion)
        {
        }


        public override Task<Notificacion> BuscarPorIdAsync(int idNotificacion)
            => UnidadDeTrabajo.Notificaciones.Where(e => e.IdNotificacion == idNotificacion && e.EstaBorrado == false).FirstOrDefaultAsync();

        public async Task<Tuple<List<Notificacion>, int>> ListarPaginadosAsync(string orden, int start, int length, long? idUsuario, string nombre)
        {
            var query = UnidadDeTrabajo.Notificaciones
                .Where(e =>
                 //(!idUsuario.HasValue || e.IdUsuario == idUsuario)
                 (string.IsNullOrWhiteSpace(nombre) || e.Nombre.StartsWith(nombre))
                 && e.EstaBorrado == false
                );

            var total = query.Count();
            switch (orden)
            {
                case "idNotificacion":
                    query = query.OrderByDescending(e => e.IdNotificacion);
                    break;
                case "idNotificacion DESC":
                    query = query.OrderByDescending(e => e.IdNotificacion);
                    break;
                case "nombre":
                    query = query.OrderBy(e => e.Nombre);
                    break;
                case "nombre DESC":
                    query = query.OrderByDescending(e => e.Nombre);
                    break;
            }

            query = query.Skip(start).Take(length);
            var resultados = await query.ToListAsync();
            return new Tuple<List<Notificacion>, int>(resultados, total);

        }

    }
}
