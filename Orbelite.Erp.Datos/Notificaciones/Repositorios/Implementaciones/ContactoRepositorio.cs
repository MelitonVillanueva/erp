﻿using Microsoft.EntityFrameworkCore;
using Orbelite.Erp.Datos.Infraestructura.Configuraciones.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Contextos.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Implementaciones;
using Orbelite.Erp.Datos.Notificaciones.Entidades;
using Orbelite.Erp.Datos.Notificaciones.Repositorios.Abstracciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Notificaciones.Repositorios.Implementaciones
{
    public class ContactoRepositorio : ErpRepositorio<Contacto, long>, IContactoRepositorio
    {

        public ContactoRepositorio(IErpUnidadDeTrabajo unidadDeTrabajo, IErpConfiguracion configuracion) : base(unidadDeTrabajo, configuracion)
        {
        }


        public override Task<Contacto> BuscarPorIdAsync(long idContacto)
            => UnidadDeTrabajo.Contactos.Where(e => e.IdContacto == idContacto && e.EstaBorrado == false).FirstOrDefaultAsync();

        public async Task<Tuple<List<Contacto>, int>> ListarPaginadosAsync(string orden, int start, int length, long? idUsuario, string documento)
        {
            var query = UnidadDeTrabajo.Contactos.Include(e => e.Persona)
                .Where(e =>
                 //(!idUsuario.HasValue || e.IdUsuario == idUsuario)
                 (string.IsNullOrWhiteSpace(documento) || e.Persona.Documento.StartsWith(documento))
                 && e.EstaBorrado == false
                );

            var total = query.Count();
            switch (orden)
            {
                case "idContacto":
                    query = query.OrderByDescending(e => e.IdContacto);
                    break;
                case "idContacto DESC":
                    query = query.OrderByDescending(e => e.IdContacto);
                    break;
                case "documento":
                    query = query.OrderBy(e => e.Persona.Documento);
                    break;
                case "documento DESC":
                    query = query.OrderByDescending(e => e.Persona.Documento);
                    break;
                case "2":
                    query = query.OrderBy(e => e.Persona.Nombres);
                    break;
                case "2 DESC":
                    query = query.OrderByDescending(e => e.Persona.Nombres);
                    break;
            }

            query = query.Skip(start).Take(length);
            var resultados = await query.ToListAsync();
            return new Tuple<List<Contacto>, int>(resultados, total);

        }

    }
}
