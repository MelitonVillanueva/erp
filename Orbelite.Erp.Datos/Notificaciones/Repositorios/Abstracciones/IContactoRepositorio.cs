﻿using Orbelite.Erp.Datos.Infraestructura.Repositorios.Abstracciones;
using Orbelite.Erp.Datos.Notificaciones.Entidades;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Notificaciones.Repositorios.Abstracciones
{
    public interface IContactoRepositorio : IErpRepositorio<Contacto, long>
    {

        Task<Tuple<List<Contacto>, int>> ListarPaginadosAsync(string orden, int start, int length, long? idUsuario, string documento);

    }
}
