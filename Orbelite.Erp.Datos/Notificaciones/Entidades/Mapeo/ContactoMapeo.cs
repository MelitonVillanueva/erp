﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Notificaciones.Entidades.Mapeo
{
    public class ContactoMapeo : IEntityTypeConfiguration<Contacto>
    {
        public void Configure(EntityTypeBuilder<Contacto> builder)
        {
            builder.ToTable("Contacto", "Notificaciones");
            builder.HasKey(e => e.IdContacto);
            builder.Property(e => e.IdContacto).HasColumnName("IdContacto").IsRequired().ValueGeneratedOnAdd();

            builder.Property(e => e.Vencimiento).HasColumnName("Vencimiento");
            builder.Property(e => e.Creado).HasColumnName("Creado");
            builder.Property(e => e.Actualizado).HasColumnName("Actualizado");
            builder.Property(e => e.EstaBorrado).HasColumnName("EstaBorrado");
            builder.Property(e => e.Borrado).HasColumnName("Borrado");
            builder.HasOne(e => e.Persona).WithMany(b => b.Contactos).HasForeignKey(c => c.IdContacto);
        }
    }
}
