﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Notificaciones.Entidades.Mapeo
{
    public class NotificacionMapeo : IEntityTypeConfiguration<Notificacion>
    {
        public void Configure(EntityTypeBuilder<Notificacion> builder)
        {
            builder.ToTable("Notificacion", "Configuracion");
            builder.HasKey(e => e.IdNotificacion);
            builder.Property(e => e.IdNotificacion).HasColumnName("IdNotificacion").IsRequired().ValueGeneratedOnAdd();

            builder.Property(e => e.Nombre).HasColumnName("Nombre").IsUnicode(false).HasMaxLength(100);
            builder.Property(e => e.Dias).HasColumnName("Dias");
            builder.Property(e => e.Creado).HasColumnName("Creado");
            builder.Property(e => e.Actualizado).HasColumnName("Actualizado");
            builder.Property(e => e.EstaBorrado).HasColumnName("EstaBorrado");
            builder.Property(e => e.Borrado).HasColumnName("Borrado");
            builder.Property(e => e.EsDeSistema).HasColumnName("EsDeSistema");
        }
    }
}
