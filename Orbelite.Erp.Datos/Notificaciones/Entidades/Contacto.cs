﻿using Orbelite.Erp.Datos.Entidad.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Notificaciones.Entidades
{
    public class Contacto
    {

        public long IdContacto { get; set; }
        public bool Vencimiento { get; set; }
        public DateTime Creado { get; set; }
        public DateTime Actualizado { get; set; }
        public bool? EstaBorrado { get; set; }
        public DateTime? Borrado { get; set; }


        public virtual Persona Persona { get; set; }

        public Contacto()
        {
            Creado = DateTime.UtcNow;
            Actualizado = DateTime.UtcNow;
            EstaBorrado = false;
        }

    }
}
