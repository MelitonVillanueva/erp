﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Notificaciones.Entidades
{
    public class Notificacion
    {

        public int IdNotificacion { get; set; }
        public string Nombre { get; set; }
        public int Dias { get; set; }
        public bool EsDeSistema { get; set; }

        public DateTime Creado { get; set; }
        public DateTime Actualizado { get; set; }
        public bool? EstaBorrado { get; set; }
        public DateTime? Borrado { get; set; }


        public Notificacion()
        {
            Creado = DateTime.UtcNow;
            Actualizado = DateTime.UtcNow;
            EstaBorrado = false;
        }


    }
}
