﻿using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Abstracciones;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Contabilidad.Repositorios.Abstracciones
{
    public interface IFacturaDetalleRepositorio : IErpRepositorio<FacturaDetalle, long>
    {

        Task<Tuple<List<FacturaDetalle>, int>> ListarPaginadosAsync(
            string orden, int start, int length, long? idUsuario,
            long idFactura);

        Task<List<FacturaDetalle>> ListarPorFactura(long idFactura);


        Task EliminarPorFactura(long idFactura);

    }
}
