﻿using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Abstracciones;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Contabilidad.Repositorios.Abstracciones
{
    public interface INotaCreditoDetalleRepositorio : IErpRepositorio<NotaCreditoDetalle, long>
    {

        Task<Tuple<List<NotaCreditoDetalle>, int>> ListarPaginadosAsync(
            string orden, int start, int length, long? idUsuario,
            long idNotaCredito);

        Task<List<NotaCreditoDetalle>> ListarPorNotaCredito(long idNotaCredito);

        Task EliminarPorNotaCredito(long idNotaCredito);

    }
}
