﻿using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Abstracciones;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Contabilidad.Repositorios.Abstracciones
{
    public interface INotaDebitoDetalleRepositorio : IErpRepositorio<NotaDebitoDetalle, long>
    {

        Task<Tuple<List<NotaDebitoDetalle>, int>> ListarPaginadosAsync(
            string orden, int start, int length, long? idUsuario,
            long idNotaDebito);

        Task<List<NotaDebitoDetalle>> ListarPorNotaDebito(long idNotaDebito);

        Task EliminarPorNotaDebito(long idNotaDebito);

    }
}
