﻿using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Abstracciones;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Contabilidad.Repositorios.Abstracciones
{
    public interface IFilesXMLRepositorio : IErpRepositorio<FilesXML, long>
    {

        public Task<FilesXML> BuscarPorSerieAsync(string serie);

    }
}
