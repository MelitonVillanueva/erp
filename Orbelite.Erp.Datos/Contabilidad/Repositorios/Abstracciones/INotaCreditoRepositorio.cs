﻿using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Abstracciones;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Contabilidad.Repositorios.Abstracciones
{
    public interface INotaCreditoRepositorio : IErpRepositorio<NotaCredito, long>
    {

        public Task<NotaCredito> BuscarPorSerieAsync(string serie);

        Task<Tuple<List<NotaCredito>, int>> ListarPaginadosAsync(
            string orden, int start, int length, long? idUsuario,
            string serie, string ruc, DateTime? emision);

        Task<List<NotaCredito>> BuscarPorCoincidenciaAsync(string nombre, int limite);

    }
}
