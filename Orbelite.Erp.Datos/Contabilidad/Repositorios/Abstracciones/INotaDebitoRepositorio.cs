﻿using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Abstracciones;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Contabilidad.Repositorios.Abstracciones
{
    public interface INotaDebitoRepositorio : IErpRepositorio<NotaDebito, long>
    {

        public Task<NotaDebito> BuscarPorSerieAsync(string serie);

        Task<Tuple<List<NotaDebito>, int>> ListarPaginadosAsync(
            string orden, int start, int length, long? idUsuario,
            string serie, string ruc, DateTime? emision);

        Task<List<NotaDebito>> BuscarPorCoincidenciaAsync(string nombre, int limite);

    }
}
