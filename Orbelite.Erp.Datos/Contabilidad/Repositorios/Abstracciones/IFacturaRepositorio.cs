﻿using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Abstracciones;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Contabilidad.Repositorios.Abstracciones
{
    public interface IFacturaRepositorio : IErpRepositorio<Factura, long>
    {

        public Task<Factura> BuscarPorSerieAsync(string serie);

        Task<Tuple<List<Factura>, int>> ListarPaginadosAsync(
            string orden, int start, int length, long? idUsuario,
            string serie, string ruc, DateTime? emision, DateTime? vencimiento);

        Task<Tuple<List<Factura>, int>> ListarUltimosCincoAsync(string orden);

        Task<List<Factura>> BuscarPorCoincidenciaAsync(string nombre, int limite);

    }
}
