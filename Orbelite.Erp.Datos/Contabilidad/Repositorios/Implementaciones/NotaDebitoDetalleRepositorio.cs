﻿using Microsoft.EntityFrameworkCore;
using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Datos.Contabilidad.Repositorios.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Configuraciones.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Contextos.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Implementaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Contabilidad.Repositorios.Implementaciones
{
    public class NotaDebitoDetalleRepositorio : ErpRepositorio<NotaDebitoDetalle, long>, INotaDebitoDetalleRepositorio
    {

        public NotaDebitoDetalleRepositorio(IErpUnidadDeTrabajo unidadDeTrabajo, IErpConfiguracion configuracion) : base(unidadDeTrabajo, configuracion)
        {
        }

        public async Task<Tuple<List<NotaDebitoDetalle>, int>> ListarPaginadosAsync(string orden, int start, int length, long? idUsuario, long idNotaDebito)
        {
            var query = UnidadDeTrabajo.NotaDebitoDetalles.Include(e => e.Producto)
                .Where(e =>
                 (e.IdNotaDebito == idNotaDebito)
                //&& e.EstaBorrado == false
                );

            var total = query.Count();
            switch (orden)
            {
                case "idNotaDebito":
                    query = query.OrderByDescending(e => e.IdNotaDebito);
                    break;
                case "idNotaDebito DESC":
                    query = query.OrderByDescending(e => e.IdNotaDebito);
                    break;
            }

            query = query.Skip(start).Take(length);
            var resultados = await query.ToListAsync();
            return new Tuple<List<NotaDebitoDetalle>, int>(resultados, total);

        }

        public async Task<List<NotaDebitoDetalle>> ListarPorNotaDebito(long idNotaDebito)
        => await UnidadDeTrabajo.NotaDebitoDetalles.Where(e => e.IdNotaDebito == idNotaDebito).Include(e => e.Producto).ToListAsync();


        public async Task EliminarPorNotaDebito(long idNotaDebito)
        {

            var entidades = UnidadDeTrabajo.NotaDebitoDetalles.Where(e => e.IdNotaDebito == idNotaDebito).Include(e => e.Producto).ToList();


            foreach (var item in entidades)
            {
                UnidadDeTrabajo.NotaDebitoDetalles.Attach(item);
                UnidadDeTrabajo.NotaDebitoDetalles.Remove(item);
                await UnidadDeTrabajo.GuardarCambiosAsync();
            }

        }

    }
}
