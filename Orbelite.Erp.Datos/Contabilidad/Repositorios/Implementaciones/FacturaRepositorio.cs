﻿using Microsoft.EntityFrameworkCore;
using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Datos.Contabilidad.Repositorios.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Configuraciones.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Contextos.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Implementaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Contabilidad.Repositorios.Implementaciones
{
    public class FacturaRepositorio : ErpRepositorio<Factura, long>, IFacturaRepositorio
    {

        public FacturaRepositorio(IErpUnidadDeTrabajo unidadDeTrabajo, IErpConfiguracion configuracion) : base(unidadDeTrabajo, configuracion)
        {
        }

        public Task<Factura> BuscarPorSerieAsync(string serie)
           => UnidadDeTrabajo.Facturas.Where(e => e.Numero == serie && e.EstaBorrado == false).FirstOrDefaultAsync();

        public override Task<Factura> BuscarPorIdAsync(long idFactura)
            => UnidadDeTrabajo.Facturas.Where(e => e.IdFactura == idFactura && e.EstaBorrado == false).FirstOrDefaultAsync();

        public async Task<List<Factura>> BuscarPorCoincidenciaAsync(string nombre, int limite)
        => await UnidadDeTrabajo.Facturas.Where(e => e.Numero.ToLower().StartsWith(nombre.ToLower())).Take(limite).ToListAsync();

        public async Task<Tuple<List<Factura>, int>> ListarPaginadosAsync(string orden, int start, int length, long? idUsuario, string serie, string ruc, DateTime? emision, DateTime? vencimiento)
        {
            var query = UnidadDeTrabajo.Facturas.Include(e => e.Persona)
                .Where(e =>
                 //(!idUsuario.HasValue || e.IdUsuario == idUsuario)
                 (string.IsNullOrWhiteSpace(serie) || e.Numero.StartsWith(serie))
                 && (string.IsNullOrWhiteSpace(ruc) || e.Persona.Documento.StartsWith(ruc))
                 && (string.IsNullOrWhiteSpace(emision.ToString()) || e.FechaEmision.Equals(emision))
                 && (string.IsNullOrWhiteSpace(vencimiento.ToString()) || e.FechaVencimiento.Equals(vencimiento))
                 //&& (string.IsNullOrWhiteSpace(razon) || e.RazonSocial.StartsWith(razon))
                 && e.EstaBorrado == false
                );

            var total = query.Count();
            switch (orden)
            {
                case "idFactura":
                    query = query.OrderByDescending(e => e.IdFactura);
                    break;
                case "idFactura DESC":
                    query = query.OrderByDescending(e => e.IdFactura);
                    break;
                case "numero":
                    query = query.OrderBy(e => e.Numero);
                    break;
                case "numero DESC":
                    query = query.OrderByDescending(e => e.Numero);
                    break;
            }

            query = query.Skip(start).Take(length);
            var resultados = await query.ToListAsync();
            return new Tuple<List<Factura>, int>(resultados, total);

        }


        public async Task<Tuple<List<Factura>, int>> ListarUltimosCincoAsync(string orden)
        {
            var query = UnidadDeTrabajo.Facturas.Include(e => e.Persona)
                .Where(e =>
                 e.EstaBorrado == false
                ).Take(5).OrderByDescending(e => e.Creado);

            var total = query.Count();
            switch (orden)
            {
                case "idFactura":
                    query = query.OrderByDescending(e => e.IdFactura);
                    break;
                case "idFactura DESC":
                    query = query.OrderByDescending(e => e.IdFactura);
                    break;
                case "numero":
                    query = query.OrderBy(e => e.Numero);
                    break;
                case "numero DESC":
                    query = query.OrderByDescending(e => e.Numero);
                    break;
            }

            //query = query.Take(5).OrderByDescending(e => e.Creado);
            var resultados = await query.ToListAsync();
            return new Tuple<List<Factura>, int>(resultados, total);

        }

    }
}
