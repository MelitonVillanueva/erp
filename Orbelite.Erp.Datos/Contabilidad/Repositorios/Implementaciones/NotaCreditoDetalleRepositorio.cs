﻿using Microsoft.EntityFrameworkCore;
using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Datos.Contabilidad.Repositorios.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Configuraciones.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Contextos.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Implementaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Contabilidad.Repositorios.Implementaciones
{
    public class NotaCreditoDetalleRepositorio : ErpRepositorio<NotaCreditoDetalle, long>, INotaCreditoDetalleRepositorio
    {

        public NotaCreditoDetalleRepositorio(IErpUnidadDeTrabajo unidadDeTrabajo, IErpConfiguracion configuracion) : base(unidadDeTrabajo, configuracion)
        {
        }

        public async Task<Tuple<List<NotaCreditoDetalle>, int>> ListarPaginadosAsync(string orden, int start, int length, long? idUsuario, long idNotaCredito)
        {
            var query = UnidadDeTrabajo.NotaCreditoDetalles.Include(e => e.Producto)
                .Where(e =>
                 (e.IdNotaCredito == idNotaCredito)
                //&& e.EstaBorrado == false
                );

            var total = query.Count();
            switch (orden)
            {
                case "idNotaCredito":
                    query = query.OrderByDescending(e => e.IdNotaCredito);
                    break;
                case "idNotaCredito DESC":
                    query = query.OrderByDescending(e => e.IdNotaCredito);
                    break;
            }

            query = query.Skip(start).Take(length);
            var resultados = await query.ToListAsync();
            return new Tuple<List<NotaCreditoDetalle>, int>(resultados, total);

        }

        public async Task<List<NotaCreditoDetalle>> ListarPorNotaCredito(long idNotaCredito)
        => await UnidadDeTrabajo.NotaCreditoDetalles.Where(e => e.IdNotaCredito == idNotaCredito).Include(e => e.Producto).ToListAsync();


        public async Task EliminarPorNotaCredito(long idNotaCredito)
        {

            var entidades = UnidadDeTrabajo.NotaCreditoDetalles.Where(e => e.IdNotaCredito == idNotaCredito).Include(e => e.Producto).ToList();


            foreach (var item in entidades)
            {
                UnidadDeTrabajo.NotaCreditoDetalles.Attach(item);
                UnidadDeTrabajo.NotaCreditoDetalles.Remove(item);
                await UnidadDeTrabajo.GuardarCambiosAsync();
            }

        }

    }
}
