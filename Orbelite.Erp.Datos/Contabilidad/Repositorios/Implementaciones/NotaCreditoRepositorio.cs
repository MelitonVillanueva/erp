﻿using Microsoft.EntityFrameworkCore;
using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Datos.Contabilidad.Repositorios.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Configuraciones.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Contextos.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Implementaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Contabilidad.Repositorios.Implementaciones
{
    public class NotaCreditoRepositorio : ErpRepositorio<NotaCredito, long>, INotaCreditoRepositorio
    {

        public NotaCreditoRepositorio(IErpUnidadDeTrabajo unidadDeTrabajo, IErpConfiguracion configuracion) : base(unidadDeTrabajo, configuracion)
        {
        }

        public Task<NotaCredito> BuscarPorSerieAsync(string serie)
           => UnidadDeTrabajo.NotaCreditos.Where(e => e.Numero == serie && e.EstaBorrado == false).FirstOrDefaultAsync();

        public override Task<NotaCredito> BuscarPorIdAsync(long idNotaCredito)
            => UnidadDeTrabajo.NotaCreditos.Where(e => e.IdNotaCredito == idNotaCredito && e.EstaBorrado == false).FirstOrDefaultAsync();

        public async Task<List<NotaCredito>> BuscarPorCoincidenciaAsync(string nombre, int limite)
        => await UnidadDeTrabajo.NotaCreditos.Where(e => e.Numero.ToLower().StartsWith(nombre.ToLower())).Take(limite).ToListAsync();

        public async Task<Tuple<List<NotaCredito>, int>> ListarPaginadosAsync(string orden, int start, int length, long? idUsuario, string serie, string ruc, DateTime? emision)
        {
            var query = UnidadDeTrabajo.NotaCreditos.Include(e => e.Persona)
                .Where(e =>
                 //(!idUsuario.HasValue || e.IdUsuario == idUsuario)
                 (string.IsNullOrWhiteSpace(serie) || e.Numero.StartsWith(serie))
                 && (string.IsNullOrWhiteSpace(ruc) || e.Persona.Documento.StartsWith(ruc))
                 && (string.IsNullOrWhiteSpace(emision.ToString()) || e.FechaEmision.Equals(emision))
                 //&& (string.IsNullOrWhiteSpace(vencimiento.ToString()) || e.FechaVencimiento.Equals(vencimiento))
                 //&& (string.IsNullOrWhiteSpace(razon) || e.RazonSocial.StartsWith(razon))
                 && e.EstaBorrado == false
                );

            var total = query.Count();
            switch (orden)
            {
                case "idNotaCredito":
                    query = query.OrderByDescending(e => e.IdNotaCredito);
                    break;
                case "idNotaCredito DESC":
                    query = query.OrderByDescending(e => e.IdNotaCredito);
                    break;
                case "numero":
                    query = query.OrderBy(e => e.Numero);
                    break;
                case "numero DESC":
                    query = query.OrderByDescending(e => e.Numero);
                    break;
                    //case "2":
                    //    query = query.OrderBy(e => e.Nombres);
                    //    break;
                    //case "2 DESC":
                    //    query = query.OrderByDescending(e => e.Nombres);
                    //    break;
                    //case "3":
                    //    query = query.OrderBy(e => e.Paterno);
                    //    break;
                    //case "3 DESC":
                    //    query = query.OrderByDescending(e => e.Paterno);
                    //    break;
                    //case "materno":
                    //    query = query.OrderBy(e => e.Materno);
                    //    break;
                    //case "materno DESC":
                    //    query = query.OrderByDescending(e => e.Materno);
                    //    break;
                    //case "razonsocial":
                    //    query = query.OrderBy(e => e.RazonSocial);
                    //    break;
                    //case "razonsocial DESC":
                    //    query = query.OrderByDescending(e => e.RazonSocial);
                    //    break;
            }

            query = query.Skip(start).Take(length);
            var resultados = await query.ToListAsync();
            return new Tuple<List<NotaCredito>, int>(resultados, total);
        }
    }
}
