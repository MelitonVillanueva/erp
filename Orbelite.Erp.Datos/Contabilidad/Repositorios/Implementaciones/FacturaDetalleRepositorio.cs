﻿using Microsoft.EntityFrameworkCore;
using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Datos.Contabilidad.Repositorios.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Configuraciones.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Contextos.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Implementaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Contabilidad.Repositorios.Implementaciones
{
    public class FacturaDetalleRepositorio : ErpRepositorio<FacturaDetalle, long>, IFacturaDetalleRepositorio
    {

        public FacturaDetalleRepositorio(IErpUnidadDeTrabajo unidadDeTrabajo, IErpConfiguracion configuracion) : base(unidadDeTrabajo, configuracion)
        {
        }

        public async Task<Tuple<List<FacturaDetalle>, int>> ListarPaginadosAsync(string orden, int start, int length, long? idUsuario, long idFactura)
        {
            var query = UnidadDeTrabajo.FacturaDetalles.Include(e => e.Producto)
                .Where(e =>
                 (e.IdFactura == idFactura)
                 //&& e.EstaBorrado == false
                );

            var total = query.Count();
            switch (orden)
            {
                case "idFactura":
                    query = query.OrderByDescending(e => e.IdFactura);
                    break;
                case "idFactura DESC":
                    query = query.OrderByDescending(e => e.IdFactura);
                    break;
            }

            query = query.Skip(start).Take(length);
            var resultados = await query.ToListAsync();
            return new Tuple<List<FacturaDetalle>, int>(resultados, total);

        }

        public async Task<List<FacturaDetalle>> ListarPorFactura(long idFactura)
        => await UnidadDeTrabajo.FacturaDetalles.Where(e => e.IdFactura == idFactura).Include(e => e.Producto).ToListAsync();

        public async Task EliminarPorFactura(long idFactura)
        {

            var entidades = UnidadDeTrabajo.FacturaDetalles.Where(e => e.IdFactura == idFactura).Include(e => e.Producto).ToList();


            foreach (var item in entidades)
            {
                UnidadDeTrabajo.FacturaDetalles.Attach(item);
                UnidadDeTrabajo.FacturaDetalles.Remove(item);
                await UnidadDeTrabajo.GuardarCambiosAsync();
            }

        }

    }
}
