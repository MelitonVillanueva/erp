﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Contabilidad.Entidades
{
    public class Moneda
    {

        public int IdMoneda { get; set; }
        public string Nombre { get; set; }

    }
}
