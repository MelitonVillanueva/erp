﻿using Orbelite.Erp.Datos.Inventario.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Contabilidad.Entidades
{
    public class NotaCreditoDetalle
    {

        public long IdNotaCreditoDetalle { get; set; }
        public int Cantidad { get; set; }
        public decimal Precio { get; set; }
        public int IdProducto { get; set; }
        public long IdNotaCredito { get; set; }

        public virtual Producto Producto { get; set; }
        public virtual NotaCredito NotaCredito { get; set; }

    }
}
