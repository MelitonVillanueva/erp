﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Contabilidad.Entidades.Mapeo
{
    public class NotaDebitoMapeo : IEntityTypeConfiguration<NotaDebito>
    {
        public void Configure(EntityTypeBuilder<NotaDebito> builder)
        {
            builder.ToTable("NotaDebito", "Contabilidad");
            builder.HasKey(e => e.IdNotaDebito);
            builder.Property(e => e.IdNotaDebito).HasColumnName("IdNotaDebito").IsRequired().ValueGeneratedOnAdd();

            builder.Property(e => e.Numero).HasColumnName("Numero").IsUnicode(false).HasMaxLength(100);
            builder.Property(e => e.FechaEmision).HasColumnName("FechaEmision");
            builder.Property(e => e.Motivo).HasColumnName("Motivo").IsUnicode(false).HasMaxLength(2000);
            builder.Property(e => e.ValorVentaOperacionesGratutitas).HasColumnName("ValorVentaOperacionesGratutitas");
            builder.Property(e => e.TributosCargosOperacionesGratutitas).HasColumnName("TributosCargosOperacionesGratuitas");
            builder.Property(e => e.ValorVenta).HasColumnName("ValorVenta");
            builder.Property(e => e.Isc).HasColumnName("Isc");
            builder.Property(e => e.Igv).HasColumnName("Igv");
            builder.Property(e => e.OtrosCargos).HasColumnName("OtroCargos");
            builder.Property(e => e.OtrosTributos).HasColumnName("OtrosTributos");
            builder.Property(e => e.ImporteTotal).HasColumnName("ImporteTotal");
            builder.Property(e => e.Creado).HasColumnName("Creado");
            builder.Property(e => e.Actualizado).HasColumnName("Actualizado");
            builder.Property(e => e.EstaBorrado).HasColumnName("EstaBorrado");
            builder.Property(e => e.Borrado).HasColumnName("Borrado");

            builder.HasOne(e => e.Persona).WithMany(b => b.NotasDebito).HasForeignKey(c => c.IdCliente);
            builder.HasOne(e => e.Factura).WithMany(b => b.NotasDebito).HasForeignKey(c => c.IdFactura);
        }
    }
}
