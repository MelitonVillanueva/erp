﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Contabilidad.Entidades.Mapeo
{
    public class MonedaMapeo : IEntityTypeConfiguration<Moneda>
    {
        public void Configure(EntityTypeBuilder<Moneda> builder)
        {
            builder.ToTable("Moneda", "Contabilidad");
            builder.HasKey(e => e.IdMoneda);
            builder.Property(e => e.IdMoneda).HasColumnName("IdMoneda").IsRequired().ValueGeneratedOnAdd();
        }
    }
}
