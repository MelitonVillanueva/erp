﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Contabilidad.Entidades.Mapeo
{
    public class NotaCreditoMapeo : IEntityTypeConfiguration<NotaCredito>
    {
        public void Configure(EntityTypeBuilder<NotaCredito> builder)
        {
            builder.ToTable("NotaCredito", "Contabilidad");
            builder.HasKey(e => e.IdNotaCredito);
            builder.Property(e => e.IdNotaCredito).HasColumnName("IdNotaCredito").IsRequired().ValueGeneratedOnAdd();

            builder.Property(e => e.Numero).HasColumnName("Numero").IsUnicode(false).HasMaxLength(100);
            builder.Property(e => e.FechaEmision).HasColumnName("FechaEmision");
            builder.Property(e => e.Motivo).HasColumnName("Motivo").IsUnicode(false).HasMaxLength(2000);
            builder.Property(e => e.SubTotalVentas).HasColumnName("SubTotalVentas");
            builder.Property(e => e.ImporteTotal).HasColumnName("ImporteTotal");
            builder.Property(e => e.Anticipos).HasColumnName("Anticipos");
            builder.Property(e => e.Descuentos).HasColumnName("Descuentos");
            builder.Property(e => e.ValorVenta).HasColumnName("ValorVenta");
            builder.Property(e => e.Isc).HasColumnName("Isc");
            builder.Property(e => e.Igv).HasColumnName("Igv");
            builder.Property(e => e.OtrosCargos).HasColumnName("OtroCargos");
            builder.Property(e => e.OtrosTributos).HasColumnName("OtrosTributos");
            builder.Property(e => e.MontoDeRedondeo).HasColumnName("MontoDeRedondeo");
            builder.Property(e => e.Creado).HasColumnName("Creado");
            builder.Property(e => e.Actualizado).HasColumnName("Actualizado");
            builder.Property(e => e.EstaBorrado).HasColumnName("EstaBorrado");
            builder.Property(e => e.Borrado).HasColumnName("Borrado");

            builder.HasOne(e => e.Persona).WithMany(b => b.NotasCredito).HasForeignKey(c => c.IdCliente);
            builder.HasOne(e => e.Factura).WithMany(b => b.NotasCredito).HasForeignKey(c => c.IdFactura);
        }
    }
}
