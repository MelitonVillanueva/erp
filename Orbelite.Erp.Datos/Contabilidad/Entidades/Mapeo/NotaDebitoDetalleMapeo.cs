﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Contabilidad.Entidades.Mapeo
{
    public class NotaDebitoDetalleMapeo : IEntityTypeConfiguration<NotaDebitoDetalle>
    {
        public void Configure(EntityTypeBuilder<NotaDebitoDetalle> builder)
        {
            builder.ToTable("NotaDebitoDetalle", "Contabilidad");
            builder.HasKey(e => e.IdNotaDebitoDetalle);
            builder.Property(e => e.IdNotaDebitoDetalle).HasColumnName("IdNotaDebitoDetalle").IsRequired().ValueGeneratedOnAdd();

            builder.Property(e => e.Cantidad).HasColumnName("Cantidad");
            builder.Property(e => e.Precio).HasColumnName("Precio");

            builder.HasOne(e => e.NotaDebito).WithMany(b => b.NotaDebitoDetalles).HasForeignKey(c => c.IdNotaDebito);
            builder.HasOne(e => e.Producto).WithMany(b => b.NotaDebitoDetalles).HasForeignKey(c => c.IdProducto);
        }
    }
}
