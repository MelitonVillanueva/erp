﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Contabilidad.Entidades.Mapeo
{
    public class NotaCreditoDetalleMapeo : IEntityTypeConfiguration<NotaCreditoDetalle>
    {
        public void Configure(EntityTypeBuilder<NotaCreditoDetalle> builder)
        {
            builder.ToTable("NotaCreditoDetalle", "Contabilidad");
            builder.HasKey(e => e.IdNotaCreditoDetalle);
            builder.Property(e => e.IdNotaCreditoDetalle).HasColumnName("IdNotaCreditoDetalle").IsRequired().ValueGeneratedOnAdd();

            builder.Property(e => e.Cantidad).HasColumnName("Cantidad");
            builder.Property(e => e.Precio).HasColumnName("Precio");

            builder.HasOne(e => e.NotaCredito).WithMany(b => b.NotaCreditoDetalles).HasForeignKey(c => c.IdNotaCredito);
            builder.HasOne(e => e.Producto).WithMany(b => b.NotaCreditoDetalles).HasForeignKey(c => c.IdProducto);
        }
    }
}
