﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Contabilidad.Entidades.Mapeo
{
    public class FilesXMLMapeo : IEntityTypeConfiguration<FilesXML>
    {
        public void Configure(EntityTypeBuilder<FilesXML> builder)
        {
            builder.ToTable("FilesXML", "Contabilidad");
            builder.HasKey(e => e.IdFile);
            builder.Property(e => e.IdFile).HasColumnName("IdFile").IsRequired().ValueGeneratedOnAdd();

            builder.Property(e => e.Serie).HasColumnName("Serie").IsUnicode(false).HasMaxLength(100);
            builder.Property(e => e.Nombre).HasColumnName("Nombre").IsUnicode(false).HasMaxLength(500);
            builder.Property(e => e.Descripcion).HasColumnName("Descripcion").IsUnicode(false).HasMaxLength(1000);

            builder.HasOne(e => e.Tipo).WithMany(b => b.FilesXMLs).HasForeignKey(c => c.IdTipo);
        }
    }
}
