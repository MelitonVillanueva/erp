﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Contabilidad.Entidades.Mapeo
{
    public class FacturaDetalleMapeo : IEntityTypeConfiguration<FacturaDetalle>
    {
        public void Configure(EntityTypeBuilder<FacturaDetalle> builder)
        {
            builder.ToTable("FacturaDetalle", "Contabilidad");
            builder.HasKey(e => e.IdFacturaDetalle);
            builder.Property(e => e.IdFacturaDetalle).HasColumnName("IdFacturaDetalle").IsRequired().ValueGeneratedOnAdd();

            builder.Property(e => e.Cantidad).HasColumnName("Cantidad");
            builder.Property(e => e.Precio).HasColumnName("Precio");

            builder.HasOne(e => e.Factura).WithMany(b => b.FacturaDetalles).HasForeignKey(c => c.IdFactura);
            builder.HasOne(e => e.Producto).WithMany(b => b.FacturaDetalles).HasForeignKey(c => c.IdProducto);
        }
    }
}
