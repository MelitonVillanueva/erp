﻿using Orbelite.Erp.Datos.Entidad.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Contabilidad.Entidades
{
    public class FilesXML
    {

        public long IdFile { get; set; }
        public string Serie { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int? IdTipo { get; set; }
        public DateTime Creado { get; set; }
        public DateTime Actualizado { get; set; }
        public bool? EstaBorrado { get; set; }
        public DateTime? Borrado { get; set; }

        public Tipo Tipo { get; set; }


        public FilesXML()
        {
            Creado = DateTime.UtcNow;
            Actualizado = DateTime.UtcNow;
            EstaBorrado = false;
        }

    }
}
