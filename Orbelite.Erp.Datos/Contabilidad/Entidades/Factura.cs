﻿using Orbelite.Erp.Datos.Entidad.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Contabilidad.Entidades
{
    public class Factura
    {

        public long IdFactura { get; set; }
        public string Numero { get; set; }
        public DateTime? FechaVencimiento { get; set; }
        public DateTime? FechaEmision { get; set; }
        public long? IdCliente { get; set; }
        public int IdMoneda { get; set; }
        public string Observacion { get; set; }
        public decimal ImporteTotal { get; set; }
        public decimal SubTotalVentas { get; set; }
        public decimal Anticipos { get; set; }
        public decimal Descuentos { get; set; }
        public decimal ValorVenta { get; set; }
        public decimal Isc { get; set; }
        public decimal Igv { get; set; }
        public decimal OtrosCargos { get; set; }
        public decimal OtrosTributos { get; set; }
        public decimal MontoDeRedondeo { get; set; }
        public decimal ValorVentaOperacionesGratutitas { get; set; }
        public DateTime Creado { get; set; }
        public DateTime Actualizado { get; set; }
        public bool? EstaBorrado { get; set; }
        public DateTime? Borrado { get; set; }


        public virtual Persona Persona { get; set; }
        public virtual List<FacturaDetalle> FacturaDetalles { get; set; }
        public virtual List<NotaCredito> NotasCredito { get; set; }
        public virtual List<NotaDebito> NotasDebito { get; set; }


        public Factura()
        {
            Creado = DateTime.UtcNow;
            Actualizado = DateTime.UtcNow;
            EstaBorrado = false;
        }

    }
}
