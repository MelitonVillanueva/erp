﻿using Orbelite.Erp.Datos.Entidad.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Contabilidad.Entidades
{
    public class NotaDebito
    {

        public long IdNotaDebito { get; set; }
        public string Numero { get; set; }
        public DateTime? FechaEmision { get; set; }
        public long? IdCliente { get; set; }
        public int IdMoneda { get; set; }
        public string Motivo { get; set; }
        public decimal ValorVentaOperacionesGratutitas { get; set; }
        public decimal TributosCargosOperacionesGratutitas { get; set; }
        public decimal ValorVenta { get; set; }
        public decimal Isc { get; set; }
        public decimal Igv { get; set; }
        public decimal OtrosCargos { get; set; }
        public decimal OtrosTributos { get; set; }
        public decimal ImporteTotal { get; set; }
        public DateTime Creado { get; set; }
        public DateTime Actualizado { get; set; }
        public bool? EstaBorrado { get; set; }
        public DateTime? Borrado { get; set; }
        public long IdFactura { get; set; }


        public virtual Persona Persona { get; set; }
        public virtual Factura Factura { get; set; }
        public virtual List<NotaDebitoDetalle> NotaDebitoDetalles { get; set; }


        public NotaDebito()
        {
            Creado = DateTime.UtcNow;
            Actualizado = DateTime.UtcNow;
            EstaBorrado = false;
        }

    }
}
