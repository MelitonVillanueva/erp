﻿using Microsoft.EntityFrameworkCore;
using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Datos.Entidad.Entidades;
using Orbelite.Erp.Datos.Inventario.Entidades;
using Orbelite.Erp.Datos.Notificaciones.Entidades;
using Orbelite.Principal.DatosEF.Infraestructura.Contexto.Abstracciones;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Infraestructura.Contextos.Abstracciones
{
    public interface IErpUnidadDeTrabajo: IEFUnidadDeTrabajo
    {
        public DbSet<Persona> Personas { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<PersonaContacto> PersonaContactos { get; set; }
        public DbSet<Tipo> Tipos { get; set; }
        public DbSet<Factura> Facturas { get; set; }
        public DbSet<FacturaDetalle> FacturaDetalles { get; set; }
        public DbSet<NotaCredito> NotaCreditos { get; set; }
        public DbSet<NotaCreditoDetalle> NotaCreditoDetalles { get; set; }
        public DbSet<NotaDebito> NotaDebitos { get; set; }
        public DbSet<NotaDebitoDetalle> NotaDebitoDetalles { get; set; }
        public DbSet<Moneda> Monedas { get; set; }
        public DbSet<FilesXML> FilesXMLs { get; set; }
        public DbSet<Contacto> Contactos { get; set; }
        public DbSet<Notificacion> Notificaciones { get; set; }
    }
}
