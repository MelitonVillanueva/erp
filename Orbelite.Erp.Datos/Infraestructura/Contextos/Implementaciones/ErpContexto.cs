﻿using Microsoft.EntityFrameworkCore;
using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Datos.Contabilidad.Entidades.Mapeo;
using Orbelite.Erp.Datos.Entidad.Entidades;
using Orbelite.Erp.Datos.Entidad.Entidades.Mapeo;
using Orbelite.Erp.Datos.Infraestructura.Contextos.Abstracciones;
using Orbelite.Erp.Datos.Inventario.Entidades;
using Orbelite.Erp.Datos.Inventario.Entidades.Mapeo;
using Orbelite.Erp.Datos.Notificaciones.Entidades;
using Orbelite.Erp.Datos.Notificaciones.Entidades.Mapeo;
using Orbelite.Principal.DatosEF.Infraestructura.Contexto.Implementaciones;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Infraestructura.Contextos.Implementaciones
{
    public class ErpContexto: EFDbContext, IErpUnidadDeTrabajo
    {
        public ErpContexto(DbContextOptions opciones) : base(opciones) { }

        public DbSet<Persona> Personas { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<PersonaContacto> PersonaContactos { get; set; }
        public DbSet<Tipo> Tipos { get; set; }
        public DbSet<Factura> Facturas { get; set; }
        public DbSet<FacturaDetalle> FacturaDetalles { get; set; }
        public DbSet<NotaCredito> NotaCreditos { get; set; }
        public DbSet<NotaCreditoDetalle> NotaCreditoDetalles { get; set; }
        public DbSet<NotaDebito> NotaDebitos { get; set; }
        public DbSet<NotaDebitoDetalle> NotaDebitoDetalles { get; set; }
        public DbSet<Moneda> Monedas { get; set; }
        public DbSet<FilesXML> FilesXMLs { get; set; }
        public DbSet<Contacto> Contactos { get; set; }
        public DbSet<Notificacion> Notificaciones { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new PersonaMapeo());
            modelBuilder.ApplyConfiguration(new ProductoMapeo());
            modelBuilder.ApplyConfiguration(new PersonaContactoMapeo());
            modelBuilder.ApplyConfiguration(new TipoMapeo());
            modelBuilder.ApplyConfiguration(new FacturaMapeo());
            modelBuilder.ApplyConfiguration(new FacturaDetalleMapeo());
            modelBuilder.ApplyConfiguration(new NotaCreditoMapeo());
            modelBuilder.ApplyConfiguration(new NotaCreditoDetalleMapeo());
            modelBuilder.ApplyConfiguration(new NotaDebitoMapeo());
            modelBuilder.ApplyConfiguration(new NotaDebitoDetalleMapeo());
            modelBuilder.ApplyConfiguration(new MonedaMapeo());
            modelBuilder.ApplyConfiguration(new FilesXMLMapeo());
            modelBuilder.ApplyConfiguration(new ContactoMapeo());
            modelBuilder.ApplyConfiguration(new NotificacionMapeo());
        }

    }
}
