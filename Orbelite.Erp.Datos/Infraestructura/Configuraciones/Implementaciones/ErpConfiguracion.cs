﻿using Orbelite.Erp.Datos.Infraestructura.Configuraciones.Abstracciones;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Infraestructura.Configuraciones.Implementaciones
{
    public class ErpConfiguracion: IErpConfiguracion
    {
        public string CadenaConexion { get; set; }
    }
}
