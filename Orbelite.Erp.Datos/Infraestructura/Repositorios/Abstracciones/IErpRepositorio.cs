﻿using Orbelite.Erp.Datos.Infraestructura.Contextos.Abstracciones;
using Orbelite.Principal.DatosEF.Infraestructura.Repositorios.Abstracciones;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Infraestructura.Repositorios.Abstracciones
{
    public interface IErpRepositorio<TEntidad, TLlave> : IEFRepositorio<TEntidad, TLlave, IErpUnidadDeTrabajo>
        where TEntidad : class
    {

    }
}
