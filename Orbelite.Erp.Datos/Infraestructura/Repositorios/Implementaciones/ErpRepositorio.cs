﻿using Orbelite.Erp.Datos.Infraestructura.Configuraciones.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Contextos.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Abstracciones;
using Orbelite.Principal.DatosEF.Infraestructura.Repositorios.Implementaciones;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Infraestructura.Repositorios.Implementaciones
{
    public class ErpRepositorio<TEntidad, TLlave> : EFRepositorio<TEntidad, TLlave, IErpConfiguracion, IErpUnidadDeTrabajo>, IErpRepositorio<TEntidad, TLlave>
         where TEntidad : class
    {
        public ErpRepositorio(IErpUnidadDeTrabajo unidadDeTrabajo, IErpConfiguracion configuracion) : base(unidadDeTrabajo, configuracion)
        {
        }

    }
}
