﻿using Microsoft.EntityFrameworkCore;
using Orbelite.Erp.Datos.Infraestructura.Configuraciones.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Contextos.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Implementaciones;
using Orbelite.Erp.Datos.Inventario.Entidades;
using Orbelite.Erp.Datos.Inventario.Repositorios.Abstracciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Inventario.Repositorios.Implementaciones
{
    class ProductoRepositorio : ErpRepositorio<Producto, int>, IProductoRepositorio
    {

        public ProductoRepositorio(IErpUnidadDeTrabajo unidadDeTrabajo, IErpConfiguracion configuracion) : base(unidadDeTrabajo, configuracion)
        {
        }

        public override Task<Producto> BuscarPorIdAsync(int idProducto)
            => UnidadDeTrabajo.Productos.Where(e => e.Id == idProducto && e.EstaBorrado == false).FirstOrDefaultAsync();


        public Task<Producto> BuscarPorNombreAsync(string nombre)
           => UnidadDeTrabajo.Productos.Where(e => e.Nombre == nombre).FirstOrDefaultAsync();

        public async Task<List<Producto>> BuscarPorCoincidenciaAsync(string nombre, int limite)
        => await UnidadDeTrabajo.Productos.Where(e => e.Nombre.ToLower().StartsWith(nombre.ToLower())).Take(limite).ToListAsync();

        public async Task<Tuple<List<Producto>, int>> ListarPaginadosAsync(string orden, int start, int length, long? idUsuario, string nombre, bool? estaHabilitado)
        {
            var query = UnidadDeTrabajo.Productos
                .Where(e =>
                 //(!idUsuario.HasValue || e.IdUsuario == idUsuario)
                 (string.IsNullOrWhiteSpace(nombre) || e.Nombre.StartsWith(nombre))
                 //&& (string.IsNullOrWhiteSpace(nombres) || e.Nombres.StartsWith(nombres))
                 //&& (string.IsNullOrWhiteSpace(paterno) || e.Paterno.StartsWith(paterno))
                 //&& (string.IsNullOrWhiteSpace(materno) || e.Materno.StartsWith(materno))
                 //&& (string.IsNullOrWhiteSpace(razon) || e.RazonSocial.StartsWith(razon))
                 && (!estaHabilitado.HasValue || e.EstaHabilitado == estaHabilitado)
                 && e.EstaBorrado == false
                );

            var total = query.Count();
            switch (orden)
            {
                case "id":
                    query = query.OrderByDescending(e => e.Id);
                    break;
                case "id DESC":
                    query = query.OrderByDescending(e => e.Id);
                    break;
                case "nombre":
                    query = query.OrderBy(e => e.Nombre);
                    break;
                case "nombre DESC":
                    query = query.OrderByDescending(e => e.Nombre);
                    break;
                //case "2":
                //    query = query.OrderBy(e => e.Nombres);
                //    break;
                //case "2 DESC":
                //    query = query.OrderByDescending(e => e.Nombres);
                //    break;
                //case "3":
                //    query = query.OrderBy(e => e.Paterno);
                //    break;
                //case "3 DESC":
                //    query = query.OrderByDescending(e => e.Paterno);
                //    break;
                    //case "materno":
                    //    query = query.OrderBy(e => e.Materno);
                    //    break;
                    //case "materno DESC":
                    //    query = query.OrderByDescending(e => e.Materno);
                    //    break;
                    //case "razonsocial":
                    //    query = query.OrderBy(e => e.RazonSocial);
                    //    break;
                    //case "razonsocial DESC":
                    //    query = query.OrderByDescending(e => e.RazonSocial);
                    //    break;
            }

            query = query.Skip(start).Take(length);
            var resultados = await query.ToListAsync();
            return new Tuple<List<Producto>, int>(resultados, total);

        }
    }
}
