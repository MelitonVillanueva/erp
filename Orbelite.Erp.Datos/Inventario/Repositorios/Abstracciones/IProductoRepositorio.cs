﻿using Orbelite.Erp.Datos.Infraestructura.Repositorios.Abstracciones;
using Orbelite.Erp.Datos.Inventario.Entidades;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Inventario.Repositorios.Abstracciones
{
    public interface IProductoRepositorio : IErpRepositorio<Producto, int>
    {

        Task<Producto> BuscarPorNombreAsync(string nombre);
        Task<List<Producto>> BuscarPorCoincidenciaAsync(string nombre, int limite);

        Task<Tuple<List<Producto>, int>> ListarPaginadosAsync(
            string orden, int start, int length, long? idUsuario,
            string nombre, bool? estaHabilitado);

    }
}
