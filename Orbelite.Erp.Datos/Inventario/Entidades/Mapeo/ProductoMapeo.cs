﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Inventario.Entidades.Mapeo
{
    public class ProductoMapeo : IEntityTypeConfiguration<Producto>
    {
        public void Configure(EntityTypeBuilder<Producto> builder)
        {
            builder.ToTable("Producto", "Inventario");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasColumnName("IdProducto").IsRequired().ValueGeneratedOnAdd();

            builder.Property(e => e.Nombre).HasColumnName("Nombre").IsUnicode(false).HasMaxLength(200);
            builder.Property(e => e.Descripcion).HasColumnName("Descripcion").IsUnicode(false).HasMaxLength(200);
            builder.Property(e => e.Cantidad).HasColumnName("Cantidad");
            builder.Property(e => e.EsIlimitado).HasColumnName("EsIlimitado");
            builder.Property(e => e.EstaHabilitado).HasColumnName("EstaHabilitado");
            builder.Property(e => e.Creado).HasColumnName("Creado");
            builder.Property(e => e.Actualizado).HasColumnName("Actualizado");
            builder.Property(e => e.EstaBorrado).HasColumnName("EstaBorrado");
            builder.Property(e => e.Borrado).HasColumnName("Borrado");
        }
    }
}
