﻿using Orbelite.Erp.Datos.Contabilidad.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Inventario.Entidades
{
    public class Producto
    {

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int? Cantidad { get; set; }
        public bool? EsIlimitado { get; set; }
        public bool? EstaHabilitado { get; set; }
        public DateTime Creado { get; set; }
        public DateTime Actualizado { get; set; }
        public bool? EstaBorrado { get; set; }
        public DateTime? Borrado { get; set; }

        public virtual List<FacturaDetalle> FacturaDetalles { get; set; }
        public virtual List<NotaCreditoDetalle> NotaCreditoDetalles { get; set; }
        public virtual List<NotaDebitoDetalle> NotaDebitoDetalles { get; set; }

        public Producto()
        {
            Creado = DateTime.UtcNow;
            Actualizado = DateTime.UtcNow;
            EstaBorrado = false;
        }

    }
}
