﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Datos.Configuracion.Entidades
{
    public class MenuUrl
    {
        public int IdMenuUrl { get; set; }
        public string Nombre { get; set; }
        public string Icono { get; set; }
        public int? IdMenuUrlPadre { get; set; }
        public bool EstaHabilitado { get; set; }
        public int? IdPermiso { get; set; }
        public string Url { get; set; }
        public bool EsParaAdmin { get; set; }
        public int Orden { get; set; }
        public bool MostrarSiempre { get; set; }
        public int? IdAplicativo { get; set; }
    }
}
