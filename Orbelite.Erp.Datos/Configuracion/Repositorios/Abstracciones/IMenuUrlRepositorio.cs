﻿using Orbelite.Erp.Datos.Configuracion.Entidades;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Abstracciones;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Configuracion.Repositorios.Abstracciones
{
    public interface IMenuUrlRepositorio: IErpRepositorio<MenuUrl, int>
    {
        Task<List<MenuUrl>> ListarPorGrupoAsync(int? idGrupo, bool? EsParaAdmin);
    }
}
