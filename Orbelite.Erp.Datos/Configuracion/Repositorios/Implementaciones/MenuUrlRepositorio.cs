﻿using Orbelite.Erp.Datos.Configuracion.Entidades;
using Orbelite.Erp.Datos.Configuracion.Repositorios.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Configuraciones.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Contextos.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Abstracciones;
using Orbelite.Erp.Datos.Infraestructura.Repositorios.Implementaciones;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Datos.Configuracion.Repositorios.Implementaciones
{
    public class MenuUrlRepositorio:ErpRepositorio<MenuUrl, int>, IMenuUrlRepositorio
    {
        public MenuUrlRepositorio(IErpUnidadDeTrabajo unidadDeTrabajo, IErpConfiguracion configuracion) : base(unidadDeTrabajo, configuracion) { }

        public async Task<List<MenuUrl>> ListarPorGrupoAsync(int? idGrupo, bool? EsParaAdmin)
        {
            List<MenuUrl> dato = new List<MenuUrl>();
            using (var conexion = new SqlConnection(Configuracion.CadenaConexion))
            {
                using var comando = new SqlCommand("Configuracion.ObtenerMenuUrl", conexion)
                {
                    CommandType = CommandType.StoredProcedure
                };
                comando.Parameters.Add("@IdGrupo", SqlDbType.BigInt).Value = idGrupo;
                comando.Parameters.Add("@EsParaAdmin", SqlDbType.Bit).Value = EsParaAdmin.HasValue ? EsParaAdmin: (object)DBNull.Value;
                conexion.Open();
                using var reader = await comando.ExecuteReaderAsync(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    dato.Add(new MenuUrl()
                    {
                        IdMenuUrl = !reader.IsDBNull(reader.GetOrdinal("IdMenuUrl")) ? reader.GetInt32(reader.GetOrdinal("IdMenuUrl")) : 0,
                        Nombre = !reader.IsDBNull(reader.GetOrdinal("Nombre")) ? reader.GetString(reader.GetOrdinal("Nombre")) : null,
                        Icono = !reader.IsDBNull(reader.GetOrdinal("Icono")) ? reader.GetString(reader.GetOrdinal("Icono")) : null,
                        IdMenuUrlPadre = !reader.IsDBNull(reader.GetOrdinal("IdMenuUrlPadre")) ? reader.GetInt32(reader.GetOrdinal("IdMenuUrlPadre")) : new int?(),
                        Url = !reader.IsDBNull(reader.GetOrdinal("Url")) ? reader.GetString(reader.GetOrdinal("Url")) : null,
                        Orden = !reader.IsDBNull(reader.GetOrdinal("Orden")) ? reader.GetInt32(reader.GetOrdinal("Orden")) : 0
                    });
                }
            }
            return dato;
        }

    }
}
