﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Inventario.Dtos
{
    public class ProductoDto
    {

        public bool IngresoPorSunat { get; set; }

        public string Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int Cantidad { get; set; }
        public bool? EsIlimitado { get; set; }
        public bool? EstaHabilitado { get; set; }
        public DateTime Creado { get; set; }
        public DateTime Actualizado { get; set; }
        public bool EstaBorrado { get; set; }
        public DateTime Borrado { get; set; }

    }
}
