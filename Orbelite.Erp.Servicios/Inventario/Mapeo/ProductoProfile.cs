﻿using AutoMapper;
using Orbelite.Erp.Datos.Inventario.Entidades;
using Orbelite.Erp.Servicios.Inventario.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Inventario.Mapeo
{
    public class ProductoProfile : Profile
    {
        public ProductoProfile()
        {
            CreateMap<Producto, ProductoDto>()
                 .AfterMap<ProductoProfileAction>();
        }
    }
}
