﻿using AutoMapper;
using Orbelite.Erp.Datos.Inventario.Entidades;
using Orbelite.Erp.Servicios.Inventario.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Utilitarios;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Inventario.Mapeo
{
    public class ProductoProfileAction : IMappingAction<Producto, ProductoDto>
    {

        public void Process(Producto source, ProductoDto destination, ResolutionContext context)
        {
            //if (!string.IsNullOrWhiteSpace(source.Dni))
            //{
            //    destination.Dni = source.Dni.PadLeft(8, '0');
            //}

            destination.Descripcion = source.Descripcion.Trim();
            destination.Nombre = source.Nombre.Trim();

            destination.Id = RijndaelUtilitario.EncryptRijndaelToUrl(source.Id.ToString());
            //if (source.Tipo != null)
            //{
            //    destination.Tipo = context.Mapper.Map<TipoDto>(source.Tipo);
            //}
        }

    }
}
