﻿using Orbelite.Erp.Servicios.Inventario.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Servicios.Inventario.Servicios.Abstracciones
{
    public interface IProductoServicio
    {

        Task<OperacionDto<RespuestaSimpleDto<string>>> RegistrarProductoAsync(ProductoDto peticion);
        Task<OperacionDto<RespuestaSimpleDto<string>>> EditarProductoAsync(ProductoDto peticion);

        Task<OperacionDto<ProductoDto>> DetalleAsync(string id);

        Task<OperacionDto<RespuestaSimpleDto<string>>> EliminarAsync(string id);

        Task<OperacionDto<JQueryDatatableDto<ProductoDto>>> ListarPaginadosAsync(
            string columna, int start, int length, int draw, long? idUsuario,
            string nombre, bool? estaHabilitado);

        Task<OperacionDto<List<ProductoDto>>> ListarPorCoincidenciaAsync(string coincidencia);

    }
}
