﻿using AutoMapper;
using Orbelite.Erp.Datos.Inventario.Entidades;
using Orbelite.Erp.Datos.Inventario.Repositorios.Abstracciones;
using Orbelite.Erp.Servicios.Inventario.Dtos;
using Orbelite.Erp.Servicios.Inventario.Servicios.Abstracciones;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Utilitarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Servicios.Inventario.Servicios.Implementaciones
{
    public class ProductoServicio : IProductoServicio
    {

        private readonly IProductoRepositorio _productoRepositorio;
        private readonly IMapper _mapper;
        public ProductoServicio(
            IProductoRepositorio productoRepositorio,
            IMapper mapper
            )
        {
            _productoRepositorio = productoRepositorio;
            _mapper = mapper;
        }

        public async Task<OperacionDto<JQueryDatatableDto<ProductoDto>>> ListarPaginadosAsync(
            string columna, int start, int length, int draw, long? idUsuario,
            string nombre, bool? estaHabilitado)
        {
            var resultados = await _productoRepositorio.ListarPaginadosAsync(
                columna, start, length, idUsuario,
                nombre, estaHabilitado
                );

            var dto = new JQueryDatatableDto<ProductoDto>()
            {
                Data = resultados.Item1.Select(e => _mapper.Map<ProductoDto>(e)).ToList(),
                RecordsTotal = resultados.Item2,
                Draw = draw,
                RecordsFiltered = resultados.Item2
            };
            return new OperacionDto<JQueryDatatableDto<ProductoDto>>(dto);
        }


        public async Task<OperacionDto<RespuestaSimpleDto<string>>> RegistrarProductoAsync(ProductoDto peticion)
        {
            var operacionValidacion = ValidacionUtilitario.ValidarModelo<RespuestaSimpleDto<string>>(peticion);
            if (!operacionValidacion.Completado)
            {
                return operacionValidacion;
            }

            var entidad = new Producto()
            {
                Nombre = peticion.Nombre,
                Descripcion = peticion.Descripcion,
                Cantidad = peticion.Cantidad,
                EstaHabilitado = peticion.EstaHabilitado,
                EsIlimitado = peticion.EsIlimitado
            };

            var existe = await _productoRepositorio.BuscarPorNombreAsync(peticion.Nombre);

            if (existe == null)
            {
                _productoRepositorio.Insertar(entidad);
                await _productoRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();
            }
            else
            {

                if (peticion.IngresoPorSunat)
                {
                    var idExistente = RijndaelUtilitario.EncryptRijndaelToUrl(existe.Id.ToString());
                    return new OperacionDto<RespuestaSimpleDto<string>>(new RespuestaSimpleDto<string>() { Id = idExistente });
                }

                return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, "El producto ya está registrado");
            }

            var idCifrado = RijndaelUtilitario.EncryptRijndaelToUrl(entidad.Id.ToString());


            return new OperacionDto<RespuestaSimpleDto<string>>(new RespuestaSimpleDto<string>() { Id = idCifrado });

        }

        public async Task<OperacionDto<RespuestaSimpleDto<string>>> EditarProductoAsync(ProductoDto peticion)
        {
            var operacionValidacion = ValidacionUtilitario.ValidarModelo<RespuestaSimpleDto<string>>(peticion);
            if (!operacionValidacion.Completado)
            {
                return operacionValidacion;
            }

            var id = RijndaelUtilitario.DecryptRijndaelFromUrl<int>(peticion.Id);

            var entidad = await _productoRepositorio.BuscarPorIdAsync(id);
            if (entidad == null)
            {
                return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, "Id no existe");
            }


            entidad.Nombre = peticion.Nombre;
            entidad.Descripcion = peticion.Descripcion;
            entidad.Cantidad = peticion.Cantidad;
            entidad.EstaHabilitado = peticion.EstaHabilitado;
            entidad.EsIlimitado = peticion.EsIlimitado;
            entidad.Actualizado = DateTime.UtcNow;

            _productoRepositorio.Editar(entidad);
            await _productoRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();

            return new OperacionDto<RespuestaSimpleDto<string>>(new RespuestaSimpleDto<string>() { Mensaje = "Se guardó correctamente" });

        }

        public async Task<OperacionDto<ProductoDto>> DetalleAsync(string id)
        {
            var idProducto = RijndaelUtilitario.DecryptRijndaelFromUrl<int>(id);
            var entidad = await _productoRepositorio.BuscarPorIdAsync(idProducto);
            if (entidad == null)
            {
                return new OperacionDto<ProductoDto>(CodigosOperacionDto.Invalido, "No se encontro el registro");
            }
            var dto = _mapper.Map<ProductoDto>(entidad);

            return new OperacionDto<ProductoDto>(dto);
        }

        public async Task<OperacionDto<RespuestaSimpleDto<string>>> EliminarAsync(string id)
        {
            var idProducto = RijndaelUtilitario.DecryptRijndaelFromUrl<int>(id);
            var entidad = await _productoRepositorio.BuscarPorIdAsync(idProducto);
            if (entidad == null)
            {
                return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, "Hubo un error al tratar de eliminar el registro.");
            }
            //var dto = _mapper.Map<VehiculoDto>(entidad);

            entidad.EstaBorrado = true;
            entidad.Borrado = DateTime.UtcNow;

            _productoRepositorio.Editar(entidad);

            await _productoRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();

            return new OperacionDto<RespuestaSimpleDto<string>>(new RespuestaSimpleDto<string>() { Mensaje = "Se eliminó correctamente" });
        }


        public async Task<OperacionDto<List<ProductoDto>>> ListarPorCoincidenciaAsync(string coincidencia)
        {
            //var idSitioWeb = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(idSitioWebCifrado);

            var lista = await _productoRepositorio.BuscarPorCoincidenciaAsync(coincidencia, 20);

            lista.Select(e => _mapper.Map<ProductoDto>(e)).ToList();

            //var listaDto = _mapper.Map<List<ProductoDto>>(lista);
            var listaDto = lista.Select(e => _mapper.Map<ProductoDto>(e)).ToList();

            //resultados.Item1.Select(e => _mapper.Map<ProductoDto>(e)).ToList(),
            return new OperacionDto<List<ProductoDto>>(listaDto);
        }

    }
}
