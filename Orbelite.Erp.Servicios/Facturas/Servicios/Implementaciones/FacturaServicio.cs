﻿using ICSharpCode.SharpZipLib.Zip;
using Microsoft.AspNetCore.Http;
using Orbelite.Erp.Servicios.Facturas.Dtos.Documentos.Factura;
using Orbelite.Erp.Servicios.Facturas.Servicios.Abstracciones;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using System.Xml.Linq;
using Orbelite.Erp.Servicios.Facturas.UtilXml;
using Orbelite.Erp.Servicios.Facturas.Dtos;
using Orbelite.Erp.Servicios.Facturas.Dtos.Documentos.NotaDebito;
using Orbelite.Erp.Servicios.Facturas.Dtos.Documentos.NotaCredito;
using Orbelite.Principal.Seguridad.Servicios.General.Dtos;
using Orbelite.Principal.Seguridad.Servicios.Peticiones.Servicios.Abstracciones;
using Newtonsoft.Json.Linq;
using Orbelite.Erp.Servicios.Entidad.Servicios.Abstracciones;
using Orbelite.Erp.Servicios.Entidad.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Utilitarios;
using Orbelite.Erp.Servicios.Inventario.Dtos;
using Orbelite.Erp.Servicios.Inventario.Servicios.Abstracciones;
using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Datos.Contabilidad.Repositorios.Abstracciones;
using Orbelite.Erp.Datos.Entidad.Repositorios.Abstracciones;
using Newtonsoft.Json;

namespace Orbelite.Erp.Servicios.Facturas.Servicios.Implementaciones
{
    public class FacturaServicio : IFacturaServicio
    {

        private readonly UrlConfiguracionDto _urlConfiguracionDto;
        private readonly SeguridadConfiguracionDto _seguridadConfiguracionDto;
        private readonly IPeticionServicio _peticionServicio;
        private readonly IPersonaRepositorio _personaRepositorio;
        private readonly IPersonaServicio _personaServicio;
        private readonly IProductoServicio _productoServicio;
        private readonly IFacturaRepositorio _facturaRepositorio;
        private readonly IFacturaDetalleRepositorio _facturaDetalleRepositorio;
        private readonly INotaCreditoRepositorio _notaCreditoRepositorio;
        private readonly INotaCreditoDetalleRepositorio _notaCreditoDetalleRepositorio;
        private readonly INotaDebitoRepositorio _notaDebitoRepositorio;
        private readonly INotaDebitoDetalleRepositorio _notaDebitoDetalleRepositorio;
        private readonly IFilesXMLRepositorio _filesXMLRepositorio;

        public FacturaServicio(
            UrlConfiguracionDto urlConfiguracionDto,
            SeguridadConfiguracionDto seguridadConfiguracionDto,
            IPeticionServicio peticionServicio,
            IPersonaServicio personaServicio,
            IPersonaRepositorio personaRepositorio,
            IProductoServicio productoServicio,
            IFacturaRepositorio facturaRepositorio,
            IFacturaDetalleRepositorio facturaDetalleRepositorio,
            INotaCreditoRepositorio notaCreditoRepositorio,
            INotaCreditoDetalleRepositorio notaCreditoDetalleRepositorio,
            INotaDebitoRepositorio notaDebitoRepositorio,
            INotaDebitoDetalleRepositorio notaDebitoDetalleRepositorio,
            IFilesXMLRepositorio filesXMLRepositorio)
        {
            _urlConfiguracionDto = urlConfiguracionDto;
            _seguridadConfiguracionDto = seguridadConfiguracionDto;
            _peticionServicio = peticionServicio;
            _personaRepositorio = personaRepositorio;
            _personaServicio = personaServicio;
            _productoServicio = productoServicio;
            _facturaRepositorio = facturaRepositorio;
            _facturaDetalleRepositorio = facturaDetalleRepositorio;
            _notaCreditoRepositorio = notaCreditoRepositorio;
            _notaCreditoDetalleRepositorio = notaCreditoDetalleRepositorio;
            _notaDebitoRepositorio = notaDebitoRepositorio;
            _notaDebitoDetalleRepositorio = notaDebitoDetalleRepositorio;
            _filesXMLRepositorio = filesXMLRepositorio;
        }


        public async Task<OperacionDto<CargarFileDto>> ValidarArchivoAsync(IFormFile File, string rutaComprimidos, string rutaDocumentos, long IdUsuario)
        {

            var extension = Path.GetExtension(File.FileName);
            if (extension != ".zip")
            {
                return new OperacionDto<CargarFileDto>(CodigosOperacionDto.Invalido, "El documento debe estar en formato .zip");
            }

            var nameArchivo = $"{Guid.NewGuid()}{extension}";

            var rutaArchivoCreado = $"{rutaComprimidos}{nameArchivo}";

            using (FileStream filestream = System.IO.File.Create($"{rutaArchivoCreado}"))
            {
                await File.CopyToAsync(filestream);
                filestream.Flush();
            }

            var validar = await UnzipFile(rutaArchivoCreado, rutaDocumentos, IdUsuario);
            if (!validar.Completado)
            {
                return new OperacionDto<CargarFileDto>(CodigosOperacionDto.Invalido, validar.Mensajes);
            }

            return new OperacionDto<CargarFileDto>(validar.Resultado);
        }

        public async Task<OperacionDto<CargarFileDto>> UnzipFile(string file, string urlUnZip, long idUsuario)
        {

            string sArchivoZip = file;
            ZipFile zip = new ZipFile(sArchivoZip);

            var errores = new List<string>();


            foreach (ZipEntry entry in zip)
            {
                using (StreamReader stream = new StreamReader(zip.GetInputStream(entry)))
                {
                    if (!entry.Name.ToLower().Contains(".xml"))
                    {
                        errores.Add($"El archivo {entry.Name} no es un xml");
                    }
                }
            }

            var cadenaErrores = String.Join("</br>", errores.ToArray());

            if (errores.Count > 0)
            {
                return new OperacionDto<CargarFileDto>(CodigosOperacionDto.Invalido, cadenaErrores);
            }

            var rutasAProcesar = new List<string>();

            foreach (ZipEntry entry in zip)
            {
                using (StreamReader stream = new StreamReader(zip.GetInputStream(entry)))
                {
                    if (!entry.Name.ToLower().Contains(".xml")) continue;
                    var sArchivoXml = $"{urlUnZip}{Guid.NewGuid()}.xml";
                    File.WriteAllText(sArchivoXml, stream.ReadToEnd());
                    stream.Close();
                    rutasAProcesar.Add(sArchivoXml);
                }
            }

            return new OperacionDto<CargarFileDto>(new CargarFileDto { ListaXml = rutasAProcesar });

        }

        public async Task<OperacionDto<RespuestaSimpleDto<string>>> ProcesarListaXmlAsync(string rutass, long idUsuario, bool overrideDoc)
        {

            List<string> rutas = string.IsNullOrEmpty(rutass) ? new List<string>() : JsonConvert.DeserializeObject<List<string>>(rutass);

            var errores = new List<string>();
            var salida = new List<string>();

            if (rutas.Count > 0)
            {
                foreach (var rutaXml in rutas)
                {
                    var resultadoOperacion = await ProcesarXmlDocumentoAsync(rutaXml, idUsuario, overrideDoc);

                    if (!resultadoOperacion.Completado)
                    {
                        errores.AddRange(resultadoOperacion.Mensajes);
                    }
                    else
                    {
                        salida.Add(resultadoOperacion.Resultado.Mensaje);
                    }

                }

                var cadenaSalida = String.Join("</br>", salida.ToArray());
                var cadenaErrores = String.Join("</br>", errores.ToArray());

                if (errores.Count > 0)
                {
                    return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, cadenaErrores);
                }

                return new OperacionDto<RespuestaSimpleDto<string>>(
                        new RespuestaSimpleDto<string>()
                        {
                            Mensaje = cadenaSalida
                        }
                    );
            }
            else
            {
                return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, "No se han cargado archivos válidos.");
            }
            
        }


        private async Task<OperacionDto<RespuestaSimpleDto<string>>> ProcesarXmlDocumentoAsync(string rutaDocumento, long idUsuario, bool overrideDoc)
        {

            var doc = new XmlDocument();

            doc.Load(rutaDocumento);

            var cssUrls = (from XmlNode childNode in doc.ChildNodes
                           where childNode.NodeType == XmlNodeType.ProcessingInstruction && childNode.Name == "xml-stylesheet"
                           select (XmlProcessingInstruction)childNode
                into procNode
                           select Regex.Match(procNode.Data, "href=\"(?<url>.*?)\"").Groups["url"].Value).ToList();

            var tipoDoc = cssUrls[0];


            var salidaXml = await DeserializeFromXML(rutaDocumento, tipoDoc, idUsuario, overrideDoc);

            return !salidaXml.Completado ?
                new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, salidaXml.Mensajes)
                : new OperacionDto<RespuestaSimpleDto<string>>(salidaXml.Resultado);
        }




        public async Task<OperacionDto<RespuestaSimpleDto<string>>> DeserializeFromXML(string xml, string tipoDoc, long? idUsuario, bool overrideDoc)
        {

            DocumentoDto result = new DocumentoDto();

            Serializer ser = new Serializer();

            string path = xml;
            string xmlInputData = File.ReadAllText(path);


            if (tipoDoc.Contains("factura"))
            {
                var objInvoice = new Invoice();
                result.Invoice = ser.Deserialize<Invoice>(xmlInputData);

                objInvoice = result.Invoice;
                var docIdentidadIn = objInvoice.AccountingCustomerParty.Party.PartyIdentification.ID.Value;

                var razonSocialIn = objInvoice.AccountingCustomerParty.Party.PartyLegalEntity.RegistrationName;

                var departamentoIn = objInvoice.AccountingCustomerParty.Party.PartyLegalEntity.RegistrationAddress.CityName;
                var provinciaIn = objInvoice.AccountingCustomerParty.Party.PartyLegalEntity.RegistrationAddress.CountrySubentity;
                var distritoIn = objInvoice.AccountingCustomerParty.Party.PartyLegalEntity.RegistrationAddress.District;
                var direccionIn = objInvoice.AccountingCustomerParty.Party.PartyLegalEntity.RegistrationAddress.AddressLine.Line;

                var ubigeo = $" {departamentoIn}-{provinciaIn}-{distritoIn}".Trim();

                direccionIn = direccionIn.Replace("<![CDATA[ ", "").Replace("]]>", "").Replace(ubigeo, "").Trim();
                razonSocialIn = razonSocialIn.Replace("<![CDATA[ ", "").Replace("]]>", "");

                var serieFacturaIn = objInvoice.ID.Value.Replace(" ", "");
                var fechaVencimientoIn = objInvoice.DueDate;
                var fechaEmisionIn = objInvoice.IssueDate;
                var subTotalVentasIn = objInvoice.TaxTotal.TaxSubtotal.TaxableAmount.Value;
                var anticiposIn = objInvoice.LegalMonetaryTotal.PrepaidAmount.Value;
                var descuentosIn = 0.0;
                var valorVentaIn = 0.0;
                var iscIn = 0.0;
                var igvIn = objInvoice.TaxTotal.TaxSubtotal.TaxAmount.Value;
                var otrosCargosIn = 0.0;
                var otrosTributosIn = 0.0;
                var montoRedondeoIn = 0.0;
                var valorVentaOGIn = 0.0;
                var importeTotalIn = objInvoice.LegalMonetaryTotal.PayableAmount.Value;


                if (docIdentidadIn.Count() > 8)
                {

                    var cliente = await _personaRepositorio.BuscarPorDocumentoAsync(docIdentidadIn);
                    long idCliente = 0;

                    if (cliente != null)
                    {
                        idCliente = cliente.Id;
                    }
                    else
                    {

                        var dtoPersona = new PersonaDto()
                        {
                            IngresoPorSunat = true,
                            IdTipo = 101,
                            Documento = docIdentidadIn,
                            Nombres = "",
                            RazonSocial = razonSocialIn,
                            Paterno = "",
                            Materno = "",
                            Correo = "",
                            Telefono = "",
                            Departamento = departamentoIn,
                            Provincia = provinciaIn,
                            Distrito = distritoIn,
                            Direccion = direccionIn
                        };

                        var regEmrpresa = await _personaServicio.RegistrarPersonaAsync(dtoPersona);

                        idCliente = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(regEmrpresa.Resultado.Id);
                    }


                    var factura = new Factura()
                    {
                        Numero = serieFacturaIn,
                        FechaVencimiento = fechaVencimientoIn,
                        FechaEmision = fechaEmisionIn,
                        IdCliente = idCliente,
                        IdMoneda = 1,
                        Observacion = "",
                        ImporteTotal = importeTotalIn,
                        SubTotalVentas = subTotalVentasIn,
                        Anticipos = anticiposIn,
                        Descuentos = (decimal)descuentosIn,
                        ValorVenta = (decimal)valorVentaIn,
                        Isc = (decimal)iscIn,
                        Igv = igvIn,
                        OtrosCargos = (decimal)otrosCargosIn,
                        OtrosTributos = (decimal)otrosTributosIn,
                        MontoDeRedondeo = (decimal)montoRedondeoIn,
                        ValorVentaOperacionesGratutitas = (decimal)valorVentaOGIn
                    };


                    var entidadIn = await _facturaRepositorio.BuscarPorSerieAsync(serieFacturaIn);

                    if (entidadIn == null)
                    {
                        _facturaRepositorio.Insertar(factura);
                        await _facturaRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();
                    }
                    else
                    {

                        entidadIn.Actualizado = DateTime.UtcNow;
                        entidadIn.Numero = serieFacturaIn;
                        entidadIn.FechaVencimiento = fechaVencimientoIn;
                        entidadIn.FechaEmision = fechaEmisionIn;
                        entidadIn.IdCliente = idCliente;
                        entidadIn.IdMoneda = 1;
                        entidadIn.Observacion = "";
                        entidadIn.ImporteTotal = importeTotalIn;
                        entidadIn.SubTotalVentas = subTotalVentasIn;
                        entidadIn.Anticipos = anticiposIn;
                        entidadIn.Descuentos = (decimal)descuentosIn;
                        entidadIn.ValorVenta = (decimal)valorVentaIn;
                        entidadIn.Isc = (decimal)iscIn;
                        entidadIn.Igv = igvIn;
                        entidadIn.OtrosCargos = (decimal)otrosCargosIn;
                        entidadIn.OtrosTributos = (decimal)otrosTributosIn;
                        entidadIn.MontoDeRedondeo = (decimal)montoRedondeoIn;
                        entidadIn.ValorVentaOperacionesGratutitas = (decimal)valorVentaOGIn;


                        if (overrideDoc)
                        {
                            _facturaRepositorio.Editar(entidadIn);
                            await _facturaRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();

                            await _facturaDetalleRepositorio.EliminarPorFactura(factura.IdFactura == 0 ? entidadIn.IdFactura : factura.IdFactura);
                            await _facturaDetalleRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();
                        }
                        else
                        {
                            return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, $"La factura {serieFacturaIn} ya se encuentra registrada");
                        }

                    }


                    //Detalle Producto
                    foreach (var item in objInvoice.InvoiceLine)
                    {

                        var dtoProducto = new ProductoDto()
                        {
                            IngresoPorSunat = true,
                            Nombre = item.Item.Description.Replace("<![CDATA[ ", "").Replace(" ]]>", "").Trim(),
                            Descripcion = item.Item.Description.Replace("<![CDATA[ ", "").Replace(" ]]>", "").Trim(),
                            Cantidad = (int)item.InvoicedQuantity.Value,
                            EstaHabilitado = true,
                            EsIlimitado = false
                        };

                        var regProducto = await _productoServicio.RegistrarProductoAsync(dtoProducto);

                        var idProducto = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(regProducto.Resultado.Id);

                        var facturaDetalle = new FacturaDetalle()
                        {
                            Cantidad = (int)item.InvoicedQuantity.Value,
                            Precio = item.Price.PriceAmount.Value,
                            IdFactura = factura.IdFactura == 0 ? entidadIn.IdFactura : factura.IdFactura,
                            IdProducto = (int)idProducto
                        };

                        _facturaDetalleRepositorio.Insertar(facturaDetalle);
                        await _facturaDetalleRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();

                    }

                    if (factura.IdFactura == 0)
                    {
                        return new OperacionDto<RespuestaSimpleDto<string>>(new RespuestaSimpleDto<string>() { Mensaje = $"La factura {serieFacturaIn} se ha actualizado satisfactoriamente" });
                    }
                    else
                    {
                        return new OperacionDto<RespuestaSimpleDto<string>>(new RespuestaSimpleDto<string>() { Mensaje = $"La factura {serieFacturaIn} se ha registrado satisfactoriamente" });
                    }

                }
                else
                {
                    return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.NoExiste, "El N° de documento no es válido.");
                }

            }
            else if (tipoDoc.Contains("notadebito"))
            {
                var objDebitNote = new DebitNote();
                result.DebitNote = ser.Deserialize<DebitNote>(xmlInputData);

                objDebitNote = result.DebitNote;
                var docIdentidadNd = objDebitNote.AccountingCustomerParty.Party.PartyIdentification.ID.Value;

                var razonSocialNd = objDebitNote.AccountingCustomerParty.Party.PartyLegalEntity.RegistrationName;

                var departamentoNd = objDebitNote.AccountingCustomerParty.Party.PartyLegalEntity.RegistrationAddress.CityName;
                var provinciaNd = objDebitNote.AccountingCustomerParty.Party.PartyLegalEntity.RegistrationAddress.CountrySubentity;
                var distritoNd = objDebitNote.AccountingCustomerParty.Party.PartyLegalEntity.RegistrationAddress.District;
                var direccionNd = objDebitNote.AccountingCustomerParty.Party.PartyLegalEntity.RegistrationAddress.AddressLine.Line;

                var ubigeo = $" {departamentoNd}-{provinciaNd}-{distritoNd}".Trim();

                direccionNd = direccionNd.Replace("<![CDATA[ ", "").Replace("]]>", "").Replace(ubigeo, "").Trim();
                razonSocialNd = razonSocialNd.Replace("<![CDATA[ ", "").Replace("]]>", "");


                var serieNotaDebitoNb = objDebitNote.ID.Value.Replace(" ", ""); ;
                var fechaEmisionNb = objDebitNote.IssueDate;
                var serieFacturaNb = objDebitNote.DiscrepancyResponse.ReferenceID.Replace(" ", ""); ;
                var motivoNb = objDebitNote.DiscrepancyResponse.Description.Replace("<![CDATA[ ", "").Replace(" ]]>", "");

                var valorVentaNb = objDebitNote.RequestedMonetaryTotal.LineExtensionAmount.Value;
                var iscNc = 0.0;
                var igvNc = objDebitNote.TaxTotal.TaxAmount.Value;
                var otrosCargosNb = 0.0;
                var otrosTributosNb = 0.0;
                var importeTotalNb = objDebitNote.RequestedMonetaryTotal.PayableAmount.Value;

                var vvOG = objDebitNote.RequestedMonetaryTotal.TaxExclusiveAmount.Value;
                var tcOG = objDebitNote.RequestedMonetaryTotal.ChargeTotalAmount.Value;


                if (docIdentidadNd.Count() > 8)
                {

                    var cliente = await _personaRepositorio.BuscarPorDocumentoAsync(docIdentidadNd);
                    long idCliente = 0;
                    long idFactura = 0;

                    if (cliente != null)
                    {
                        idCliente = cliente.Id;
                    }
                    else
                    {
                        var dtoPersona = new PersonaDto()
                        {
                            IngresoPorSunat = true,
                            IdTipo = 101,
                            Documento = docIdentidadNd,
                            Nombres = "",
                            RazonSocial = razonSocialNd,
                            Paterno = "",
                            Materno = "",
                            Correo = "",
                            Telefono = "",
                            Departamento = departamentoNd,
                            Provincia = provinciaNd,
                            Distrito = distritoNd,
                            Direccion = direccionNd
                        };

                        var regEmrpresa = await _personaServicio.RegistrarPersonaAsync(dtoPersona);

                        idCliente = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(regEmrpresa.Resultado.Id);
                    }

                    var factura = await _facturaRepositorio.BuscarPorSerieAsync(serieFacturaNb);

                    if (factura != null)
                    {
                        idFactura = factura.IdFactura;
                    }
                    else
                    {

                        return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, $"No existe la factura {serieFacturaNb} asociada a la nota de débito.");

                    }

                    var notaDebito = new NotaDebito()
                    {
                        Numero = serieNotaDebitoNb,
                        FechaEmision = fechaEmisionNb,
                        IdCliente = idCliente,
                        IdMoneda = 1,
                        Motivo = motivoNb,
                        ImporteTotal = importeTotalNb,
                        ValorVenta = (decimal)valorVentaNb,
                        Isc = (decimal)iscNc,
                        Igv = igvNc,
                        OtrosCargos = (decimal)otrosCargosNb,
                        OtrosTributos = (decimal)otrosTributosNb,
                        TributosCargosOperacionesGratutitas = (decimal)tcOG,
                        ValorVentaOperacionesGratutitas = (decimal)vvOG,
                        IdFactura = idFactura
                    };

                    var entidadNC = await _notaDebitoRepositorio.BuscarPorSerieAsync(serieNotaDebitoNb);

                    if (entidadNC == null)
                    {
                        _notaDebitoRepositorio.Insertar(notaDebito);
                        await _notaDebitoRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();
                    }
                    else
                    {

                        entidadNC.Numero = serieNotaDebitoNb;
                        entidadNC.FechaEmision = fechaEmisionNb;
                        entidadNC.IdCliente = idCliente;
                        entidadNC.IdMoneda = 1;
                        entidadNC.Motivo = motivoNb;
                        entidadNC.ImporteTotal = importeTotalNb;
                        entidadNC.ValorVenta = (decimal)valorVentaNb;
                        entidadNC.Isc = (decimal)iscNc;
                        entidadNC.Igv = igvNc;
                        entidadNC.OtrosCargos = (decimal)otrosCargosNb;
                        entidadNC.OtrosTributos = (decimal)otrosTributosNb;
                        entidadNC.TributosCargosOperacionesGratutitas = (decimal)tcOG;
                        entidadNC.ValorVentaOperacionesGratutitas = (decimal)vvOG;
                        entidadNC.IdFactura = idFactura;


                        if (overrideDoc)
                        {
                            _notaDebitoRepositorio.Editar(entidadNC);
                            await _notaDebitoRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();

                            await _notaDebitoDetalleRepositorio.EliminarPorNotaDebito(notaDebito.IdNotaDebito == 0 ? entidadNC.IdFactura : notaDebito.IdNotaDebito);
                            await _notaDebitoDetalleRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();
                        }
                        else
                        {
                            return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, $"La nota de débito {serieNotaDebitoNb} ya se encuentra registrada");
                        }

                    }


                    //Detalle Producto
                    foreach (var item in objDebitNote.DebitNoteLine)
                    {

                        var dtoProducto = new ProductoDto()
                        {
                            IngresoPorSunat = true,
                            Nombre = item.Item.Description.Replace("<![CDATA[ ", "").Replace(" ]]>", "").Trim(),
                            Descripcion = item.Item.Description.Replace("<![CDATA[ ", "").Replace(" ]]>", "").Trim(),
                            Cantidad = int.Parse(item.Item.SellersItemIdentification.ID.Value),
                            EstaHabilitado = true,
                            EsIlimitado = false
                        };

                        var regProducto = await _productoServicio.RegistrarProductoAsync(dtoProducto);

                        var idProducto = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(regProducto.Resultado.Id);

                        var notaDebitoDetalle = new NotaDebitoDetalle()
                        {
                            Cantidad = int.Parse(item.Item.SellersItemIdentification.ID.Value),
                            Precio = item.Price.PriceAmount.Value,
                            IdNotaDebito = notaDebito.IdNotaDebito,
                            IdProducto = (int)idProducto
                        };


                        _notaDebitoDetalleRepositorio.Insertar(notaDebitoDetalle);
                        await _notaDebitoDetalleRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();

                    }


                    if (notaDebito.IdNotaDebito == 0)
                    {
                        return new OperacionDto<RespuestaSimpleDto<string>>(new RespuestaSimpleDto<string>() { Mensaje = $"La nota de débito {serieNotaDebitoNb} se ha actualizado satisfactoriamente" });
                    }
                    else
                    {
                        return new OperacionDto<RespuestaSimpleDto<string>>(new RespuestaSimpleDto<string>() { Mensaje = $"La nota de débito {serieNotaDebitoNb} se ha registrado satisfactoriamente" });
                    }

                }
                else
                {
                    return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.NoExiste, "El N° de documento no es válido.");
                }

            }
            else
            {
                var objCreditNote = new CreditNote();
                result.CreditNote = ser.Deserialize<CreditNote>(xmlInputData);

                objCreditNote = result.CreditNote;
                var docIdentidadNc = objCreditNote.AccountingCustomerParty.Party.PartyIdentification.ID.Value;

                var razonSocialNc = objCreditNote.AccountingCustomerParty.Party.PartyLegalEntity.RegistrationName;

                var departamentoNc = objCreditNote.AccountingCustomerParty.Party.PartyLegalEntity.RegistrationAddress.CityName;
                var provinciaNc = objCreditNote.AccountingCustomerParty.Party.PartyLegalEntity.RegistrationAddress.CountrySubentity;
                var distritoNc = objCreditNote.AccountingCustomerParty.Party.PartyLegalEntity.RegistrationAddress.District;
                var direccionNc = objCreditNote.AccountingCustomerParty.Party.PartyLegalEntity.RegistrationAddress.AddressLine.Line;

                var ubigeo = $" {departamentoNc}-{provinciaNc}-{distritoNc}";

                direccionNc = direccionNc.Replace("<![CDATA[ ", "").Replace("]]>", "").Replace(ubigeo, "");
                razonSocialNc = razonSocialNc.Replace("<![CDATA[ ", "").Replace("]]>", "");


                var serieNotaCreditoNc = objCreditNote.ID.Value.Replace(" ", ""); ;
                var fechaEmisionNc = objCreditNote.IssueDate;
                var serieFacturaNc = objCreditNote.DiscrepancyResponse.ReferenceID.Replace(" ", ""); ;
                var motivoNc = objCreditNote.DiscrepancyResponse.Description.Replace("<![CDATA[", "").Replace("]]>", "");

                var subTotalVentasNc = objCreditNote.TaxTotal.TaxSubtotal.TaxableAmount.Value;
                var anticiposNc = objCreditNote.LegalMonetaryTotal.PrepaidAmount.Value;
                var descuentosNc = 0.0;
                var valorVentaNc = objCreditNote.TaxTotal.TaxSubtotal.TaxableAmount.Value;
                var iscNc = 0.0;
                var igvNc = objCreditNote.TaxTotal.TaxSubtotal.TaxAmount.Value;
                var otrosCargosNc = 0.0;
                var otrosTributosNc = 0.0;
                var montoRedondeoNc = 0.0;
                var importeTotalNc = objCreditNote.LegalMonetaryTotal.PayableAmount.Value;


                if (docIdentidadNc.Count() > 8)
                {

                    var cliente = await _personaRepositorio.BuscarPorDocumentoAsync(docIdentidadNc);
                    long idCliente = 0;
                    long idFactura = 0;

                    if (cliente != null)
                    {
                        idCliente = cliente.Id;
                    }
                    else
                    {
                        var dtoPersona = new PersonaDto()
                        {
                            IngresoPorSunat = true,
                            IdTipo = 101,
                            Documento = docIdentidadNc,
                            Nombres = "",
                            RazonSocial = razonSocialNc,
                            Paterno = "",
                            Materno = "",
                            Correo = "",
                            Telefono = "",
                            Departamento = departamentoNc,
                            Provincia = provinciaNc,
                            Distrito = distritoNc,
                            Direccion = direccionNc
                        };

                        var regEmrpresa = await _personaServicio.RegistrarPersonaAsync(dtoPersona);

                        idCliente = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(regEmrpresa.Resultado.Id);
                    }

                    var factura = await _facturaRepositorio.BuscarPorSerieAsync(serieFacturaNc);

                    if (factura != null)
                    {
                        idFactura = factura.IdFactura;
                    }
                    else
                    {

                        //return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, "No existe la factura asociada a la nota de crédito.");
                        return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, $"No existe la factura {serieFacturaNc} asociada a la nota de crédito.");

                    }

                    var noteCredito = new NotaCredito()
                    {
                        Numero = serieNotaCreditoNc,
                        FechaEmision = fechaEmisionNc,
                        IdCliente = idCliente,
                        IdMoneda = 1,
                        Motivo = motivoNc,
                        ImporteTotal = importeTotalNc,
                        SubTotalVentas = subTotalVentasNc,
                        Anticipos = anticiposNc,
                        Descuentos = (decimal)descuentosNc,
                        ValorVenta = (decimal)valorVentaNc,
                        Isc = (decimal)iscNc,
                        Igv = igvNc,
                        OtrosCargos = (decimal)otrosCargosNc,
                        OtrosTributos = (decimal)otrosTributosNc,
                        MontoDeRedondeo = (decimal)montoRedondeoNc,
                        IdFactura = idFactura
                    };

                    var entidadNC = await _notaCreditoRepositorio.BuscarPorSerieAsync(serieNotaCreditoNc);

                    if (entidadNC == null)
                    {
                        _notaCreditoRepositorio.Insertar(noteCredito);
                        await _notaCreditoRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();
                    }
                    else
                    {
                        entidadNC.Numero = serieNotaCreditoNc;
                        entidadNC.FechaEmision = fechaEmisionNc;
                        entidadNC.IdCliente = idCliente;
                        entidadNC.IdMoneda = 1;
                        entidadNC.Motivo = motivoNc;
                        entidadNC.ImporteTotal = importeTotalNc;
                        entidadNC.SubTotalVentas = subTotalVentasNc;
                        entidadNC.Anticipos = anticiposNc;
                        entidadNC.Descuentos = (decimal)descuentosNc;
                        entidadNC.ValorVenta = (decimal)valorVentaNc;
                        entidadNC.Isc = (decimal)iscNc;
                        entidadNC.Igv = igvNc;
                        entidadNC.OtrosCargos = (decimal)otrosCargosNc;
                        entidadNC.OtrosTributos = (decimal)otrosTributosNc;
                        entidadNC.MontoDeRedondeo = (decimal)montoRedondeoNc;
                        entidadNC.IdFactura = idFactura;

                        if (overrideDoc)
                        {
                            _notaCreditoRepositorio.Editar(entidadNC);
                            await _notaCreditoRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();

                            await _notaCreditoDetalleRepositorio.EliminarPorNotaCredito(noteCredito.IdNotaCredito == 0 ? entidadNC.IdNotaCredito : noteCredito.IdNotaCredito);
                            await _notaCreditoDetalleRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();
                        }
                        else
                        {
                            return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, $"La nota de crédito {serieNotaCreditoNc} ya se encuentra registrada");
                        }
                    }

                    //Detalle Producto
                    foreach (var item in objCreditNote.CreditNoteLine)
                    {

                        var dtoProducto = new ProductoDto()
                        {
                            IngresoPorSunat = true,
                            Nombre = item.Item.Description.Replace("<![CDATA[ ", "").Replace(" ]]>", "").Trim(),
                            Descripcion = item.Item.Description.Replace("<![CDATA[ ", "").Replace(" ]]>", "").Trim(),
                            Cantidad = (int)item.CreditedQuantity.Value,
                            EstaHabilitado = true,
                            EsIlimitado = false
                        };

                        var regProducto = await _productoServicio.RegistrarProductoAsync(dtoProducto);

                        var idProducto = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(regProducto.Resultado.Id);

                        var notaCreditoDetalle = new NotaCreditoDetalle()
                        {
                            Cantidad = (int)item.CreditedQuantity.Value,
                            Precio = item.Price.PriceAmount.Value,
                            IdNotaCredito = noteCredito.IdNotaCredito,
                            IdProducto = (int)idProducto
                        };


                        _notaCreditoDetalleRepositorio.Insertar(notaCreditoDetalle);
                        await _notaCreditoDetalleRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();

                    }

                    if (noteCredito.IdNotaCredito == 0)
                    {
                        return new OperacionDto<RespuestaSimpleDto<string>>(new RespuestaSimpleDto<string>() { Mensaje = $"La nota de crédito {serieNotaCreditoNc} se ha actualizado satisfactoriamente" });
                    }
                    else
                    {
                        return new OperacionDto<RespuestaSimpleDto<string>>(new RespuestaSimpleDto<string>() { Mensaje = $"La nota de crédito {serieNotaCreditoNc} se ha registrado satisfactoriamente" });
                    }

                }
                else
                {
                    return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.NoExiste, "El N° de documento no es válido.");
                }

            }

        }

    }
}
