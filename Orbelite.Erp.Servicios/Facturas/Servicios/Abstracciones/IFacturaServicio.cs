﻿using Microsoft.AspNetCore.Http;
using Orbelite.Erp.Servicios.Facturas.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Servicios.Facturas.Servicios.Abstracciones
{
    public interface IFacturaServicio
    {
        Task<OperacionDto<CargarFileDto>> ValidarArchivoAsync(IFormFile File, string rutaComprimidos, string rutaDocumentos, long IdUsuario);

        Task<OperacionDto<RespuestaSimpleDto<string>>> ProcesarListaXmlAsync(string listado, long idUsuario, bool overrideDoc);

    }
}
