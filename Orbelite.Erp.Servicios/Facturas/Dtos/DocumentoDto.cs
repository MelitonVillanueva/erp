﻿using Orbelite.Erp.Servicios.Facturas.Dtos.Documentos.Factura;
using Orbelite.Erp.Servicios.Facturas.Dtos.Documentos.NotaCredito;
using Orbelite.Erp.Servicios.Facturas.Dtos.Documentos.NotaDebito;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Facturas.Dtos
{
    public class DocumentoDto
    {

        public Invoice Invoice { get; set; }
        public CreditNote CreditNote { get; set; }
        public DebitNote DebitNote { get; set; }

    }
}
