﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Facturas.Dtos
{
    public class CargarFileDto
    {
        public bool Habilitado { get; set; }
        public List<string> ListaXml { get; set; }
        public string ListaCadena { get; set; }
    }
}
