﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.General.Dtos
{
    public class ConfiguracionDto
    {
        public GeneralDto General { get; set; }
        public EmailDto Email { get; set; }
        public RecaptchaDto Recaptcha { get; set; }
        public VisaDto Visa { get; set; }
    }

    public class GeneralDto {
        public string WhatsApp { get; set; }
        public string ApiKeyGoogle { get; set; }
        public string UrlSitioPersonalizadoBase { get; set; }
        public string UrlBase { get; set; }
        public string UrlApiConsultas { get; set; }
        public string RutaArhivos { get; set; }
        public string CorreoLandingIbroker { get; set; }
        public string UrlApiEscaner { get; set; }
        public string UrlEscaner { get; set; }
        public string UrlInfraxion { get; set; }
        public string UrlInfraxionPro { get; set; }
        public string UrlBasePersonalizado { get; set; }
        public string UrlBaseUrbelite { get; set; }
        public string UrlAutoencePersonalizado { get; set; }
    }

    public class RecaptchaDto
    {
        public string ClaveSitioWeb { get; set; }
        public string ClaveSecrera { get; set; }
    }


    public class EmailDto
    {
        public string Email_Host { get; set; }
        public string Email_Port { get; set; }
        public string Email_Psw { get; set; }
        public string Email_Usuario { get; set; }
        public string Email_From { get; set; }
    }

    public class VisaDto
    {
        public string Usuario { get; set; }
        public string Password { get; set; }
        public string IdMercader { get; set; }
        public string TimeOut { get; set; }
        public string Javascript { get; set; }
        public string UrlToken { get; set; }
        public string UrlSesion { get; set; }
        public string UrlAutorizacion { get; set; }
    }
}
