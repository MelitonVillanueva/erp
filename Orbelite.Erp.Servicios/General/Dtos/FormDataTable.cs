﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.General.Dtos
{
    public class FormDataTable
    {
        public int Draw { get; set; }

        public int Length { get; set; }

        public int Start { get; set; }

        public List<ColumnsTable> Columns { get; set; }

        public List<OrderTable> Order { get; set; }
    }
    public class ColumnsTable
    {
        public string data { get; set; }

        public string name { get; set; }
    }

    public class OrderTable
    {
        public int column { get; set; }

        public string dir { get; set; }
    }
}
