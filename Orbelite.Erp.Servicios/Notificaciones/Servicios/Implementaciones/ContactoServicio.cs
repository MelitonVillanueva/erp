﻿using AutoMapper;
using Orbelite.Erp.Datos.Notificaciones.Entidades;
using Orbelite.Erp.Datos.Notificaciones.Repositorios.Abstracciones;
using Orbelite.Erp.Servicios.Notificaciones.Dtos;
using Orbelite.Erp.Servicios.Notificaciones.Servicios.Abstracciones;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Utilitarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Servicios.Notificaciones.Servicios.Implementaciones
{
    public class ContactoServicio : IContactoServicio
    {

        private readonly IContactoRepositorio _contactoRepositorio;
        private readonly IMapper _mapper;

        public ContactoServicio(
            IContactoRepositorio contactoRepositorio,
            IMapper mapper)
        {
            _contactoRepositorio = contactoRepositorio;
            _mapper = mapper;
        }


        public async Task<OperacionDto<JQueryDatatableDto<ContactoDto>>> ListarPaginadosAsync(string columna, int start, int length, int draw, long? idUsuario, string documento)
        {
            var resultados = await _contactoRepositorio.ListarPaginadosAsync(
                columna, start, length, idUsuario, documento
                );

            var dto = new JQueryDatatableDto<ContactoDto>()
            {
                Data = resultados.Item1.Select(e => _mapper.Map<ContactoDto>(e)).ToList(),
                RecordsTotal = resultados.Item2,
                Draw = draw,
                RecordsFiltered = resultados.Item2
            };
            return new OperacionDto<JQueryDatatableDto<ContactoDto>>(dto);
        }


        public async Task<OperacionDto<RespuestaSimpleDto<string>>> RegistrarContactoAsync(ContactoDto peticion)
        {
            var operacionValidacion = ValidacionUtilitario.ValidarModelo<RespuestaSimpleDto<string>>(peticion);
            if (!operacionValidacion.Completado)
            {
                return operacionValidacion;
            }

            var idContacto = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(peticion.IdContacto);

            var entidad = new Contacto()
            {
                IdContacto = idContacto,
                Vencimiento = peticion.Vencimiento
            };

            var existe = await _contactoRepositorio.BuscarPorIdAsync(idContacto);

            if (existe == null)
            {
                _contactoRepositorio.Insertar(entidad);
                await _contactoRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();
            }
            else
            {
                return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, "El usuario ya existe");
            }

            var idCifrado = RijndaelUtilitario.EncryptRijndaelToUrl(entidad.IdContacto.ToString());


            return new OperacionDto<RespuestaSimpleDto<string>>(new RespuestaSimpleDto<string>() { Id = idCifrado });

        }


        public async Task<OperacionDto<RespuestaSimpleDto<string>>> EditarContactoAsync(ContactoDto peticion)
        {
            var operacionValidacion = ValidacionUtilitario.ValidarModelo<RespuestaSimpleDto<string>>(peticion);
            if (!operacionValidacion.Completado)
            {
                return operacionValidacion;
            }

            var id = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(peticion.IdContacto);

            var entidad = await _contactoRepositorio.BuscarPorIdAsync(id);
            if (entidad == null)
            {
                return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, "Contacto no existe");
            }

            entidad.Vencimiento = peticion.Vencimiento;
            entidad.Actualizado = DateTime.UtcNow;

            _contactoRepositorio.Editar(entidad);
            await _contactoRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();

            return new OperacionDto<RespuestaSimpleDto<string>>(new RespuestaSimpleDto<string>() { Mensaje = "Se guardó correctamente" });

        }

    }
}
