﻿using AutoMapper;
using Orbelite.Erp.Datos.Notificaciones.Entidades;
using Orbelite.Erp.Datos.Notificaciones.Repositorios.Abstracciones;
using Orbelite.Erp.Servicios.Notificaciones.Dtos;
using Orbelite.Erp.Servicios.Notificaciones.Servicios.Abstracciones;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Utilitarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Servicios.Notificaciones.Servicios.Implementaciones
{
    public class NotificacionServicio : INotificacionServicio
    {

        private readonly INotificacionRepositorio _notificacionRepositorio;
        private readonly IMapper _mapper;

        public NotificacionServicio(
            INotificacionRepositorio notificacionRepositorio,
            IMapper mapper)
        {
            _notificacionRepositorio = notificacionRepositorio;
            _mapper = mapper;
        }


        public async Task<OperacionDto<JQueryDatatableDto<NotificacionDto>>> ListarPaginadosAsync(string columna, int start, int length, int draw, long? idUsuario, string nombre)
        {
            var resultados = await _notificacionRepositorio.ListarPaginadosAsync(
                columna, start, length, idUsuario, nombre
                );

            var dto = new JQueryDatatableDto<NotificacionDto>()
            {
                Data = resultados.Item1.Select(e => _mapper.Map<NotificacionDto>(e)).ToList(),
                RecordsTotal = resultados.Item2,
                Draw = draw,
                RecordsFiltered = resultados.Item2
            };
            return new OperacionDto<JQueryDatatableDto<NotificacionDto>>(dto);
        }


        public async Task<OperacionDto<RespuestaSimpleDto<string>>> RegistrarNotificacionAsync(NotificacionDto peticion)
        {
            var operacionValidacion = ValidacionUtilitario.ValidarModelo<RespuestaSimpleDto<string>>(peticion);
            if (!operacionValidacion.Completado)
            {
                return operacionValidacion;
            }

            var idNotificacion = RijndaelUtilitario.DecryptRijndaelFromUrl<int>(peticion.Id);

            if (peticion.TipoAlerta == 30)
            {
                peticion.Dias *= -1;
            }

            var entidad = new Notificacion()
            {
                Nombre = peticion.Nombre,
                Dias = peticion.Dias,
                EsDeSistema = peticion.EsDeSistema
            };

            var existe = await _notificacionRepositorio.BuscarPorIdAsync(idNotificacion);

            if (existe == null)
            {
                _notificacionRepositorio.Insertar(entidad);
                await _notificacionRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();
            }
            else
            {
                return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, "Error al generar la notificación, ya existe.");
            }

            var idCifrado = RijndaelUtilitario.EncryptRijndaelToUrl(entidad.IdNotificacion.ToString());


            return new OperacionDto<RespuestaSimpleDto<string>>(new RespuestaSimpleDto<string>() { Id = idCifrado });

        }


        public async Task<OperacionDto<RespuestaSimpleDto<string>>> EditarNotificacionAsync(NotificacionDto peticion)
        {
            var operacionValidacion = ValidacionUtilitario.ValidarModelo<RespuestaSimpleDto<string>>(peticion);
            if (!operacionValidacion.Completado)
            {
                return operacionValidacion;
            }

            var id = RijndaelUtilitario.DecryptRijndaelFromUrl<int>(peticion.Id);

            if (peticion.TipoAlerta == 30)
            {
                peticion.Dias *= -1;
            }

            var entidad = await _notificacionRepositorio.BuscarPorIdAsync(id);
            if (entidad == null)
            {
                return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, "La notificación no existe");
            }

            entidad.Nombre = peticion.Nombre;
            entidad.Dias = peticion.Dias;
            entidad.EsDeSistema = peticion.EsDeSistema;
            entidad.Actualizado = DateTime.UtcNow;

            _notificacionRepositorio.Editar(entidad);
            await _notificacionRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();

            return new OperacionDto<RespuestaSimpleDto<string>>(new RespuestaSimpleDto<string>() { Mensaje = "Se guardó correctamente" });

        }


        public async Task<OperacionDto<RespuestaSimpleDto<string>>> EliminarAsync(string id)
        {
            var idNotificacion = RijndaelUtilitario.DecryptRijndaelFromUrl<int>(id);
            var entidad = await _notificacionRepositorio.BuscarPorIdAsync(idNotificacion);
            if (entidad == null)
            {
                return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, "Hubo un error al tratar de eliminar el registro.");
            }
            //var dto = _mapper.Map<VehiculoDto>(entidad);

            entidad.EstaBorrado = true;
            entidad.Borrado = DateTime.UtcNow;

            _notificacionRepositorio.Editar(entidad);

            await _notificacionRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();

            return new OperacionDto<RespuestaSimpleDto<string>>(new RespuestaSimpleDto<string>() { Mensaje = "Se eliminó correctamente" });
        }


        public async Task<OperacionDto<NotificacionDto>> DetalleAsync(string id)
        {
            var idNotificacion = RijndaelUtilitario.DecryptRijndaelFromUrl<int>(id);
            var entidad = await _notificacionRepositorio.BuscarPorIdAsync(idNotificacion);
            if (entidad == null)
            {
                return new OperacionDto<NotificacionDto>(CodigosOperacionDto.Invalido, "No se encontro el registro");
            }
            var dto = _mapper.Map<NotificacionDto>(entidad);

            return new OperacionDto<NotificacionDto>(dto);
        }

    }
}
