﻿using Orbelite.Erp.Servicios.Notificaciones.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Servicios.Notificaciones.Servicios.Abstracciones
{
    public interface IContactoServicio
    {

        Task<OperacionDto<JQueryDatatableDto<ContactoDto>>> ListarPaginadosAsync(
            string columna, int start, int length, int draw, long? idUsuario, string documento);

        Task<OperacionDto<RespuestaSimpleDto<string>>> RegistrarContactoAsync(ContactoDto peticion);

        Task<OperacionDto<RespuestaSimpleDto<string>>> EditarContactoAsync(ContactoDto peticion);

    }
}
