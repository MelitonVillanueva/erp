﻿using Orbelite.Erp.Servicios.Notificaciones.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Servicios.Notificaciones.Servicios.Abstracciones
{
    public interface INotificacionServicio
    {

        Task<OperacionDto<JQueryDatatableDto<NotificacionDto>>> ListarPaginadosAsync(
            string columna, int start, int length, int draw, long? idUsuario, string nombre);

        Task<OperacionDto<RespuestaSimpleDto<string>>> RegistrarNotificacionAsync(NotificacionDto peticion);

        Task<OperacionDto<RespuestaSimpleDto<string>>> EditarNotificacionAsync(NotificacionDto peticion);

        Task<OperacionDto<RespuestaSimpleDto<string>>> EliminarAsync(string id);

        Task<OperacionDto<NotificacionDto>> DetalleAsync(string id);

    }
}
