﻿using AutoMapper;
using Orbelite.Erp.Datos.Notificaciones.Entidades;
using Orbelite.Erp.Servicios.Notificaciones.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Notificaciones.Mapeo
{
    public class ContactoProfile : Profile
    {
        public ContactoProfile()
        {
            CreateMap<Contacto, ContactoDto>()
                 .AfterMap<ContactoProfileAction>();
        }
    }
}
