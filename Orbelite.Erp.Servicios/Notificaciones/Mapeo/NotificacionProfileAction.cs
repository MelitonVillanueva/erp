﻿using AutoMapper;
using Orbelite.Erp.Datos.Notificaciones.Entidades;
using Orbelite.Erp.Servicios.Notificaciones.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Utilitarios;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Notificaciones.Mapeo
{
    public class NotificacionProfileAction : IMappingAction<Notificacion, NotificacionDto>
    {

        public void Process(Notificacion source, NotificacionDto destination, ResolutionContext context)
        {

            destination.Id = RijndaelUtilitario.EncryptRijndaelToUrl(source.IdNotificacion.ToString());
            
        }

    }
}
