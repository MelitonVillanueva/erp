﻿using AutoMapper;
using Orbelite.Erp.Datos.Notificaciones.Entidades;
using Orbelite.Erp.Servicios.Entidad.Dtos;
using Orbelite.Erp.Servicios.Notificaciones.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Utilitarios;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Notificaciones.Mapeo
{
    public class ContactoProfileAction : IMappingAction<Contacto, ContactoDto>
    {

        public void Process(Contacto source, ContactoDto destination, ResolutionContext context)
        {

            destination.IdContacto = RijndaelUtilitario.EncryptRijndaelToUrl(source.IdContacto.ToString());
            if (source.Persona != null)
            {
                destination.Persona = context.Mapper.Map<PersonaDto>(source.Persona);
            }

        }

    }
}
