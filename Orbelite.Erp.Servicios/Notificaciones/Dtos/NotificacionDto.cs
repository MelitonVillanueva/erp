﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Notificaciones.Dtos
{
    public class NotificacionDto
    {


        public string Id { get; set; }
        public string Nombre { get; set; }
        public int Dias { get; set; }
        public bool EsDeSistema { get; set; }

        public int TipoAlerta { get; set; }


    }
}
