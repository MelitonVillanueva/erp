﻿using Orbelite.Erp.Servicios.Entidad.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Notificaciones.Dtos
{
    public class ContactoDto
    {

        public string IdContacto { get; set; }
        public bool Vencimiento { get; set; }

        public PersonaDto Persona { get; set; }

    }
}
