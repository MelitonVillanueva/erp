﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Orbelite.Erp.Servicios.Admin.Auth.Usuarios.Dtos
{
    public class RecuperarContraseniaDto
    {

        [Required(ErrorMessage = "Usuario es requerido")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Correo eletrónico es requerido")]
        public string Correo { get; set; }


        [JsonIgnore]
        public string Plantilla { get; set; }

    }
}
