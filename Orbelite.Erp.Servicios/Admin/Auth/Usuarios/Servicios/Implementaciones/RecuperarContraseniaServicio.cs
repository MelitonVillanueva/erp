﻿using Orbelite.Erp.Servicios.Admin.Auth.Usuarios.Dtos;
using Orbelite.Erp.Servicios.Auth.Usuarios.Servicios.Abstracciones;
using Orbelite.Erp.Servicios.General.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Utilitarios;
using Orbelite.Principal.Seguridad.Servicios.General.Dtos;
using Orbelite.Principal.Seguridad.Servicios.Peticiones.Dtos;
using Orbelite.Principal.Seguridad.Servicios.Peticiones.Servicios.Abstracciones;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Servicios.Auth.Usuarios.Servicios.Implementaciones
{
    public class RecuperarContraseniaServicio: IRecuperarContraseniaServicio
    {
        private readonly ConfiguracionDto _configuracion;
        private readonly UrlConfiguracionDto _urlConfiguracion;
        private readonly SeguridadConfiguracionDto _seguridadConfiguracionDto;
        private readonly Principal.Seguridad.Servicios.Auth.Servicios.Abstracciones.IUsuarioServicio _usuarioInfraxionServicio;
        private readonly Principal.Seguridad.Datos.Repositorios.Abstracciones.IPersonaRepositorio _personaRepositorio;
        private readonly Principal.Seguridad.Datos.Repositorios.Abstracciones.IUsuarioRepositorio _usuarioInfraxionRepositorio;        
        private readonly IPeticionServicio _peticionServicio;        

        public RecuperarContraseniaServicio(
            ConfiguracionDto configuracion,
            UrlConfiguracionDto urlConfiguracion,
            SeguridadConfiguracionDto seguridadConfiguracionDto,
            Principal.Seguridad.Datos.Repositorios.Abstracciones.IPersonaRepositorio personaRepositorio,
           Principal.Seguridad.Servicios.Auth.Servicios.Abstracciones.IUsuarioServicio usuarioInfraxionServicio,
           Principal.Seguridad.Datos.Repositorios.Abstracciones.IUsuarioRepositorio usuarioInfraxionRepositorio,
          IPeticionServicio peticionServicio
                               )
        {
            _configuracion = configuracion;
            _urlConfiguracion = urlConfiguracion;
            _seguridadConfiguracionDto = seguridadConfiguracionDto;
            _personaRepositorio = personaRepositorio;
            _usuarioInfraxionServicio = usuarioInfraxionServicio;            
            _usuarioInfraxionRepositorio = usuarioInfraxionRepositorio;
            _peticionServicio = peticionServicio;
        }

        public async Task<OperacionDto<RespuestaSimpleDto<bool>>> EnviarCorreoRecuperarContrasenia(RecuperarContraseniaDto peticion)
        {
            var operacionValidacion = ValidacionUtilitario.ValidarModelo<RespuestaSimpleDto<bool>>(peticion);
            if (!operacionValidacion.Completado)
            {
                return operacionValidacion;
            }

            var usuario = await _usuarioInfraxionRepositorio.BuscarPorUsernameAsync(peticion.Username);
            if (usuario == null)
            {
                return new OperacionDto<RespuestaSimpleDto<bool>>(CodigosOperacionDto.NoExiste, "No existe Usuario ingresado");
            }

            if (!usuario.Correo.Equals(peticion.Correo, StringComparison.OrdinalIgnoreCase))
            {
                return new OperacionDto<RespuestaSimpleDto<bool>>(CodigosOperacionDto.NoExiste, "El Correo Electrónico no pertenece al Usuario");
            }

            var personaInfraxion = await _usuarioInfraxionServicio.BuscarPersonaPorUsuarioAsync(usuario.IdUsuario);
            if (!personaInfraxion.Completado)
            {
                return new OperacionDto<RespuestaSimpleDto<bool>>(CodigosOperacionDto.NoExiste, personaInfraxion.Mensajes);
            }

            var html = File.ReadAllText(peticion.Plantilla);

            var cadena = $"{usuario.IdUsuario}__{DateTime.UtcNow.Ticks}";
            var IdRecuperarContrasenia = RijndaelUtilitario.EncryptRijndaelToUrl(cadena);

            html = html.Replace("%%NOMBRES%%", $"{personaInfraxion.Resultado.Nombres} {personaInfraxion.Resultado.ApellidoPaterno} {personaInfraxion.Resultado.ApellidoMaterno}");            
            html = html.Replace("%%FECHA%%", DateTime.UtcNow.AddHours(-5).ToString("dd/MM/yyyy hh:mm:ss tt"));
            html = html.Replace("%%LINK_RECUPERAR%%", $"{_configuracion.General.UrlBase}Admin/Auth/NuevaContrasenia/{IdRecuperarContrasenia}");
            var cuerpo = html;

            var destinatarios = new List<string>();
            destinatarios.Add(peticion.Correo);

            var cuerpoCorreo = new CorreoParametrosDto
            {
                Asunto = "Solicitud de cambio de Contraseña - IBROKER",
                Para = destinatarios,
                Contenido = cuerpo,
                Usuario = _configuracion.Email.Email_Usuario,
                Password = _configuracion.Email.Email_Psw,
                Puerto = int.Parse(_configuracion.Email.Email_Port),
                Servidor = _configuracion.Email.Email_Host,
                Ssl = true
            };
            var cabecera = new Dictionary<string, string>();
            cabecera.Add("app-name", _seguridadConfiguracionDto.AppName);
           var operacionEmail = _peticionServicio.EnviarCorreoJsonPost($"{_urlConfiguracion.UrlOrbeliteApp}api/Notificacion/Email/Crear", cuerpoCorreo, 0, cabecera);
            

            return new OperacionDto<RespuestaSimpleDto<bool>>(
                new RespuestaSimpleDto<bool>()
                {
                    Id = true,
                    Mensaje = "Se envió correctamente"
                }
                );
        }
        
        //public async Task<OperacionDto<RespuestaSimpleDto<string>>> CambiarContrasenia(CambiarContraseniaDto peticion)
        //{
            
        //    var operacionValidacion = ValidacionUtilitario.ValidarModelo<RespuestaSimpleDto<string>>(peticion);
        //    if (!operacionValidacion.Completado)
        //    {
        //        return operacionValidacion;
        //    }
            
        //    if (peticion.Contrasenia.Trim() != peticion.ConfirmarContrasenia.Trim())
        //    {
        //        return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.NoExiste, "Confirme correctamente su contraseña");
        //    }

        //    string[] cadena = RijndaelUtilitario.DecryptRijndaelFromUrl<string>(peticion.Token).Split("__");

        //    var fecha = new DateTime(long.Parse(cadena[1]));            
        //    TimeSpan span = (DateTime.UtcNow - fecha);
        //    if (span.Minutes > 10)
        //    {
        //        return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.NoExiste, "Su tiempo del espera supero los 10 min, debe realizar otra solicitud");
        //    }

        //    var entidad = await _usuarioInfraxionRepositorio.BuscarPorIdAsync(Convert.ToInt64(cadena[0]));
        //    if (entidad == null)
        //    {
        //        return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.NoExiste, "No existe el usuario ingresado");
        //    }

        //    var personaInfraxion = await _usuarioInfraxionServicio.BuscarPersonaPorUsuarioAsync(entidad.IdUsuario);
        //    if (!personaInfraxion.Completado)
        //    {
        //        return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.NoExiste, personaInfraxion.Mensajes);
        //    }
        //    var IdUsuarioCadena = RijndaelUtilitario.EncryptRijndaelToUrl(entidad.IdUsuario);
        //    entidad.Password = Md5Utilitario.Cifrar(peticion.Contrasenia.Trim(), _seguridadConfiguracionDto.PasswordSalt);
        //    entidad.PasswordSalt = _seguridadConfiguracionDto.PasswordSalt;
        //    entidad.Actualizado = DateTime.UtcNow;
        //    await _usuarioInfraxionRepositorio.ActualizarClaveAsync(entidad);

        //    var html = File.ReadAllText(peticion.Plantilla);



        //    html = html.Replace("%%NOMBRES%%", $"{personaInfraxion.Resultado.Nombres} {personaInfraxion.Resultado.ApellidoPaterno} {personaInfraxion.Resultado.ApellidoMaterno}");            
        //    html = html.Replace("%%FECHA%%", DateTime.UtcNow.AddHours(-5).ToString("dd/MM/yyyy hh:mm:ss tt"));
        //    var cuerpo = html;

        //    var destinatarios = new List<string>();
        //    destinatarios.Add(entidad.Correo);

        //    var cuerpoCorreo = new CorreoParametrosDto
        //    {
        //        Asunto = "Tu contraseña se actualizó correctamente en IBROKER",
        //        Para = destinatarios,
        //        Contenido = cuerpo,
        //        Usuario = _configuracion.Email.Email_Usuario,
        //        Password = _configuracion.Email.Email_Psw,
        //        Puerto = int.Parse(_configuracion.Email.Email_Port),
        //        Servidor = _configuracion.Email.Email_Host,
        //        Ssl = true
        //    };
        //    var cabecera = new Dictionary<string, string>();
        //    cabecera.Add("app-name", _seguridadConfiguracionDto.AppName);

        //   var operacionEmail = _peticionServicio.EnviarCorreoJsonPost($"{_urlConfiguracion.UrlOrbeliteApp}api/Notificacion/Email/Crear", cuerpoCorreo, 0, cabecera);

        //    return new OperacionDto<RespuestaSimpleDto<string>>(
        //        new RespuestaSimpleDto<string>()
        //        {
        //            Id = IdUsuarioCadena,
        //            Mensaje = "Se actualizó correctamente"
        //        }
        //        );
        //}


    }
}
