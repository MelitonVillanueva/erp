﻿using Orbelite.Erp.Servicios.Admin.Auth.Usuarios.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Servicios.Auth.Usuarios.Servicios.Abstracciones
{
    public interface IRecuperarContraseniaServicio
    {
        Task<OperacionDto<RespuestaSimpleDto<bool>>> EnviarCorreoRecuperarContrasenia(RecuperarContraseniaDto peticion);
        //Task<OperacionDto<RespuestaSimpleDto<string>>> CambiarContrasenia(CambiarContraseniaDto peticion);
    }
}
