﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Orbelite.Erp.Servicios.Admin.Me.Dtos
{
    public class ConsultarSbsCalificacionPeticionDto
    {
        [Required(ErrorMessage = "Documento es requerido")]
        public string Documento { get; set; }

        [Required(ErrorMessage = "Tipo es requerido")]
        [Range(1, short.MaxValue, ErrorMessage = "Un Tipo Válido es requerido")]
        public int Tipo { get; set; }
    }
}
