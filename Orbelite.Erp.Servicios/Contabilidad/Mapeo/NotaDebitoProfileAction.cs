﻿using AutoMapper;
using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Servicios.Contabilidad.Dtos;
using Orbelite.Erp.Servicios.Entidad.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Utilitarios;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Contabilidad.Mapeo
{
    public class NotaDebitoProfileAction : IMappingAction<NotaDebito, NotaDebitoDto>
    {

        public void Process(NotaDebito source, NotaDebitoDto destination, ResolutionContext context)
        {
            //if (!string.IsNullOrWhiteSpace(source.Dni))
            //{
            //    destination.Dni = source.Dni.PadLeft(8, '0');
            //}

            destination.ValorVenta = decimal.Round(source.ValorVenta, 2);
            destination.Isc = decimal.Round(source.Isc, 2);
            destination.Igv = decimal.Round(source.Igv, 2);
            destination.OtrosCargos = decimal.Round(source.OtrosCargos, 2);
            destination.OtrosTributos = decimal.Round(source.OtrosTributos, 2);
            destination.ImporteTotal = decimal.Round(source.ImporteTotal, 2);

            destination.IdNotaDebito = RijndaelUtilitario.EncryptRijndaelToUrl(source.IdNotaDebito.ToString());
            if (source.Persona != null)
            {
                destination.Persona = context.Mapper.Map<PersonaDto>(source.Persona);
            }

            if (source.Factura != null)
            {
                destination.Factura = context.Mapper.Map<FacturaDto>(source.Factura);
            }
        }

    }
}
