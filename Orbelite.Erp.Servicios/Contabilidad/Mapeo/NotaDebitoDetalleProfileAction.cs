﻿using AutoMapper;
using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Servicios.Contabilidad.Dtos;
using Orbelite.Erp.Servicios.Inventario.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Utilitarios;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Contabilidad.Mapeo
{
    public class NotaDebitoDetalleProfileAction : IMappingAction<NotaDebitoDetalle, DetalleNotaDebitoDto>
    {

        public void Process(NotaDebitoDetalle source, DetalleNotaDebitoDto destination, ResolutionContext context)
        {
            //if (!string.IsNullOrWhiteSpace(source.Dni))
            //{
            //    destination.Dni = source.Dni.PadLeft(8, '0');
            //}

            destination.Precio = decimal.Round(source.Precio, 2);

            destination.IdNotaDebitoDetalle = RijndaelUtilitario.EncryptRijndaelToUrl(source.IdNotaDebitoDetalle.ToString());
            if (source.Producto != null)
            {
                destination.Producto = context.Mapper.Map<ProductoDto>(source.Producto);
            }
        }

    }
}
