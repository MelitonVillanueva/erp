﻿using AutoMapper;
using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Servicios.Contabilidad.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Contabilidad.Mapeo
{
    public class FacturaDetalleProfile : Profile
    {
        public FacturaDetalleProfile()
        {
            CreateMap<FacturaDetalle, DetalleFacturaDto>()
                 .AfterMap<FacturaDetalleProfileAction>();
        }
    }
}
