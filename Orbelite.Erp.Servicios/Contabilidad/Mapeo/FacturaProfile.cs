﻿using AutoMapper;
using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Servicios.Contabilidad.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Contabilidad.Mapeo
{
    public class ContactoProfile : Profile
    {
        public ContactoProfile()
        {
            CreateMap<Factura, FacturaDto>()
                 .AfterMap<FacturaProfileAction>();
        }
    }
}
