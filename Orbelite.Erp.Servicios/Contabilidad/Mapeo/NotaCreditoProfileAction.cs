﻿using AutoMapper;
using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Servicios.Contabilidad.Dtos;
using Orbelite.Erp.Servicios.Entidad.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Utilitarios;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Contabilidad.Mapeo
{
    public class NotaCreditoProfileAction : IMappingAction<NotaCredito, NotaCreditoDto>
    {

        public void Process(NotaCredito source, NotaCreditoDto destination, ResolutionContext context)
        {
            //if (!string.IsNullOrWhiteSpace(source.Dni))
            //{
            //    destination.Dni = source.Dni.PadLeft(8, '0');
            //}

            destination.SubTotalVentas = decimal.Round(source.SubTotalVentas, 2);
            destination.Anticipos = decimal.Round(source.Anticipos, 2);
            destination.Descuentos = decimal.Round(source.Descuentos, 2);
            destination.ValorVenta = decimal.Round(source.ValorVenta, 2);
            destination.Isc = decimal.Round(source.Isc, 2);
            destination.Igv = decimal.Round(source.Igv, 2);
            destination.OtrosCargos = decimal.Round(source.OtrosCargos, 2);
            destination.OtrosTributos = decimal.Round(source.OtrosTributos, 2);
            destination.MontoDeRedondeo = decimal.Round(source.MontoDeRedondeo, 2);
            destination.ImporteTotal = decimal.Round(source.ImporteTotal, 2);

            destination.IdNotaCredito = RijndaelUtilitario.EncryptRijndaelToUrl(source.IdNotaCredito.ToString());
            if (source.Persona != null)
            {
                destination.Persona = context.Mapper.Map<PersonaDto>(source.Persona);
            }

            if (source.Factura != null)
            {
                destination.Factura = context.Mapper.Map<FacturaDto>(source.Factura);
            }
        }

    }
}
