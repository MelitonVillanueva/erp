﻿using AutoMapper;
using Orbelite.Erp.Datos.Contabilidad.Entidades;
using Orbelite.Erp.Servicios.Contabilidad.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Contabilidad.Mapeo
{
    public class NotaDebitoProfile : Profile
    {
        public NotaDebitoProfile()
        {
            CreateMap<NotaDebito, NotaDebitoDto>()
                 .AfterMap<NotaDebitoProfileAction>();
        }
    }
}
