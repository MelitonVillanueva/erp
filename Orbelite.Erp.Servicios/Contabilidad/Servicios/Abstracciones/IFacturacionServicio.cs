﻿using Orbelite.Erp.Servicios.Contabilidad.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Servicios.Contabilidad.Servicios.Abstracciones
{
    public interface IFacturacionServicio
    {

        Task<OperacionDto<JQueryDatatableDto<FacturaDto>>> ListarPaginadosAsync(
            string columna, int start, int length, int draw, long? idUsuario,
            string serie, string ruc, DateTime? emision, DateTime? vencimiento);

        Task<OperacionDto<JQueryDatatableDto<NotaCreditoDto>>> ListarPaginadosNotaCreditoAsync(
            string columna, int start, int length, int draw, long? idUsuario,
            string serie, string ruc, DateTime? emision);

        Task<OperacionDto<JQueryDatatableDto<NotaDebitoDto>>> ListarPaginadosNotaDebitoAsync(
            string columna, int start, int length, int draw, long? idUsuario,
            string serie, string ruc, DateTime? emision);
        Task<OperacionDto<JQueryDatatableDto<DetalleFacturaDto>>> ListarPaginadosDetalleFacturaAsync(
            string columna, int start, int length, int draw, long? idUsuario,
            long idFactura);

        Task<OperacionDto<JQueryDatatableDto<DetalleNotaCreditoDto>>> ListarPaginadosDetalleNotaCreditoAsync(
            string columna, int start, int length, int draw, long? idUsuario,
            long idNotaCredito);

        Task<OperacionDto<JQueryDatatableDto<DetalleNotaDebitoDto>>> ListarPaginadosDetalleNotaDebitoAsync(
            string columna, int start, int length, int draw, long? idUsuario,
            long idNotaDebito);

        Task<OperacionDto<List<DetalleFacturaDto>>> ListarDetalleFacturaAsync(string idFactura);
        Task<OperacionDto<List<DetalleNotaCreditoDto>>> ListarDetalleNotaCreditoAsync(string idNotaCredito);
        Task<OperacionDto<List<DetalleNotaDebitoDto>>> ListarDetalleNotaDebitoAsync(string idNotaDebito);

        Task<OperacionDto<FacturaDto>> ObtenerFacturaAsync(string IdFactura);
        Task<OperacionDto<NotaCreditoDto>> ObtenerNotaCreditoAsync(string IdNotaCredito);
        Task<OperacionDto<NotaDebitoDto>> ObtenerNotaDebitoAsync(string IdNotaDebito);

        Task<OperacionDto<List<NotaDebitoDto>>> ListarNotaDebitoPorCoincidenciaAsync(string coincidencia);
        Task<OperacionDto<List<NotaCreditoDto>>> ListarNotaCreditoPorCoincidenciaAsync(string coincidencia);
        Task<OperacionDto<List<FacturaDto>>> ListarNotaFacturaPorCoincidenciaAsync(string coincidencia);


        Task<OperacionDto<JQueryDatatableDto<FacturaDto>>> ListarUltimosCincoAsync(string columna, int draw);

    }
}
