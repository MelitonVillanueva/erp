﻿using AutoMapper;
using Orbelite.Erp.Datos.Contabilidad.Repositorios.Abstracciones;
using Orbelite.Erp.Servicios.Contabilidad.Dtos;
using Orbelite.Erp.Servicios.Contabilidad.Servicios.Abstracciones;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Utilitarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Servicios.Contabilidad.Servicios.Implementaciones
{
    public class FacturacionServicio : IFacturacionServicio
    {

        private readonly IFacturaRepositorio _facturaRepositorio;
        private readonly INotaCreditoRepositorio _notaCreditoRepositorio;
        private readonly INotaDebitoRepositorio _notaDebitoRepositorio;
        private readonly IFacturaDetalleRepositorio _facturaDetalleRepositorio;
        private readonly INotaCreditoDetalleRepositorio _notaCreditoDetalleRepositorio;
        private readonly INotaDebitoDetalleRepositorio _notaDebitoDetalleRepositorio;
        private readonly IMapper _mapper;

        public FacturacionServicio(
            IFacturaRepositorio facturaRepositorio,
            INotaCreditoRepositorio notaCreditoRepositorio,
            INotaDebitoRepositorio notaDebitoRepositorio,
            IFacturaDetalleRepositorio facturaDetalleRepositorio,
            INotaCreditoDetalleRepositorio notaCreditoDetalleRepositorio,
            INotaDebitoDetalleRepositorio notaDebitoDetalleRepositorio,
            IMapper mapper
            )
        {
            _facturaRepositorio = facturaRepositorio;
            _notaCreditoRepositorio = notaCreditoRepositorio;
            _notaDebitoRepositorio = notaDebitoRepositorio;
            _facturaDetalleRepositorio = facturaDetalleRepositorio;
            _notaCreditoDetalleRepositorio = notaCreditoDetalleRepositorio;
            _notaDebitoDetalleRepositorio = notaDebitoDetalleRepositorio;
            _mapper = mapper;
        }

        public async Task<OperacionDto<List<DetalleFacturaDto>>> ListarDetalleFacturaAsync(string idFactura)
        {

            var id = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(idFactura);

            var resultados = await _facturaDetalleRepositorio.ListarPorFactura(id);

            var listaDto = resultados.Select(e => _mapper.Map<DetalleFacturaDto>(e)).ToList();

            return new OperacionDto<List<DetalleFacturaDto>>(listaDto);

        }

        public async Task<OperacionDto<List<DetalleNotaCreditoDto>>> ListarDetalleNotaCreditoAsync(string idNotaCredito)
        {

            var id = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(idNotaCredito);

            var resultados = await _notaCreditoDetalleRepositorio.ListarPorNotaCredito(id);

            var listaDto = resultados.Select(e => _mapper.Map<DetalleNotaCreditoDto>(e)).ToList();

            return new OperacionDto<List<DetalleNotaCreditoDto>>(listaDto);

        }

        public async Task<OperacionDto<List<DetalleNotaDebitoDto>>> ListarDetalleNotaDebitoAsync(string idNotaDebito)
        {

            var id = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(idNotaDebito);

            var resultados = await _notaDebitoDetalleRepositorio.ListarPorNotaDebito(id);

            var listaDto = resultados.Select(e => _mapper.Map<DetalleNotaDebitoDto>(e)).ToList();

            return new OperacionDto<List<DetalleNotaDebitoDto>>(listaDto);

        }


        public async Task<OperacionDto<JQueryDatatableDto<DetalleFacturaDto>>> ListarPaginadosDetalleFacturaAsync(string columna, int start, int length, int draw, long? idUsuario, long idFactura)
        {
            var resultados = await _facturaDetalleRepositorio.ListarPaginadosAsync(
                columna, start, length, idUsuario, idFactura
                );

            var dto = new JQueryDatatableDto<DetalleFacturaDto>()
            {
                Data = resultados.Item1.Select(e => _mapper.Map<DetalleFacturaDto>(e)).ToList(),
                RecordsTotal = resultados.Item2,
                Draw = draw,
                RecordsFiltered = resultados.Item2
            };
            return new OperacionDto<JQueryDatatableDto<DetalleFacturaDto>>(dto);
        }

        public async Task<OperacionDto<JQueryDatatableDto<DetalleNotaCreditoDto>>> ListarPaginadosDetalleNotaCreditoAsync(string columna, int start, int length, int draw, long? idUsuario, long idNotaCredito)
        {
            var resultados = await _notaCreditoDetalleRepositorio.ListarPaginadosAsync(
                columna, start, length, idUsuario, idNotaCredito
                );

            var dto = new JQueryDatatableDto<DetalleNotaCreditoDto>()
            {
                Data = resultados.Item1.Select(e => _mapper.Map<DetalleNotaCreditoDto>(e)).ToList(),
                RecordsTotal = resultados.Item2,
                Draw = draw,
                RecordsFiltered = resultados.Item2
            };
            return new OperacionDto<JQueryDatatableDto<DetalleNotaCreditoDto>>(dto);
        }

        public async Task<OperacionDto<JQueryDatatableDto<DetalleNotaDebitoDto>>> ListarPaginadosDetalleNotaDebitoAsync(string columna, int start, int length, int draw, long? idUsuario, long idNotaDebito)
        {
            var resultados = await _notaDebitoDetalleRepositorio.ListarPaginadosAsync(
                columna, start, length, idUsuario, idNotaDebito
                );

            var dto = new JQueryDatatableDto<DetalleNotaDebitoDto>()
            {
                Data = resultados.Item1.Select(e => _mapper.Map<DetalleNotaDebitoDto>(e)).ToList(),
                RecordsTotal = resultados.Item2,
                Draw = draw,
                RecordsFiltered = resultados.Item2
            };
            return new OperacionDto<JQueryDatatableDto<DetalleNotaDebitoDto>>(dto);
        }

        public async Task<OperacionDto<JQueryDatatableDto<FacturaDto>>> ListarPaginadosAsync(string columna, int start, int length, int draw, long? idUsuario, string serie, string ruc, DateTime? emision, DateTime? vencimiento)
        {
            var resultados = await _facturaRepositorio.ListarPaginadosAsync(
                columna, start, length, idUsuario,
                serie, ruc, emision, vencimiento
                );

            var dto = new JQueryDatatableDto<FacturaDto>()
            {
                Data = resultados.Item1.Select(e => _mapper.Map<FacturaDto>(e)).ToList(),
                RecordsTotal = resultados.Item2,
                Draw = draw,
                RecordsFiltered = resultados.Item2
            };
            return new OperacionDto<JQueryDatatableDto<FacturaDto>>(dto);
        }

        public async Task<OperacionDto<JQueryDatatableDto<NotaCreditoDto>>> ListarPaginadosNotaCreditoAsync(string columna, int start, int length, int draw, long? idUsuario, string serie, string ruc, DateTime? emision)
        {
            var resultados = await _notaCreditoRepositorio.ListarPaginadosAsync(
                columna, start, length, idUsuario,
                serie, ruc, emision
                );

            var dto = new JQueryDatatableDto<NotaCreditoDto>()
            {
                Data = resultados.Item1.Select(e => _mapper.Map<NotaCreditoDto>(e)).ToList(),
                RecordsTotal = resultados.Item2,
                Draw = draw,
                RecordsFiltered = resultados.Item2
            };
            return new OperacionDto<JQueryDatatableDto<NotaCreditoDto>>(dto);
        }

        public async Task<OperacionDto<JQueryDatatableDto<NotaDebitoDto>>> ListarPaginadosNotaDebitoAsync(string columna, int start, int length, int draw, long? idUsuario, string serie, string ruc, DateTime? emision)
        {
            var resultados = await _notaDebitoRepositorio.ListarPaginadosAsync(
                columna, start, length, idUsuario,
                serie, ruc, emision
                );

            var dto = new JQueryDatatableDto<NotaDebitoDto>()
            {
                Data = resultados.Item1.Select(e => _mapper.Map<NotaDebitoDto>(e)).ToList(),
                RecordsTotal = resultados.Item2,
                Draw = draw,
                RecordsFiltered = resultados.Item2
            };
            return new OperacionDto<JQueryDatatableDto<NotaDebitoDto>>(dto);
        }

        public async Task<OperacionDto<FacturaDto>> ObtenerFacturaAsync(string IdFactura)
        {
            var id = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(IdFactura);

            var entidad = await _facturaRepositorio.BuscarPorIdAsync(id);

            if (entidad == null)
            {
                return new OperacionDto<FacturaDto>(CodigosOperacionDto.NoExiste, "No existe la factura");
            }

            if (entidad.IdCliente.HasValue)
            {
                await _facturaRepositorio.UnidadDeTrabajo.Entry(entidad).Reference(e => e.Persona).LoadAsync();
            }

            var dto = _mapper.Map<FacturaDto>(entidad);

            return new OperacionDto<FacturaDto>(dto);
        }

        public async Task<OperacionDto<NotaCreditoDto>> ObtenerNotaCreditoAsync(string IdNotaCredito)
        {
            var id = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(IdNotaCredito);

            var entidad = await _notaCreditoRepositorio.BuscarPorIdAsync(id);

            if (entidad == null)
            {
                return new OperacionDto<NotaCreditoDto>(CodigosOperacionDto.NoExiste, "No existe la nota de crédito");
            }

            if (entidad.IdCliente.HasValue)
            {
                await _notaCreditoRepositorio.UnidadDeTrabajo.Entry(entidad).Reference(e => e.Persona).LoadAsync();
            }

            //if (entidad.IdFactura.HasValue)
            //{
            await _notaCreditoRepositorio.UnidadDeTrabajo.Entry(entidad).Reference(e => e.Factura).LoadAsync();
            //}

            var dto = _mapper.Map<NotaCreditoDto>(entidad);

            return new OperacionDto<NotaCreditoDto>(dto);
        }

        public async Task<OperacionDto<NotaDebitoDto>> ObtenerNotaDebitoAsync(string IdNotaDebito)
        {
            var id = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(IdNotaDebito);

            var entidad = await _notaDebitoRepositorio.BuscarPorIdAsync(id);

            if (entidad == null)
            {
                return new OperacionDto<NotaDebitoDto>(CodigosOperacionDto.NoExiste, "No existe la nota de débito");
            }

            if (entidad.IdCliente.HasValue)
            {
                await _notaDebitoRepositorio.UnidadDeTrabajo.Entry(entidad).Reference(e => e.Persona).LoadAsync();
            }

            //if (entidad.IdFactura.HasValue)
            //{
            await _notaDebitoRepositorio.UnidadDeTrabajo.Entry(entidad).Reference(e => e.Factura).LoadAsync();
            //}

            var dto = _mapper.Map<NotaDebitoDto>(entidad);

            return new OperacionDto<NotaDebitoDto>(dto);
        }

        public async Task<OperacionDto<List<NotaDebitoDto>>> ListarNotaDebitoPorCoincidenciaAsync(string coincidencia)
        {
            //var idSitioWeb = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(idSitioWebCifrado);

            var lista = await _notaDebitoRepositorio.BuscarPorCoincidenciaAsync(coincidencia, 20);

            lista.Select(e => _mapper.Map<NotaDebitoDto>(e)).ToList();

            //var listaDto = _mapper.Map<List<ProductoDto>>(lista);
            var listaDto = lista.Select(e => _mapper.Map<NotaDebitoDto>(e)).ToList();

            //resultados.Item1.Select(e => _mapper.Map<ProductoDto>(e)).ToList(),
            return new OperacionDto<List<NotaDebitoDto>>(listaDto);
        }

        public async Task<OperacionDto<List<NotaCreditoDto>>> ListarNotaCreditoPorCoincidenciaAsync(string coincidencia)
        {
            //var idSitioWeb = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(idSitioWebCifrado);

            var lista = await _notaCreditoRepositorio.BuscarPorCoincidenciaAsync(coincidencia, 20);

            lista.Select(e => _mapper.Map<NotaCreditoDto>(e)).ToList();

            //var listaDto = _mapper.Map<List<ProductoDto>>(lista);
            var listaDto = lista.Select(e => _mapper.Map<NotaCreditoDto>(e)).ToList();

            //resultados.Item1.Select(e => _mapper.Map<ProductoDto>(e)).ToList(),
            return new OperacionDto<List<NotaCreditoDto>>(listaDto);
        }

        public async Task<OperacionDto<List<FacturaDto>>> ListarNotaFacturaPorCoincidenciaAsync(string coincidencia)
        {
            //var idSitioWeb = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(idSitioWebCifrado);

            var lista = await _facturaRepositorio.BuscarPorCoincidenciaAsync(coincidencia, 20);

            lista.Select(e => _mapper.Map<FacturaDto>(e)).ToList();

            //var listaDto = _mapper.Map<List<ProductoDto>>(lista);
            var listaDto = lista.Select(e => _mapper.Map<FacturaDto>(e)).ToList();

            //resultados.Item1.Select(e => _mapper.Map<ProductoDto>(e)).ToList(),
            return new OperacionDto<List<FacturaDto>>(listaDto);
        }


        public async Task<OperacionDto<JQueryDatatableDto<FacturaDto>>> ListarUltimosCincoAsync(string columna, int draw)
        {
            var resultados = await _facturaRepositorio.ListarUltimosCincoAsync(
                columna);

            var dto = new JQueryDatatableDto<FacturaDto>()
            {
                Data = resultados.Item1.Select(e => _mapper.Map<FacturaDto>(e)).ToList(),
                RecordsTotal = resultados.Item2,
                Draw = draw,
                RecordsFiltered = resultados.Item2
            };
            return new OperacionDto<JQueryDatatableDto<FacturaDto>>(dto);
        }

    }
}
