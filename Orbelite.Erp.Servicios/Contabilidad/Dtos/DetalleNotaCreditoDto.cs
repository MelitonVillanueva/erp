﻿using Orbelite.Erp.Servicios.Inventario.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Contabilidad.Dtos
{
    public class DetalleNotaCreditoDto
    {
        public string IdNotaCreditoDetalle { get; set; }
        public int Cantidad { get; set; }
        public decimal Precio { get; set; }
        public int IdProducto { get; set; }

        public ProductoDto Producto { get; set; }
    }
}
