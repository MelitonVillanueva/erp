﻿using Orbelite.Erp.Servicios.Entidad.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Orbelite.Erp.Servicios.Contabilidad.Dtos
{
    public class NotaCreditoDto
    {

        public string IdNotaCredito { get; set; }
        public string Numero { get; set; }

        [JsonIgnore]
        public DateTime? FechaEmision { get; set; }

        public string FechaEmisionCadena
        {
            get
            {
                return FechaEmision.HasValue ? FechaEmision.Value.ToString("dd/MM/yyyy") : null;
            }
        }

        public long IdCliente { get; set; }
        public long IdFactura { get; set; }
        public int IdMoneda { get; set; }
        public string Motivo { get; set; }
        public decimal SubTotalVentas { get; set; }
        public decimal ImporteTotal { get; set; }
        public decimal Anticipos { get; set; }
        public decimal Descuentos { get; set; }
        public decimal ValorVenta { get; set; }
        public decimal Isc { get; set; }
        public decimal Igv { get; set; }
        public decimal OtrosCargos { get; set; }
        public decimal OtrosTributos { get; set; }
        public decimal MontoDeRedondeo { get; set; }
        public DateTime Creado { get; set; }
        public DateTime Actualizado { get; set; }
        public bool? EstaBorrado { get; set; }
        public DateTime? Borrado { get; set; }

        public PersonaDto Persona { get; set; }
        public FacturaDto Factura { get; set; }

    }
}
