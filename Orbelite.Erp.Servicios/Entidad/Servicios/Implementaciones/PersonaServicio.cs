﻿using AutoMapper;
using Orbelite.Erp.Datos.Entidad.Entidades;
using Orbelite.Erp.Datos.Entidad.Repositorios.Abstracciones;
using Orbelite.Erp.Servicios.Entidad.Dtos;
using Orbelite.Erp.Servicios.Entidad.Servicios.Abstracciones;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Utilitarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Servicios.Entidad.Servicios.Implementaciones
{
    public class PersonaServicio : IPersonaServicio
    {

        private readonly IPersonaRepositorio _personaRepositorio;
        private readonly IMapper _mapper;
        public PersonaServicio(
            IPersonaRepositorio personaRepositorio,
            IMapper mapper
            )
        {
            _personaRepositorio = personaRepositorio;
            _mapper = mapper;
        }

        public async Task<OperacionDto<JQueryDatatableDto<PersonaDto>>> ListarPaginadosAsync(
            string columna, int start, int length, int draw, long? idUsuario,
            string documento, string nombres, string paterno, string materno, string razon)
        {
            var resultados = await _personaRepositorio.ListarPaginadosAsync(
                columna, start, length, idUsuario,
                documento, nombres, paterno, materno, razon
                );

            var dto = new JQueryDatatableDto<PersonaDto>()
            {
                Data = resultados.Item1.Select(e => _mapper.Map<PersonaDto>(e)).ToList(),
                RecordsTotal = resultados.Item2,
                Draw = draw,
                RecordsFiltered = resultados.Item2
            };
            return new OperacionDto<JQueryDatatableDto<PersonaDto>>(dto);
        }


        public async Task<OperacionDto<RespuestaSimpleDto<string>>> RegistrarPersonaAsync(PersonaDto peticion)
        {
            var operacionValidacion = ValidacionUtilitario.ValidarModelo<RespuestaSimpleDto<string>>(peticion);
            if (!operacionValidacion.Completado)
            {
                return operacionValidacion;
            }

            var entidad = new Persona()
            {
                Documento = peticion.Documento,
                Nombres = peticion.Nombres,
                Paterno = peticion.Paterno,
                Materno = peticion.Materno,
                RazonSocial = peticion.RazonSocial,
                IdTipo = peticion.IdTipo,
                Direccion = peticion.Direccion,
                Departamento = peticion.Departamento,
                Provincia = peticion.Provincia,
                Distrito = peticion.Distrito,
                Telefono = peticion.Telefono,
                Correo = peticion.Correo
            };

            var existe = await _personaRepositorio.BuscarPorDocumentoAsync(peticion.Documento);

            if (existe == null)
            {
                _personaRepositorio.Insertar(entidad);
                await _personaRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();
            }
            else
            {
                if (peticion.IngresoPorSunat)
                {
                    var idExistente = RijndaelUtilitario.EncryptRijndaelToUrl(existe.Id.ToString());
                    return new OperacionDto<RespuestaSimpleDto<string>>(new RespuestaSimpleDto<string>() { Id = idExistente });
                }

                return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, "El usuario ya existe");
            }

            var idCifrado = RijndaelUtilitario.EncryptRijndaelToUrl(entidad.Id.ToString());


            return new OperacionDto<RespuestaSimpleDto<string>>(new RespuestaSimpleDto<string>() { Id = idCifrado });

        }

        public async Task<OperacionDto<RespuestaSimpleDto<string>>> EditarPersonaAsync(PersonaDto peticion)
        {
            var operacionValidacion = ValidacionUtilitario.ValidarModelo<RespuestaSimpleDto<string>>(peticion);
            if (!operacionValidacion.Completado)
            {
                return operacionValidacion;
            }

            var id = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(peticion.Id);

            var entidad = await _personaRepositorio.BuscarPorIdAsync(id);
            if (entidad == null)
            {
                return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, "Id no existe");
            }

            entidad.Documento = peticion.Documento;
            entidad.Nombres = peticion.Nombres;
            entidad.Paterno = peticion.Paterno;
            entidad.Materno = peticion.Materno;
            entidad.RazonSocial = peticion.RazonSocial;
            entidad.IdTipo = peticion.IdTipo;
            entidad.Direccion = peticion.Direccion;
            entidad.Departamento = peticion.Departamento;
            entidad.Provincia = peticion.Provincia;
            entidad.Distrito = peticion.Distrito;
            entidad.Telefono = peticion.Telefono;
            entidad.Correo = peticion.Correo;
            entidad.Actualizado = DateTime.UtcNow;

            _personaRepositorio.Editar(entidad);
            await _personaRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();

            return new OperacionDto<RespuestaSimpleDto<string>>(new RespuestaSimpleDto<string>() { Mensaje = "Se guardó correctamente" });

        }

        public async Task<OperacionDto<PersonaDto>> DetalleAsync(string id)
        {
            var idPersona = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(id);
            var entidad = await _personaRepositorio.BuscarPorIdAsync(idPersona);
            if (entidad == null)
            {
                return new OperacionDto<PersonaDto>(CodigosOperacionDto.Invalido, "No se encontro el registro");
            }
            var dto = _mapper.Map<PersonaDto>(entidad);

            return new OperacionDto<PersonaDto>(dto);
        }

        public async Task<OperacionDto<RespuestaSimpleDto<string>>> EliminarAsync(string id)
        {
            var idVehiculo = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(id);
            var entidad = await _personaRepositorio.BuscarPorIdAsync(idVehiculo);
            if (entidad == null)
            {
                return new OperacionDto<RespuestaSimpleDto<string>>(CodigosOperacionDto.Invalido, "Hubo un error al tratar de eliminar el registro.");
            }
            //var dto = _mapper.Map<VehiculoDto>(entidad);

            entidad.EstaBorrado = true;
            entidad.Borrado = DateTime.UtcNow;

            _personaRepositorio.Editar(entidad);

            await _personaRepositorio.UnidadDeTrabajo.GuardarCambiosAsync();

            return new OperacionDto<RespuestaSimpleDto<string>>(new RespuestaSimpleDto<string>() { Mensaje = "Se eliminó correctamente" });
        }

        public async Task<OperacionDto<List<PersonaDto>>> ListarPorCoincidenciaAsync(string coincidencia)
        {
            //var idSitioWeb = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(idSitioWebCifrado);

            var lista = await _personaRepositorio.BuscarPorCoincidenciaAsync(coincidencia, 20);

            lista.Select(e => _mapper.Map<PersonaDto>(e)).ToList();

            //var listaDto = _mapper.Map<List<ProductoDto>>(lista);
            var listaDto = lista.Select(e => _mapper.Map<PersonaDto>(e)).ToList();

            //resultados.Item1.Select(e => _mapper.Map<ProductoDto>(e)).ToList(),
            return new OperacionDto<List<PersonaDto>>(listaDto);
        }

        public async Task<OperacionDto<PersonaDto>> BuscarPorDocumento(string documento)
        {
            //var idSitioWeb = RijndaelUtilitario.DecryptRijndaelFromUrl<long>(idSitioWebCifrado);

            var entidad = await _personaRepositorio.BuscarPorDocumentoAsync(documento);

            if (entidad == null)
            {
                return new OperacionDto<PersonaDto>(CodigosOperacionDto.NoExiste, "No existe el documento.");
            }

            var dto = _mapper.Map<PersonaDto>(entidad);

            return new OperacionDto<PersonaDto>(dto);
        }

    }
}
