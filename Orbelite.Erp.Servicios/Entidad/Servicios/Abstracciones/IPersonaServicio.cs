﻿using Orbelite.Erp.Servicios.Entidad.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Servicios.Entidad.Servicios.Abstracciones
{
    public interface IPersonaServicio
    {

        Task<OperacionDto<RespuestaSimpleDto<string>>> RegistrarPersonaAsync(PersonaDto peticion);
        Task<OperacionDto<RespuestaSimpleDto<string>>> EditarPersonaAsync(PersonaDto peticion);

        Task<OperacionDto<PersonaDto>> DetalleAsync(string id);

        Task<OperacionDto<RespuestaSimpleDto<string>>> EliminarAsync(string id);

        Task<OperacionDto<JQueryDatatableDto<PersonaDto>>> ListarPaginadosAsync(
            string columna, int start, int length, int draw, long? idUsuario,
            string documento, string nombres, string paterno, string materno, string razon);
        Task<OperacionDto<List<PersonaDto>>> ListarPorCoincidenciaAsync(string coincidencia);

        Task<OperacionDto<PersonaDto>> BuscarPorDocumento(string documento);

    }
}
