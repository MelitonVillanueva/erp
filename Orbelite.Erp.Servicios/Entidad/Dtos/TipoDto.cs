﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Entidad.Dtos
{
    public class TipoDto
    {

        public string IdTipo { get; set; }
        public string Nombre { get; set; }
        public string Grupo { get; set; }
        public int Orden { get; set; }

    }
}
