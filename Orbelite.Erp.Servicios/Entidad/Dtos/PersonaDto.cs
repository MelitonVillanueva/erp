﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Entidad.Dtos
{
    public class PersonaDto
    {

        public bool IngresoPorSunat { get; set; }

        public string Id { get; set; }
        public int IdTipo { get; set; }
        public string Documento { get; set; }
        public string Nombres { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public string RazonSocial { get; set; }
        public string Direccion { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public DateTime Creado { get; set; }
        public DateTime Actualizado { get; set; }
        public bool EstaBorrado { get; set; }
        public DateTime Borrado { get; set; }

        public TipoDto Tipo { get; set; }

    }
}
