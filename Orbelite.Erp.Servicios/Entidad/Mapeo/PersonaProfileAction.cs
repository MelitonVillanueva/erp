﻿using AutoMapper;
using Orbelite.Erp.Datos.Entidad.Entidades;
using Orbelite.Erp.Servicios.Entidad.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Utilitarios;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Entidad.Mapeo
{
    public class PersonaProfileAction : IMappingAction<Persona, PersonaDto>
    {

        public void Process(Persona source, PersonaDto destination, ResolutionContext context)
        {
            //if (!string.IsNullOrWhiteSpace(source.Dni))
            //{
            //    destination.Dni = source.Dni.PadLeft(8, '0');
            //}

            destination.Id = RijndaelUtilitario.EncryptRijndaelToUrl(source.Id.ToString());
            if (source.Tipo != null)
            {
                destination.Tipo = context.Mapper.Map<TipoDto>(source.Tipo);
            }
        }

    }
}
