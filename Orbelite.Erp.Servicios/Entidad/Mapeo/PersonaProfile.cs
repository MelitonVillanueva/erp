﻿using AutoMapper;
using Orbelite.Erp.Datos.Entidad.Entidades;
using Orbelite.Erp.Servicios.Entidad.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Entidad.Mapeo
{
    public class PersonaProfile : Profile
    {
        public PersonaProfile()
        {
            CreateMap<Persona, PersonaDto>()
                 .AfterMap<PersonaProfileAction>();
        }
    }
}
