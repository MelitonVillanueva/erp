﻿using AutoMapper;
using Orbelite.Erp.Datos.Entidad.Entidades;
using Orbelite.Erp.Servicios.Entidad.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Entidad.Mapeo
{
    public class TipoProfile : Profile
    {
        public TipoProfile()
        {
            CreateMap<Tipo, TipoDto>()
                 .AfterMap<TipoProfileAction>();
        }
    }
}
