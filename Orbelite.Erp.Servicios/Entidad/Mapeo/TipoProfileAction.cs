﻿using AutoMapper;
using Orbelite.Erp.Datos.Entidad.Entidades;
using Orbelite.Erp.Servicios.Entidad.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Utilitarios;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Entidad.Mapeo
{
    public class TipoProfileAction : IMappingAction<Tipo, TipoDto>
    {
        public void Process(Tipo source, TipoDto destination, ResolutionContext context)
        {
            destination.IdTipo = RijndaelUtilitario.EncryptRijndaelToUrl(source.IdTipo.ToString());
        }
    }
}
