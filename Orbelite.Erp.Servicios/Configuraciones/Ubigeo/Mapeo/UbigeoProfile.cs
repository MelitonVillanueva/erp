﻿using AutoMapper;
using Orbelite.Erp.Servicios.Configuraciones.Ubigeo.Dtos;
using Orbelite.Principal.Seguridad.Datos.Modelo;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Configuraciones.Ubigeo.Mapeo
{
    public class UbigeoProfile : Profile
    {
        public UbigeoProfile()
        {
            CreateMap<Departamento, UbigeoDto>();
            CreateMap<Provincia, UbigeoDto>();
            CreateMap<Distrito, UbigeoDto>();
        }
    }
}
