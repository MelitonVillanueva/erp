﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Configuraciones.Ubigeo.Dtos
{
    public class UbigeoDto
    {
        public string Nombre { get; set; }
    }
}
