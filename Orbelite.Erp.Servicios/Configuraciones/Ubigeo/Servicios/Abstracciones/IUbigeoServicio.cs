﻿using Orbelite.Erp.Servicios.Configuraciones.Ubigeo.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Servicios.Configuraciones.Ubigeo.Servicios.Abstracciones
{
    public interface IUbigeoServicio
    {
        Task<OperacionDto<List<UbigeoDto>>> BuscarDepartamentos();
        Task<OperacionDto<List<UbigeoDto>>> BuscarProvincias(string nombre);
        Task<OperacionDto<List<UbigeoDto>>> BuscarDistritos(string nombre);
    }
}
