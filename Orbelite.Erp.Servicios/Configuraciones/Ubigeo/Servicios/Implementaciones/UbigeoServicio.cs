﻿using AutoMapper;
using Orbelite.Erp.Servicios.Configuraciones.Ubigeo.Dtos;
using Orbelite.Erp.Servicios.Configuraciones.Ubigeo.Servicios.Abstracciones;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using Orbelite.Principal.Seguridad.Datos.Repositorios.Abstracciones;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Servicios.Configuraciones.Ubigeo.Servicios.Implementaciones
{
    public class UbigeoServicio : IUbigeoServicio
    {
        private readonly IUbigeoRepositorio _ubigeoRepositorio;
        private readonly IMapper _mapper;

        public UbigeoServicio(IUbigeoRepositorio ubigeoRepositorio, IMapper mapper)
        {
            _ubigeoRepositorio = ubigeoRepositorio;
            _mapper = mapper;
        }

        public async Task<OperacionDto<List<UbigeoDto>>> BuscarDepartamentos()
        {
            var entidades = await _ubigeoRepositorio.ListarDepartamentos();
            var lista = _mapper.Map<List<UbigeoDto>>(entidades);
            return new OperacionDto<List<UbigeoDto>>(lista);
        }
        public async Task<OperacionDto<List<UbigeoDto>>> BuscarProvincias(string nombre)
        {
            var entidades = await _ubigeoRepositorio.ListarProvinciasPorDepartamento(nombre);
            var lista = _mapper.Map<List<UbigeoDto>>(entidades);
            return new OperacionDto<List<UbigeoDto>>(lista);
        }

        public async Task<OperacionDto<List<UbigeoDto>>> BuscarDistritos(string nombre)
        {
            var entidades = await _ubigeoRepositorio.ListarDistritosPorProvincia(nombre);
            var lista = _mapper.Map<List<UbigeoDto>>(entidades);
            return new OperacionDto<List<UbigeoDto>>(lista);
        }
    }
}
