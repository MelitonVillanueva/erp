﻿using AutoMapper;
using Orbelite.Erp.Servicios.Configuracion.MenuUrl.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Configuracion.MenuUrl.Mapeo
{
    public class MenuUrlProfile:Profile
    {
        public MenuUrlProfile()
        {
            CreateMap<Orbelite.Erp.Datos.Configuracion.Entidades.MenuUrl, MenuUrlAdminDto>()
                .AfterMap<MenuUrlProfileAction>();
        }
    }
}
