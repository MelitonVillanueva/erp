﻿using Orbelite.Erp.Servicios.Configuracion.MenuUrl.Dtos;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Servicios.Configuracion.MenuUrl.Servicios.Abstracciones
{
    public interface IMenuUrlServicio
    {
        Task<OperacionDto<List<MenuUrlAdminDto>>> ObtenerListaMenuUrl(long idUsuario);
    }
}
