﻿using AutoMapper;
using Orbelite.Erp.Datos.Configuracion.Repositorios.Abstracciones;
using Orbelite.Erp.Servicios.Configuracion.MenuUrl.Dtos;
using Orbelite.Erp.Servicios.Configuracion.MenuUrl.Servicios.Abstracciones;
using Orbelite.Principal.Comunes.Infraestructura.Dtos;
using Orbelite.Principal.Seguridad.Datos.Repositorios.Abstracciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbelite.Erp.Servicios.Configuracion.MenuUrl.Servicios.Implementaciones
{
    public class MenuUrlServicio: IMenuUrlServicio
    {
        private readonly IMenuUrlRepositorio _menuUrlRepositorio;
        private readonly IUsuarioRepositorio _usuarioRepositorio;
        private readonly IMapper _mapper;

        public MenuUrlServicio(
            IMapper mapper,
            IMenuUrlRepositorio menuUrlRepositorio,
            IUsuarioRepositorio usuarioRepositorio
            )
        {
            _menuUrlRepositorio = menuUrlRepositorio;
            _usuarioRepositorio = usuarioRepositorio;
            _mapper = mapper;
        }


        public async Task<OperacionDto<List<MenuUrlAdminDto>>> ObtenerListaMenuUrl(long idUsuario)
        {
            var esAdministrador = false;
            var usuario = await _usuarioRepositorio.BuscarPorIdAsync(idUsuario);
            if (usuario != null)
            {
                esAdministrador = usuario.EsAdministrador;
            }
            var menuUrl = await _menuUrlRepositorio.ListarPorGrupoAsync(usuario.IdGrupo,usuario.EsAdministrador);
            var dto = _mapper.Map<List<MenuUrlAdminDto>>(menuUrl);

            var dtoFinal = new List<MenuUrlAdminDto>();
            var conUrls = dto.Where(e => !string.IsNullOrWhiteSpace(e.Url)).ToList();
            var idExistentes = conUrls.Select(e => e.IdMenuUrl).ToList();
            dtoFinal.AddRange(conUrls);

            foreach (var conUrl in conUrls)
            {
                AgregarPadres(dto, conUrl, dtoFinal, idExistentes);
            }

            return new OperacionDto<List<MenuUrlAdminDto>>(dtoFinal);
        }

        private void AgregarPadres(List<MenuUrlAdminDto> todos, MenuUrlAdminDto actual, List<MenuUrlAdminDto> final, List<int> idExistentes)
        {
            if (!actual.IdMenuUrlPadre.HasValue)
            {
                return;
            }
            var padre = todos.Where(e => e.IdMenuUrl == actual.IdMenuUrlPadre).FirstOrDefault();
            if (padre == null)
            {
                return;
            }
            else
            {
                if (!idExistentes.Contains(padre.IdMenuUrl))
                {
                    final.Add(padre);
                    idExistentes.Add(padre.IdMenuUrl);
                }
                AgregarPadres(todos, padre, final, idExistentes);
            }

        }


    }
}
