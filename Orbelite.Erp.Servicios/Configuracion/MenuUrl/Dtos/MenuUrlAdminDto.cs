﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orbelite.Erp.Servicios.Configuracion.MenuUrl.Dtos
{
    public class MenuUrlAdminDto
    {
        public int IdMenuUrl { get; set; }
        public string Nombre { get; set; }
        public string Icono { get; set; }
        public string Url { get; set; }
        public int? IdMenuUrlPadre { get; set; }
        public int Orden { get; set; }
    }
}
